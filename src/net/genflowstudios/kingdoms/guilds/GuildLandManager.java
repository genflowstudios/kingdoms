package net.genflowstudios.kingdoms.guilds;

import net.genflowstudios.kingdoms.kingdoms.areas.KingdomRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/19/2015.
 */
public class GuildLandManager{

    ArrayList<KingdomRegion> kingdomRegions;
    ArrayList<KingdomSubRegion> subRegions;
    ArrayList<KingdomCity> cities;
    ArrayList<KingdomPlayerArea> playerAreas;


    public GuildLandManager(ArrayList<KingdomRegion> kingdomRegions, ArrayList<KingdomSubRegion> subRegions, ArrayList<KingdomCity> cities, ArrayList<KingdomPlayerArea> playerAreas){
        this.kingdomRegions = kingdomRegions;
        this.subRegions = subRegions;
        this.cities = cities;
        this.playerAreas = playerAreas;
    }

    public GuildLandManager(){
        this.kingdomRegions = new ArrayList<KingdomRegion>();
        this.subRegions = new ArrayList<KingdomSubRegion>();
        this.cities = new ArrayList<KingdomCity>();
        this.playerAreas = new ArrayList<KingdomPlayerArea>();
    }


    public boolean containsKingdomRegion(KingdomRegion region){
        if(getKingdomRegions().isEmpty()){
            return false;
        }else{
            return getKingdomRegions().contains(region);
        }
    }

    public boolean containsKingdomSubRegion(KingdomSubRegion region){
        if(getSubRegions().isEmpty()){
            return false;
        }else{
            return getSubRegions().contains(region);
        }
    }

    public boolean containsKingdomCity(KingdomCity city){
        if(getCities().isEmpty()){
            return false;
        }else{
            return getCities().contains(city);
        }
    }

    public boolean containsKingdomPlayerArea(KingdomPlayerArea area){
        if(getPlayerAreas().isEmpty()){
            return false;
        }else{
            return getPlayerAreas().contains(area);
        }
    }


    public void addKingdomRegion(KingdomRegion region) throws Exception{
        if(getKingdomRegions().contains(region)){
            throw new Exception("That guild already contains that region.");
        }else{
            this.kingdomRegions.add(region);
        }
    }

    public void addKingdomSubRegion(KingdomSubRegion region) throws Exception{
        if(getSubRegions().contains(region)){
            throw new Exception("That guild already contains that region.");
        }else{
            this.subRegions.add(region);
        }
    }

    public void addKingdomCity(KingdomCity region) throws Exception{
        if(getCities().contains(region)){
            throw new Exception("That guild already contains that region.");
        }else{
            this.cities.add(region);
        }
    }

    public void addKingdomPlayerArea(KingdomPlayerArea region) throws Exception{
        if(getPlayerAreas().contains(region)){
            throw new Exception("That guild already contains that region.");
        }else{
            this.playerAreas.add(region);
        }
    }

    public void removeKingdomRegion(KingdomRegion region) throws Exception{
        if(getKingdomRegions().contains(region)){
            this.kingdomRegions.remove(region);
        }else{
            throw new Exception("That guild does not contain that region.");
        }
    }

    public void removeKingdomSubRegion(KingdomSubRegion region) throws Exception{
        if(getSubRegions().contains(region)){
            this.subRegions.remove(region);
        }else{
            throw new Exception("That guild does not contain that region.");
        }
    }

    public void removeKingdomCity(KingdomCity region) throws Exception{
        if(getCities().contains(region)){
            this.cities.remove(region);
        }else{
            throw new Exception("That guild does not contain that region.");
        }
    }

    public void removeKingdomPlayerArea(KingdomPlayerArea region) throws Exception{
        if(getPlayerAreas().contains(region)){
            this.playerAreas.remove(region);
        }else{
            throw new Exception("That guild does not contain that region.");
        }
    }


    public ArrayList<KingdomRegion> getKingdomRegions(){
        return kingdomRegions;
    }

    public void setKingdomRegions(ArrayList<KingdomRegion> kingdomRegions){
        this.kingdomRegions = kingdomRegions;
    }

    public ArrayList<KingdomSubRegion> getSubRegions(){
        return subRegions;
    }

    public void setSubRegions(ArrayList<KingdomSubRegion> subRegions){
        this.subRegions = subRegions;
    }

    public ArrayList<KingdomCity> getCities(){
        return cities;
    }

    public void setCities(ArrayList<KingdomCity> cities){
        this.cities = cities;
    }

    public ArrayList<KingdomPlayerArea> getPlayerAreas(){
        return playerAreas;
    }

    public void setPlayerAreas(ArrayList<KingdomPlayerArea> playerAreas){
        this.playerAreas = playerAreas;
    }
}
