package net.genflowstudios.kingdoms.guilds;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class Guild{

    String name;
    UUID leader;
    boolean ownsLand;
    GuildLandManager manager;
    GuildStats stats;

    ArrayList<UUID> members;
    ArrayList<String> allies;
    ArrayList<String> enemies;


    public Guild(ArrayList<UUID> members, ArrayList<String> allies, ArrayList<String> enemies, String name, UUID leader, GuildStats stats, boolean ownsLand, GuildLandManager manager){
        this.members = members;
        this.allies = allies;
        this.enemies = enemies;
        this.name = name;
        this.leader = leader;
        this.stats = stats;
        this.ownsLand = ownsLand;
        this.manager = manager;
        try{
            GuildManager.addGuild(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public Guild(String name, UUID leader, GuildStats stats, boolean ownsLand, ArrayList<UUID> members, ArrayList<String> allies, ArrayList<String> enemies){
        this.name = name;
        this.leader = leader;
        this.ownsLand = ownsLand;
        this.stats = stats;
        this.members = members;
        this.allies = allies;
        this.enemies = enemies;
        this.manager = new GuildLandManager();
        try{
            GuildManager.addGuild(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public Guild(String name, UUID leader){
        this.name = name;
        this.leader = leader;
        this.ownsLand = false;
        this.members = new ArrayList<UUID>();
        this.allies = new ArrayList<String>();
        this.enemies = new ArrayList<String>();
        this.manager = new GuildLandManager();
        try{
            GuildManager.addGuild(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public Guild(String name){
        this.name = name;
        this.ownsLand = true;
        this.manager = new GuildLandManager();
        try{
            GuildManager.addGuild(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Guild getNPC(){
        return GuildManager.getGuilds().get(0);
    }


    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public UUID getLeader(){
        return leader;
    }

    public void setLeader(UUID leader){
        this.leader = leader;
    }

    public boolean isOwnsLand(){
        return ownsLand;
    }

    public void setOwnsLand(boolean ownsLand){
        this.ownsLand = ownsLand;
    }

    public GuildLandManager getManager(){
        return manager;
    }

    public void setManager(GuildLandManager manager){
        this.manager = manager;
    }

    public ArrayList<UUID> getMembers(){
        return members;
    }

    public void setMembers(ArrayList<UUID> members){
        this.members = members;
    }

    public GuildStats getStats(){
        return stats;
    }

    public void setStats(GuildStats stats){
        this.stats = stats;
    }

    public ArrayList<String> getAllies(){
        return allies;
    }

    public void setAllies(ArrayList<String> allies){
        this.allies = allies;
    }

    public ArrayList<String> getEnemies(){
        return enemies;
    }

    public void setEnemies(ArrayList<String> enemies){
        this.enemies = enemies;
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        Guild guild = (Guild) o;

        if(isOwnsLand() != guild.isOwnsLand()) return false;
        if(!getName().equals(guild.getName())) return false;
        if(!getLeader().equals(guild.getLeader())) return false;
        return getMembers().equals(guild.getMembers());

    }
}
