package net.genflowstudios.kingdoms.guilds;

/**
 * Created by Samuel on 7/19/2015.
 */
public class GuildStats{

    int members, onlineMembers;
    boolean ownsLand;
    int kingdomRegions, kingdomSubRegions, kingdomCities, kingdomPlayerAreas;

    double guildExp;
    int guildLevel;
    double guildBonds;

    public int getMembers(){
        return members;
    }

    public void setMembers(int members){
        this.members = members;
    }

    public int getOnlineMembers(){
        return onlineMembers;
    }

    public void setOnlineMembers(int onlineMembers){
        this.onlineMembers = onlineMembers;
    }

    public boolean isOwnsLand(){
        return ownsLand;
    }

    public void setOwnsLand(boolean ownsLand){
        this.ownsLand = ownsLand;
    }

    public int getKingdomRegions(){
        return kingdomRegions;
    }

    public void setKingdomRegions(int kingdomRegions){
        this.kingdomRegions = kingdomRegions;
    }

    public int getKingdomSubRegions(){
        return kingdomSubRegions;
    }

    public void setKingdomSubRegions(int kingdomSubRegions){
        this.kingdomSubRegions = kingdomSubRegions;
    }

    public int getKingdomCities(){
        return kingdomCities;
    }

    public void setKingdomCities(int kingdomCities){
        this.kingdomCities = kingdomCities;
    }

    public int getKingdomPlayerAreas(){
        return kingdomPlayerAreas;
    }

    public void setKingdomPlayerAreas(int kingdomPlayerAreas){
        this.kingdomPlayerAreas = kingdomPlayerAreas;
    }

    public double getGuildExp(){
        return guildExp;
    }

    public void setGuildExp(double guildExp){
        this.guildExp = guildExp;
    }

    public int getGuildLevel(){
        return guildLevel;
    }

    public void setGuildLevel(int guildLevel){
        this.guildLevel = guildLevel;
    }

    public double getGuildBonds(){
        return guildBonds;
    }

    public void setGuildBonds(double guildBonds){
        this.guildBonds = guildBonds;
    }
}
