package net.genflowstudios.kingdoms.guilds;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/19/2015.
 */
public class GuildManager{

    public static ArrayList<Guild> guilds = new ArrayList<Guild>();


    public static void addGuild(Guild guild) throws Exception{
        if(guilds.isEmpty()){
            guilds.add(guild);
        }else{
            if(contains(guild)){
                throw new Exception("That guild already exists!");
            }else{
                guilds.add(guild);
            }
        }
    }

    public static void removeGuild(Guild guild) throws Exception{
        if(guilds.isEmpty()){
            throw new Exception("There are no guilds to remove!");
        }else{
            if(contains(guild)){
                guilds.remove(guild);
            }else{
                throw new Exception("That guild does not exist!");
            }
        }
    }

    public static boolean contains(Guild guild){
        for(Guild g : guilds){
            if(g.getName().equals(guild.getName())){
                return true;
            }
        }
        return false;
    }


    public static ArrayList<Guild> getGuilds(){
        return guilds;
    }

    public static void setGuilds(ArrayList<Guild> guilds){
        GuildManager.guilds = guilds;
    }
}
