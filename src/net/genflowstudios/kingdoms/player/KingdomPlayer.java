package net.genflowstudios.kingdoms.player;

import net.genflowstudios.kingdoms.display.ScoreboardTypes;
import net.genflowstudios.kingdoms.guilds.Guild;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

public class KingdomPlayer{

    public static ArrayList<KingdomPlayer> players = new ArrayList<KingdomPlayer>();

    ArrayList<ScoreboardTypes> displays;
    int displayLine = 0;
    boolean hasDisplay, ownsArea, inGuild, isInvited;
    UUID uuid;
    String username;
    KingdomPlayer player;
    KPlayerStats stats;
    KPlayerInventory inventory;
    KPlayerLocation location;
    Guild guild;

    public KingdomPlayer(UUID uuid){
        if(exists(uuid)){
            this.uuid = uuid;
            try{
                this.stats = getPlayer(uuid).getStats();
                this.location = getPlayer(uuid).getLocation();
                this.displays = getPlayer(uuid).getDisplays();
                this.inventory = getPlayer(uuid).getInventory();
                this.inGuild = getPlayer(uuid).isInGuild();
                this.ownsArea = getPlayer(uuid).isOwnsArea();
                this.isInvited = getPlayer(uuid).isInvited();
                this.player = getPlayer(uuid);
                if(Bukkit.getPlayer(uuid).getInventory().contains(createItems()[0]) || Bukkit.getPlayer(uuid).getInventory().contains(createItems()[1])){

                }else{
                    Bukkit.getPlayer(uuid).getInventory().addItem(createItems()[0], createItems()[1]);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }else{
            this.uuid = uuid;
            this.stats = new KPlayerStats(this);
            this.location = new KPlayerLocation(this);
            this.player = this;
            this.isInvited = false;
            this.inGuild = false;
            this.ownsArea = false;
            this.username = Bukkit.getPlayer(uuid).getName();
            this.inventory = new KPlayerInventory(createItems()[0], createItems()[1], uuid);
            try{
                InventoryManager invManager = new InventoryManager(inventory);
            }catch(Exception e){
                e.printStackTrace();
            }
            this.displays = new ArrayList<ScoreboardTypes>();
            this.hasDisplay = false;
            if(Bukkit.getPlayer(uuid).getInventory().contains(createItems()[0]) || Bukkit.getPlayer(uuid).getInventory().contains(createItems()[1])){

            }else{
                Bukkit.getPlayer(uuid).getInventory().addItem(createItems()[0], createItems()[1]);
            }
            this.player = this;
            players.add(player);
        }
    }

    public static boolean exists(UUID uuid){
        for(KingdomPlayer p : players){
            if(p.getUuid().equals(uuid)){
                return true;
            }
        }
        return false;
    }

    public static KingdomPlayer getPlayer(UUID uuid) throws Exception{
        if(players.isEmpty()){
            throw new Exception("Error on fetching player, player list is empty!");
        }else{
            for(KingdomPlayer p : players){
                if(p.getUuid().equals(uuid)){
                    return p;
                }
            }
            throw new Exception("Error on fetching player, player does not exist!");
        }

    }

    /**
     * @return the uuid
     */
    public UUID getUuid(){
        return uuid;

    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(UUID uuid){
        this.uuid = uuid;
        players.remove(player);
        players.add(player);
    }

    /**
     * @return the stats
     */
    public KPlayerStats getStats(){
        return stats;
    }

    /**
     * @param stats the stats to set
     */
    public void setStats(KPlayerStats stats){
        this.stats = stats;
        players.remove(player);
        players.add(player);
    }

    public Player getPlayer(){
        return Bukkit.getPlayer(getUuid());
    }

    /**
     * @return the location
     */
    public KPlayerLocation getLocation(){
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(KPlayerLocation location){
        this.location = location;
    }


    public void addDisplay(ScoreboardTypes type){
        if(displays.isEmpty()){
            this.displays.add(type);
            players.remove(player);
            players.add(player);
        }else{
            if(displays.contains(type)){

            }else{
                this.displays.add(type);
                players.remove(player);
                players.add(player);
            }
        }

    }

    public void removeDisplay(ScoreboardTypes type) throws Exception{
        if(displays.contains(type)){
            this.displays.remove(type);
            players.remove(player);
            players.add(player);
        }else{
            throw new Exception("Displays does not contain that scoreboard type!");
        }
    }


    /**
     * @return the displays
     */
    public ArrayList<ScoreboardTypes> getDisplays(){
        return displays;
    }

    /**
     * @param displays the displays to set
     */
    public void setDisplays(ArrayList<ScoreboardTypes> displays){
        this.displays = displays;
        players.remove(player);
        players.add(player);
    }

    /**
     * @return the displayLine
     */
    public int getDisplayLine(){
        return displayLine;
    }

    /**
     * @param displayLine the displayLine to set
     */
    public void setDisplayLine(int displayLine){
        this.displayLine = displayLine;
        players.remove(player);
        players.add(player);
    }

    /**
     * @return the hasDisplay
     */
    public boolean isHasDisplay(){
        return hasDisplay;
    }

    /**
     * @param hasDisplay the hasDisplay to set
     */
    public void setHasDisplay(boolean hasDisplay){
        this.hasDisplay = hasDisplay;
        players.remove(player);
        players.add(player);
    }


    public KPlayerInventory getInventory(){
        return InventoryManager.getInventory(getUuid());
    }

    public void setInventory(KPlayerInventory inventory){
        this.inventory = inventory;
        players.remove(player);
        players.add(player);
    }


    public ItemStack[] createItems(){
        ItemStack[] items = new ItemStack[2];
        ArrayList<String> lore = new ArrayList<String>();
        lore.add("Right click to see materials.");
        ItemStack stoneBag = new ItemStack(Material.STONE, 1);
        ItemMeta stoneMeta = stoneBag.getItemMeta();
        stoneMeta.setDisplayName("Stone Resources Bag");
        stoneMeta.setLore(lore);
        stoneBag.setItemMeta(stoneMeta);
        ItemStack lumberBag = new ItemStack(Material.LOG, 1);
        ItemMeta lumberMeta = lumberBag.getItemMeta();
        lumberMeta.setDisplayName("Lumber Resources Bag");
        lumberMeta.setLore(lore);
        lumberBag.setItemMeta(lumberMeta);
        items[0] = stoneBag;
        items[1] = lumberBag;
        return items;
    }


    public String getUsername(){
        return username;
    }

    public void setUsername(String username){
        this.username = username;
        players.remove(player);
        players.add(player);
    }


    public boolean isOwnsArea(){
        return ownsArea;
    }

    public void setOwnsArea(boolean ownsArea){
        this.ownsArea = ownsArea;
        players.remove(player);
        players.add(player);
    }

    public boolean isInvited(){
        return isInvited;
    }

    public void setIsInvited(boolean isInvited){
        this.isInvited = isInvited;
        players.remove(player);
        players.add(player);
    }

    public boolean isInGuild(){
        return inGuild;
    }

    public void setInGuild(boolean inGuild){
        this.inGuild = inGuild;
    }

    public Guild getGuild(){
        return guild;
    }

    public void setGuild(Guild guild){
        this.guild = guild;
    }
}
