package net.genflowstudios.kingdoms.player.skills;

import java.util.ArrayList;

/**
 * Created by Samuel on 6/17/2015.
 */
public enum SkillType{

    HITPOINTS{
        @Override
        public String getName(){
            return "Hit Points";
        }

        @Override
        public String getActionName(){
            return "";
        }
    },
    ATTACK{
        @Override
        public String getName(){
            return "Attack";
        }

        @Override
        public String getActionName(){
            return "attack";
        }
    },
    STRENGTH{
        @Override
        public String getName(){
            return "Strength";
        }

        @Override
        public String getActionName(){
            return "";
        }
    },
    DEFENCE{
        @Override
        public String getName(){
            return "Defence";
        }

        @Override
        public String getActionName(){
            return "defend";
        }


    },
    RANGED{
        @Override
        public String getName(){
            return "Ranged";
        }

        @Override
        public String getActionName(){
            return "shoot";
        }
    },
    MAGIC{
        @Override
        public String getName(){
            return "Magic";
        }

        @Override
        public String getActionName(){
            return "cast";
        }
    },
    WOODCUTTING{
        @Override
        public String getName(){
            return "Woodcutting";
        }

        @Override
        public String getActionName(){
            return "chop";
        }
    },
    MINING{
        @Override
        public String getName(){
            return "Mining";
        }

        @Override
        public String getActionName(){
            return "mine";
        }
    },
    SMITHING{
        @Override
        public String getName(){
            return "Smithing";
        }

        @Override
        public String getActionName(){
            return "smith";
        }
    },
    CRAFTING{
        @Override
        public String getName(){
            return "Crafting";
        }

        @Override
        public String getActionName(){
            return "craft";
        }
    };


    public static double getMaxAbsorb(int level){
        return (level * .05) * 100;
    }

    public static ArrayList<SkillType> getSkills(){
        ArrayList<SkillType> skills = new ArrayList<SkillType>();
        skills.add(ATTACK);
        skills.add(STRENGTH);
        skills.add(DEFENCE);
        skills.add(HITPOINTS);
        skills.add(RANGED);
        skills.add(MAGIC);
        skills.add(WOODCUTTING);
        skills.add(MINING);
        skills.add(CRAFTING);
        skills.add(SMITHING);
        return skills;
    }

    public abstract String getName();

    public abstract String getActionName();

}
