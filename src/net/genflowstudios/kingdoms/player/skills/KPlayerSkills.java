package net.genflowstudios.kingdoms.player.skills;

import net.genflowstudios.kingdoms.player.events.PlayerLevelUpEvent;
import org.bukkit.Bukkit;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Samuel on 6/17/2015.
 */
public class KPlayerSkills{


    HashMap<SkillType, Integer> skillLevels = new HashMap<SkillType, Integer>();
    HashMap<SkillType, Double> skillExp = new HashMap<SkillType, Double>();

    public KPlayerSkills(){
        if(skillLevels.isEmpty()){
            if(skillExp.isEmpty()){
                for(SkillType type : SkillType.getSkills()){
                    skillLevels.put(type, 1);
                    skillExp.put(type, 0.0);
                }
            }else{
                for(SkillType type : SkillType.getSkills()){
                    skillLevels.put(type, 1);
                }
            }
        }else{
            if(skillExp.isEmpty()){
                for(SkillType type : SkillType.getSkills()){
                    skillExp.put(type, 0.0);
                }
            }else{

            }
        }
    }


    public KPlayerSkills(HashMap<SkillType, Integer> skillLevels, HashMap<SkillType, Double> skillExp){
        this.skillLevels = skillLevels;
        this.skillExp = skillExp;
    }


    public int getSkillLevel(SkillType type){
        return skillLevels.get(type);
    }

    public double getSkillExp(SkillType type){
        return Double.parseDouble(new DecimalFormat("#####.##").format(skillExp.get(type)));
    }

    public void setSkillExp(SkillType type, double exp){
        double amount = Double.parseDouble(new DecimalFormat("#####.##").format(exp));
        skillExp.remove(type);
        skillExp.put(type, amount);
    }


    public void levelUp(UUID uuid){
        for(SkillType type : SkillType.getSkills()){
            if(getNextLevelExp(skillLevels.get(type)) <= skillExp.get(type)){
                Bukkit.getPluginManager().callEvent(new PlayerLevelUpEvent(uuid, type, 1));
            }
        }
    }


    public void addSkillExp(SkillType type, double amount){
        double intAmount = skillExp.get(type);
        skillExp.remove(type);
        skillExp.put(type, intAmount + amount);
    }

    public void addSkillLevel(SkillType type, int amount){
        int lastLevel = skillLevels.get(type);
        skillLevels.remove(type);
        skillLevels.put(type, lastLevel + amount);
    }

    public void removeSkillExp(SkillType type, double amount){
        double lastExp = skillExp.get(type);
        skillExp.remove(type);
        skillExp.put(type, lastExp - amount);
    }

    public void removeSkillLevel(SkillType type, int amount){
        int lastLevel = skillLevels.get(type);
        skillLevels.remove(type);
        skillLevels.put(type, lastLevel - amount);
    }


    public double getNextLevelExp(int level){
        double next_level = 9 * Math.pow(level, 2) + 19 * level + 10;
        return Math.round(next_level);
    }

    public double amountTillNextLevel(SkillType type){
        return getNextLevelExp(getSkillLevel(type)) - getSkillExp(type);
    }


    public HashMap<SkillType, Integer> getSkillLevels(){
        return skillLevels;
    }

    public void setSkillLevels(HashMap<SkillType, Integer> skillLevels){
        this.skillLevels = skillLevels;
    }

    public HashMap<SkillType, Double> getSkillExp(){
        return skillExp;
    }

    public void setSkillExp(HashMap<SkillType, Double> skillExp){
        this.skillExp = skillExp;
    }

    public void setSkillLevel(SkillType type, int value){
        skillLevels.remove(type);
        skillLevels.put(type, value);
        skillExp.remove(type);
        skillExp.put(type, 0.0);
    }
}
