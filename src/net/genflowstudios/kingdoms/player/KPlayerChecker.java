package net.genflowstudios.kingdoms.player;

import net.genflowstudios.kingdoms.guilds.Guild;

import java.util.UUID;

/**
 * Created by Samuel on 7/14/2015.
 */
public class KPlayerChecker{


    public boolean isAlly(UUID player, UUID target){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player);
            if(kingdomPlayer.isInGuild()){
                Guild guild = kingdomPlayer.getGuild();
                KingdomPlayer targetPlayer = KingdomPlayer.getPlayer(target);
                if(targetPlayer.isInGuild()){
                    if(guild.equals(targetPlayer.getGuild())){
                        return true;
                    }else{
                        return guild.getAllies().contains(targetPlayer.getGuild().getName());
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }


}
