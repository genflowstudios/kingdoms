package net.genflowstudios.kingdoms.player.events;

import net.genflowstudios.kingdoms.events.KingdomEvent;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class PlayerLevelUpEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    UUID uuid;
    SkillType type;
    int amount;
    boolean cancel = false;

    public PlayerLevelUpEvent(UUID uuid, SkillType type, int amount){
        this.uuid = uuid;
        this.type = type;
        this.amount = amount;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public UUID getUuid(){
        return uuid;
    }

    public SkillType getType(){
        return type;
    }

    public int getAmount(){
        return amount;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

}
