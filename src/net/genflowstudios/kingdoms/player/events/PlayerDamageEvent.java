package net.genflowstudios.kingdoms.player.events;

import net.genflowstudios.kingdoms.events.KingdomEvent;
import net.genflowstudios.kingdoms.player.PlayerDamageReason;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class PlayerDamageEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    UUID uuid;
    PlayerDamageReason reason;
    double damage;
    boolean cancel = false;

    public PlayerDamageEvent(UUID uuid, PlayerDamageReason reason, double damage){
        this.uuid = uuid;
        this.reason = reason;
        this.damage = damage;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }


    public UUID getUuid(){
        return uuid;
    }

    public PlayerDamageReason getReason(){
        return reason;
    }

    public double getDamage(){
        return damage;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

}
