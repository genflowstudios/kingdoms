package net.genflowstudios.kingdoms.player;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.items.KingdomItem;
import net.genflowstudios.kingdoms.items.items.bags.*;
import net.genflowstudios.kingdoms.items.items.itemproperties.BagTier;
import net.genflowstudios.kingdoms.items.items.itemproperties.BagType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 6/4/2015.
 */
public class KPlayerInventory implements Listener{


    UUID playerOwner;
    KPlayerTools playerTools;
    StoneBag stoneBag;
    LumberBag lumberBag;
    IronBag ironBag;
    GoldBag goldBag;
    DiamondBag diamondBag;
    ArrayList<KingdomItem> items;

    public KPlayerInventory(KPlayerTools tools, StoneBag stoneBag, LumberBag lumberBag, ArrayList<KingdomItem> items, UUID playerOwner){
        this.playerTools = tools;
        this.playerOwner = playerOwner;
        this.stoneBag = stoneBag;
        this.lumberBag = lumberBag;
        this.items = items;
        Bukkit.getPluginManager().registerEvents(this, Engine.getInstance());
    }

    public KPlayerInventory(StoneBag stoneBag, LumberBag lumberBag, ArrayList<KingdomItem> items, UUID playerOwner){
        this.stoneBag = stoneBag;
        this.playerOwner = playerOwner;
        this.lumberBag = lumberBag;
        this.items = items;
        this.playerTools = new KPlayerTools(false);
        Bukkit.getPluginManager().registerEvents(this, Engine.getInstance());
    }


    public KPlayerInventory(ItemStack stone, ItemStack lumber, UUID playerOwner){
        this.playerTools = new KPlayerTools(false);
        this.items = new ArrayList<KingdomItem>();
        this.stoneBag = new StoneBag(BagTier.TIER_ONE.getSize(), BagTier.TIER_ONE, BagType.STONE, stone);
        this.lumberBag = new LumberBag(BagTier.TIER_ONE.getSize(), BagTier.TIER_ONE, BagType.LUMBER, lumber);
        this.playerOwner = playerOwner;
        Bukkit.getPluginManager().registerEvents(this, Engine.getInstance());
    }


    public void addKingdomItem(KingdomItem item) throws Exception{
        if(itemExists(item)){
            throw new Exception("That item already exists in that array!");
        }else{
            this.items.add(item);
            Bukkit.getPlayer(getPlayerOwner()).getInventory().addItem(item.getItemstack());
        }
    }

    public void removeKingdomItem(KingdomItem item) throws Exception{
        if(itemExists(item)){
            this.items.remove(item);
            Bukkit.getPlayer(getPlayerOwner()).getInventory().remove(item.getItemstack());
        }else{
            throw new Exception("That item already exists in that array!");
        }
    }

    private boolean itemExists(KingdomItem item){
        for(KingdomItem kingdomItem : getItems()){
            if(item.equals(kingdomItem)){
                return true;
            }
        }
        return false;
    }


    public boolean isLumber(Material material){
        return material.name().equalsIgnoreCase(Material.LOG.name()) || material.name().equalsIgnoreCase(Material.LOG_2.name());
    }


    public boolean isStone(Material material){
        return material.name().equalsIgnoreCase(Material.STONE.name()) || material.name().equalsIgnoreCase(Material.COBBLESTONE.name()) || material.name().equalsIgnoreCase(Material.IRON_ORE.name()) || material.name().equalsIgnoreCase(Material.GOLD_ORE.name()) || material.name().equalsIgnoreCase(Material.COAL_ORE.name()) || material.name().equalsIgnoreCase(Material.REDSTONE_ORE.name()) || material.name().equalsIgnoreCase(Material.DIAMOND_ORE.name());
    }


    public StoneBag getStoneBag(){
        return stoneBag;
    }

    public void setStoneBag(StoneBag stoneBag){
        this.stoneBag = stoneBag;
    }

    public LumberBag getLumberBag(){
        return lumberBag;
    }

    public void setLumberBag(LumberBag lumberBag){
        this.lumberBag = lumberBag;
    }


    public UUID getPlayerOwner(){
        return playerOwner;
    }

    public void setPlayerOwner(UUID playerOwner){
        this.playerOwner = playerOwner;
    }


    public KPlayerTools getPlayerTools(){
        return playerTools;
    }

    public void setPlayerTools(KPlayerTools playerTools){
        this.playerTools = playerTools;
    }

    public IronBag getIronBag(){
        return ironBag;
    }

    public void setIronBag(IronBag ironBag){
        this.ironBag = ironBag;
    }

    public GoldBag getGoldBag(){
        return goldBag;
    }

    public void setGoldBag(GoldBag goldBag){
        this.goldBag = goldBag;
    }

    public DiamondBag getDiamondBag(){
        return diamondBag;
    }

    public void setDiamondBag(DiamondBag diamondBag){
        this.diamondBag = diamondBag;
    }

    public ArrayList<KingdomItem> getItems(){
        return items;
    }

    public void setItems(ArrayList<KingdomItem> items){
        this.items = items;
    }
}
