package net.genflowstudios.kingdoms.player;

import java.util.ArrayList;

public class KPlayerAttributes{

    public static ArrayList<KPlayerAttributes> players = new ArrayList<KPlayerAttributes>();
    KPlayerStats stats;
    KPlayerAttributes attributes;
    double health, mana, rage, focus, armor, magicArmor;
    int strength, intellect;

    int productionBonus;

    public KPlayerAttributes(KPlayerStats stats){
        if(exists(stats)){
            this.stats = stats;
        }else{
            this.stats = stats;
            this.health = 100;
            this.mana = 100;
            this.rage = 0;
            this.armor = 0;
            this.magicArmor = 0;
            this.strength = 10;
            this.intellect = 10;
            this.productionBonus = 0;
            this.attributes = this;
            players.add(attributes);
        }
    }

    private boolean exists(KPlayerStats s){
        if(players.isEmpty()){
            return false;
        }else{
            for(KPlayerAttributes att : players){
                if(att.getStats().getPlayer().getUuid().equals(s.getPlayer().getUuid())){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return the stats
     */
    public KPlayerStats getStats(){
        return stats;
    }

    /**
     * @param stats the stats to set
     */
    public void setStats(KPlayerStats stats){
        this.stats = stats;
    }

    public double getHealth(){
        return health;
    }

    public void setHealth(double health){
        this.health = health;
    }

    public double getMana(){
        return mana;
    }

    public void setMana(double mana){
        this.mana = mana;
    }

    public double getRage(){
        return rage;
    }

    public void setRage(double rage){
        this.rage = rage;
    }

    public double getFocus(){
        return focus;
    }

    public void setFocus(double focus){
        this.focus = focus;
    }

    public double getArmor(){
        return armor;
    }

    public void setArmor(double armor){
        this.armor = armor;
    }

    public double getMagicArmor(){
        return magicArmor;
    }

    public void setMagicArmor(double magicArmor){
        this.magicArmor = magicArmor;
    }

    /**
     * @return the strength
     */
    public int getStrength(){
        return strength;
    }

    /**
     * @param strength the strength to set
     */
    public void setStrength(int strength){
        this.strength = strength;
    }

    /**
     * @return the intellect
     */
    public int getIntellect(){
        return intellect;
    }

    /**
     * @param intellect the intellect to set
     */
    public void setIntellect(int intellect){
        this.intellect = intellect;
    }


    public int getProductionBonus(){
        return productionBonus;
    }

    public void setProductionBonus(int productionBonus){
        this.productionBonus = productionBonus;
    }
}
