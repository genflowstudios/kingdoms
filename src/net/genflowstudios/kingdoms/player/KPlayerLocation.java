package net.genflowstudios.kingdoms.player;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;

public class KPlayerLocation{

    public static ArrayList<KPlayerLocation> locations = new ArrayList<KPlayerLocation>();

    KingdomPlayer player;
    Location location;
    KPlayerLocation kPLocation;
    String region, subRegion, city, area;

    public KPlayerLocation(KingdomPlayer player){
        if(exists(player)){
            this.player = player;
        }else{
            this.player = player;
            this.location = Bukkit.getPlayer(player.getUuid()).getLocation();
            this.kPLocation = this;
            locations.add(kPLocation);
        }
    }

    private boolean exists(KingdomPlayer p){
        if(locations.isEmpty()){
            return false;
        }else{
            for(KPlayerLocation l : locations){
                if(l.getPlayer().getUuid().equals(p.getUuid())){
                    return true;
                }
            }
            return false;
        }

    }

    /**
     * @return the player
     */
    public KingdomPlayer getPlayer(){
        return player;
    }

    /**
     * @param player the player to set
     */
    public void setPlayer(KingdomPlayer player){
        this.player = player;
    }

    /**
     * @return the location
     */
    public Location getLocation(){
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Location location){
        this.location = location;
    }

    /**
     * @return the area
     */
    public String getArea(){
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(String area){
        this.area = area;
    }

    public String getRegion(){
        return region;
    }

    public void setRegion(String region){
        this.region = region;
    }

    public String getSubRegion(){
        return subRegion;
    }

    public void setSubRegion(String subRegion){
        this.subRegion = subRegion;
    }

    public String getCity(){
        return city;
    }

    public void setCity(String city){
        this.city = city;
    }
}
