package net.genflowstudios.kingdoms.player.actions;

import net.genflowstudios.kingdoms.player.InventoryManager;
import net.genflowstudios.kingdoms.player.KPlayerInventory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Samuel on 6/9/2015.
 */
public class ResourceHarvestToggleAction implements Listener{


    @EventHandler
    public void onResourceToggle(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
            if(player.isSneaking() && (isPickaxe(event.getItem()) || isHatchet(event.getItem()))){
                KPlayerInventory inventory = InventoryManager.getInventory(player.getUniqueId());
                if(inventory.getPlayerTools().isHarvestTool()){
                    inventory.getPlayerTools().setHarvestTool(false);
                    player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You have disabled harvesting.");
                }else{
                    inventory.getPlayerTools().setHarvestTool(true);
                    player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have enabled harvesting.");
                }
            }
        }
    }


    private boolean isPickaxe(ItemStack item){
        return item.getType().equals(Material.WOOD_PICKAXE) || item.getType().equals(Material.STONE_PICKAXE) || item.getType().equals(Material.IRON_PICKAXE) || item.getType().equals(Material.GOLD_PICKAXE) || item.getType().equals(Material.DIAMOND_PICKAXE);
    }

    private boolean isHatchet(ItemStack item){
        return item.getType().equals(Material.WOOD_AXE) || item.getType().equals(Material.STONE_AXE) || item.getType().equals(Material.IRON_AXE) || item.getType().equals(Material.GOLD_AXE) || item.getType().equals(Material.DIAMOND_AXE);
    }


}
