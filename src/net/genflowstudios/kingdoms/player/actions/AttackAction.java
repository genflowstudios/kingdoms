package net.genflowstudios.kingdoms.player.actions;

import net.genflowstudios.kingdoms.items.items.armor.ArmorTypes;
import net.genflowstudios.kingdoms.items.items.weapons.WeaponList;
import net.genflowstudios.kingdoms.player.KPlayerChecker;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.events.PlayerAddExpEvent;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Samuel on 6/18/2015.
 */
public class AttackAction implements Listener{


    @EventHandler
    public void onPlayerAttack(EntityDamageByEntityEvent event){
        KPlayerChecker checker = new KPlayerChecker();
        if(event.getDamager() instanceof Player){
            Player player = (Player) event.getDamager();
            if(event.getEntity() instanceof Player){
                Player damaged = (Player) event.getEntity();
                if(checker.isAlly(player.getUniqueId(), damaged.getUniqueId())){
                    player.sendMessage("That player is an ally of your area! You can not damage allies!");
                    event.setCancelled(true);
                }else{
                    double damage = getDamageValue(player.getUniqueId(), damaged.getUniqueId(), getWeapon(player.getItemInHand(), event.getDamage()));
                    System.out.println(damage);
                    addExp(player.getUniqueId(), Math.abs(event.getDamage() * .05));
                    if(damage < 0){
                        if(player.getItemInHand().getType().equals(Material.AIR)){
                            event.setDamage(4);
                            System.out.println(event.getDamage());
                        }else{
                            event.setDamage(0);
                        }
                    }else{
                        event.setDamage(damage);
                    }
                }

            }else{
                addExp(player.getUniqueId(), Math.abs(event.getDamage() * .02));
            }
        }
    }

    private double getWeapon(ItemStack itemInHand, double baseDamage){
        if(itemInHand.hasItemMeta()){
            if(itemInHand.getItemMeta().hasDisplayName()){
                for(WeaponList weapon : WeaponList.getWeapons()){
                    if(weapon.getName().equalsIgnoreCase(itemInHand.getItemMeta().getDisplayName())){
                        System.out.println(weapon.getWeaponMaterial().getBaseDamage(weapon.getWeaponType()));
                        return weapon.getWeaponMaterial().getBaseDamage(weapon.getWeaponType());
                    }
                }
            }
        }
        return baseDamage;
    }

    @EventHandler
    public void onPlayerDeath(EntityDeathEvent event){
        if(event.getEntity() instanceof Player){

            Player player = (Player) event.getEntity();
            Player killer = player.getKiller();
            try{
                KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(killer.getUniqueId());
                killer.setMaxHealth(kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.HITPOINTS) * 100);
                KingdomPlayer kPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
                player.setMaxHealth(kPlayer.getStats().getSkills().getSkillLevel(SkillType.HITPOINTS) * 100);
                player.setHealthScale(20);
                addExp(killer.getUniqueId(), 5);
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

    private void addExp(UUID uuid, double amount){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            Bukkit.getPluginManager().callEvent(new PlayerAddExpEvent(uuid, SkillType.ATTACK, amount));
            Bukkit.getPluginManager().callEvent(new PlayerAddExpEvent(uuid, SkillType.STRENGTH, amount * .9));
            Bukkit.getPluginManager().callEvent(new PlayerAddExpEvent(uuid, SkillType.HITPOINTS, .3));
            kingdomPlayer.getStats().getSkills().levelUp(uuid);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public double getDamageValue(UUID uuidDamager, UUID uuidDamaged, double weaponDamage){
        int defenceLevel = getDefenceLevel(uuidDamaged);
        int strengthLevel = getStrengthLevel(uuidDamager);
        int attackLevel = getAttackLevel(uuidDamager);
        double firstDamage = Math.pow(8, .076432) * ((strengthLevel / defenceLevel) + (weaponDamage * 5));
        Random r = new Random();
        System.out.println("First Damage: " + firstDamage);
        return firstDamage;
    }


    public double getBaseDamageValue(ItemStack weapon, ItemStack[] armor, UUID damager, UUID damaged){
        double armorValue = 1;
        double damageValue = 0;
        if(isWeapon(weapon)){
            for(ItemStack item : armor){
                if(isArmor(item)){
                    armorValue += getArmorValue(item, getDefenceLevel(damaged));
                }
            }
            damageValue = getDamageValue(weapon, getAttackLevel(damager));
            int total = (int) (damageValue / armorValue);
            return total;
        }else{
            for(ItemStack item : armor){
                if(isArmor(item)){
                    armorValue += getArmorValue(item, getDefenceLevel(damaged));
                }
            }
            int total = (int) (10 / armorValue);
            return total;
        }
    }


    public boolean isWeapon(ItemStack item){
        if(item.hasItemMeta()){
            return WeaponList.getNames().contains(item.getItemMeta().getDisplayName());
        }else{
            return false;
        }
    }

    public boolean isArmor(ItemStack item){
        if(item.hasItemMeta()){
            return ArmorTypes.getNames().contains(item.getItemMeta().getDisplayName());
        }else{
            return false;
        }
    }

    public double getDamageValue(ItemStack item, int level){
        for(WeaponList type : WeaponList.getWeapons()){
            if(item.getItemMeta().getDisplayName().equalsIgnoreCase(type.getName())){

            }
        }
        return 0;
    }

    public double getArmorValue(ItemStack item, int level){
        for(ArmorTypes type : ArmorTypes.getArmor()){
            if(item.getItemMeta().getDisplayName().equalsIgnoreCase(type.getName())){
                return type.getArmorValue();
            }
        }
        return 0;
    }

    public double getMagicArmorValue(ItemStack item, int level){
        for(ArmorTypes type : ArmorTypes.getArmor()){
            if(item.getItemMeta().getDisplayName().equalsIgnoreCase(type.getName())){
                return type.getMagicArmorValue();
            }
        }
        return 0;
    }


    public int getDefenceLevel(UUID uuid){
        try{
            KingdomPlayer damaged = KingdomPlayer.getPlayer(uuid);
            return damaged.getStats().getSkills().getSkillLevel(SkillType.DEFENCE);
        }catch(Exception e){
            e.printStackTrace();
            return 1;
        }
    }


    public int getAttackLevel(UUID uuid){
        try{
            KingdomPlayer damager = KingdomPlayer.getPlayer(uuid);
            return damager.getStats().getSkills().getSkillLevel(SkillType.ATTACK);
        }catch(Exception e){
            e.printStackTrace();
            return 1;
        }
    }


    public int getStrengthLevel(UUID uuid){
        try{
            KingdomPlayer damager = KingdomPlayer.getPlayer(uuid);
            return damager.getStats().getSkills().getSkillLevel(SkillType.STRENGTH);
        }catch(Exception e){
            e.printStackTrace();
            return 1;
        }
    }


}
