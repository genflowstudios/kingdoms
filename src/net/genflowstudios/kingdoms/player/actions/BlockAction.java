package net.genflowstudios.kingdoms.player.actions;

import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.UUID;

/**
 * Created by Samuel on 6/18/2015.
 */
public class BlockAction implements Listener{

    @EventHandler
    public void onPlayerBlock(EntityDamageByEntityEvent event){
        if(event.getEntity() instanceof Player){
            Player player = (Player) event.getEntity();
            if(player.isBlocking()){
                if(event.getDamager() instanceof Player){
                    addExp(player.getUniqueId(), 4);
                }else{
                    addExp(player.getUniqueId(), 3);
                }
            }else{
                if(event.getDamager() instanceof Player){
                    addExp(player.getUniqueId(), event.getDamage() * .05);
                }else{
                    addExp(player.getUniqueId(), event.getDamage() * .03);
                }
            }
        }
    }


    private void addExp(UUID uuid, double amount){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            kingdomPlayer.getStats().getSkills().addSkillExp(SkillType.DEFENCE, amount);
            kingdomPlayer.getStats().getSkills().addSkillExp(SkillType.HITPOINTS, .3);
            kingdomPlayer.getStats().getSkills().levelUp(uuid);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
