package net.genflowstudios.kingdoms.player.actions;

import net.genflowstudios.kingdoms.player.InventoryManager;
import net.genflowstudios.kingdoms.player.KPlayerInventory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Samuel on 6/9/2015.
 */
public class CheckBagAction implements Listener{


    @EventHandler
    public void onPlayerRightClick(PlayerInteractEvent event){
        Player player = event.getPlayer();
        ItemStack item = event.getItem();
        KPlayerInventory inventory = InventoryManager.getInventory(player.getUniqueId());
        if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
            if(item.getType().equals(inventory.getLumberBag().getBagMaterial().getType())){
                if(item.getItemMeta().hasDisplayName()){
                    if(item.getItemMeta().getDisplayName().equals(inventory.getLumberBag().getBagMaterial().getItemMeta().getDisplayName())){
                        if(player.isSneaking()){
                            player.updateInventory();
                            if(inventory.getLumberBag().getLumberAmount() > 0){
                                emptyLumberBag(player.getUniqueId(), inventory);
                            }else{
                                fillLumberBag(player.getUniqueId(), inventory);
                            }
                        }else{
                            player.sendMessage("===[" + ChatColor.AQUA + "Bag" + ChatColor.WHITE + "]===\n" + ChatColor.AQUA + "You have " + inventory.getLumberBag().getLumberAmount() + " lumber.");
                        }

                    }
                }
            }else if(item.getType().equals(inventory.getStoneBag().getBagMaterial().getType())){
                if(item.getItemMeta().hasDisplayName()){
                    if(item.getItemMeta().getDisplayName().equals(inventory.getStoneBag().getBagMaterial().getItemMeta().getDisplayName())){
                        if(player.isSneaking()){
                            player.updateInventory();
                            if(inventory.getStoneBag().getStoneAmount() > 0){
                                emptyStoneBag(player.getUniqueId(), inventory);
                            }else{
                                fillStoneBag(player.getUniqueId(), inventory);
                            }
                        }else{
                            player.sendMessage("===[" + ChatColor.AQUA + "Bag" + ChatColor.WHITE + "]===\n" + ChatColor.AQUA + "You have " + inventory.getStoneBag().getStoneAmount() + " stone.");
                        }

                    }
                }
            }
        }
    }

    private void emptyStoneBag(UUID uuid, KPlayerInventory inventory){
        Player player = Bukkit.getPlayer(uuid);
        for(int i = inventory.getStoneBag().getStoneAmount(); i > 64; i -= 64){
            player.getInventory().addItem(new ItemStack(Material.COBBLESTONE, 64));
            inventory.getStoneBag().setStoneAmount(inventory.getStoneBag().getStoneAmount() - 64);
            player.updateInventory();
            inventory.getStoneBag().setFull(false);
        }
        if(inventory.getStoneBag().getStoneAmount() < 64){
            player.getInventory().addItem(new ItemStack(Material.COBBLESTONE, inventory.getStoneBag().getStoneAmount()));
            inventory.getStoneBag().setStoneAmount(0);
            player.updateInventory();
            inventory.getStoneBag().setFull(false);
        }

    }


    private void fillStoneBag(UUID uuid, KPlayerInventory inventory){
        Player player = Bukkit.getPlayer(uuid);
        int amount = getStoneAmount(uuid, inventory);
        if(amount > inventory.getStoneBag().getSize()){
            amount -= inventory.getStoneBag().getSize();
            inventory.getStoneBag().setStoneAmount(inventory.getStoneBag().getSize());
            checkAmount(uuid, amount, inventory);
        }else{
            int total = inventory.getStoneBag().getStoneAmount() + amount;
            inventory.getStoneBag().setStoneAmount(total);
        }
        player.updateInventory();
        player.sendMessage("===[" + ChatColor.AQUA + "Bag" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have filled your bag.");
    }


    private void checkAmount(UUID uuid, int amount, KPlayerInventory inventory){
        int temp = amount;
        Player player = Bukkit.getPlayer(uuid);
        if(temp > 64){
            for(int i = temp; i > 64; i -= 64){
                player.getInventory().addItem(new ItemStack(Material.COBBLESTONE, 64));
                temp -= 64;
            }
            if(temp > 0){
                checkAmount(uuid, temp, inventory);
            }
        }else{
            player.getInventory().addItem(new ItemStack(Material.COBBLESTONE, amount));
            temp -= amount;
        }
        player.updateInventory();
    }

    public int getStoneAmount(UUID uuid, KPlayerInventory inventory){
        Player player = Bukkit.getPlayer(uuid);
        int amount = 0;
        for(int i = 1; i <= 36; i++){
            ItemStack item = player.getInventory().getItem(i);
            if(item == null || item.getType().equals(Material.AIR)){

            }else{
                if(item.getItemMeta().hasDisplayName() == false){
                    if(item.getType().equals(Material.COBBLESTONE) || item.getType().equals(Material.STONE)){
                        amount += item.getAmount();
                        player.getInventory().remove(item);
                    }
                }
            }
        }
        player.getInventory().remove(Material.COBBLESTONE);
        player.updateInventory();
        return amount;
    }


    private void emptyLumberBag(UUID uuid, KPlayerInventory inventory){
        Player player = Bukkit.getPlayer(uuid);
        for(int i = inventory.getLumberBag().getLumberAmount(); i > 64; i -= 64){
            player.getInventory().addItem(new ItemStack(Material.LOG, 64));
            inventory.getLumberBag().setLumberAmount(inventory.getLumberBag().getLumberAmount() - 64);
            player.updateInventory();
            inventory.getLumberBag().setFull(false);
        }
        if(inventory.getLumberBag().getLumberAmount() < 64){
            player.getInventory().addItem(new ItemStack(Material.LOG, inventory.getLumberBag().getLumberAmount()));
            inventory.getLumberBag().setLumberAmount(0);
            player.updateInventory();
            inventory.getLumberBag().setFull(false);
        }

    }


    private void fillLumberBag(UUID uuid, KPlayerInventory inventory){
        Player player = Bukkit.getPlayer(uuid);
        int amount = getLumberAmount(uuid, inventory);
        if(amount > inventory.getLumberBag().getSize()){
            amount -= inventory.getLumberBag().getSize();
            inventory.getLumberBag().setLumberAmount(inventory.getLumberBag().getSize());
            checkLumberAmount(uuid, amount, inventory);
        }else{
            int total = inventory.getLumberBag().getLumberAmount() + amount;
            inventory.getLumberBag().setLumberAmount(total);
        }
        player.updateInventory();
        player.sendMessage("===[" + ChatColor.AQUA + "Bag" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have filled your bag.");
    }


    private void checkLumberAmount(UUID uuid, int amount, KPlayerInventory inventory){
        int temp = amount;
        Player player = Bukkit.getPlayer(uuid);
        if(temp > 64){
            for(int i = temp; i > 64; i -= 64){
                player.getInventory().addItem(new ItemStack(Material.LOG, 64));
                temp -= 64;
            }
            if(temp > 0){
                checkLumberAmount(uuid, temp, inventory);
            }
        }else{
            player.getInventory().addItem(new ItemStack(Material.LOG, amount));
        }
        player.updateInventory();
    }

    public int getLumberAmount(UUID uuid, KPlayerInventory inventory){
        Player player = Bukkit.getPlayer(uuid);
        int amount = 0;
        for(int i = 1; i <= 36; i++){
            ItemStack item = player.getInventory().getItem(i);
            if(item == null || item.getType().equals(Material.AIR)){

            }else{
                if(item.getItemMeta().hasDisplayName() == false){
                    if(item.getType().equals(Material.LOG) || item.getType().equals(Material.LOG_2)){
                        amount += item.getAmount();
                    }
                }
            }
        }
        for(int i = 1; i <= 36; i++){
            ItemStack item = player.getInventory().getItem(i);
            if(item == null || item.getType().equals(Material.AIR)){

            }else{
                if(item.getItemMeta().hasDisplayName() == false){
                    if(item.getType().equals(Material.LOG) || item.getType().equals(Material.LOG_2)){
                        player.getInventory().remove(item);
                    }
                }
            }
        }
        player.updateInventory();
        return amount;
    }

}
