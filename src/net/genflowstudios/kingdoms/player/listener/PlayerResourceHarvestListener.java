package net.genflowstudios.kingdoms.player.listener;

import net.genflowstudios.kingdoms.events.ResourceGainEvent;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.PlayerLocationManager;
import net.genflowstudios.kingdoms.player.InventoryManager;
import net.genflowstudios.kingdoms.player.KPlayerInventory;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class PlayerResourceHarvestListener implements Listener{

    @EventHandler
    public void onResourceHarvest(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getPlayer().getItemInHand().getType().equals(Material.AIR)){

        }else{
            KPlayerInventory inventory = InventoryManager.getInventory(player.getUniqueId());
            if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                if(inventory.getPlayerTools().isHarvestTool()){
                    if(isHatchet(event.getItem())){
                        if(canHarvest(player.getUniqueId())){
                            Block clickedBlock = event.getClickedBlock();
                            if(canBreakBlock(clickedBlock)){
                                Material item = clickedBlock.getType();
                                if(item.equals(Material.LOG) || item.equals(Material.LOG_2)){
                                    if(inventory.getLumberBag().isFull()){
                                        player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "Your lumber bag is full! Empty it in order to collect more!");
                                    }else{
                                        Bukkit.getPluginManager().callEvent(new ResourceGainEvent(player.getUniqueId(), Material.LOG));
                                    }

                                }else{

                                }
                            }else{
                                player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "The block must be in a mine harvesting zone to harvest.");
                            }
                        }else{
                            player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "You must be in a lumber harvesting zone to harvest.");
                        }
                    }else if(isPickaxe(event.getItem())){
                        if(canHarvest(player.getUniqueId())){
                            if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
                                Block clickedBlock = event.getClickedBlock();
                                if(canBreakBlock(clickedBlock)){
                                    Material item = clickedBlock.getType();
                                    if(item.equals(Material.STONE) || item.equals(Material.COAL_ORE) || item.equals(Material.COBBLESTONE) || item.equals(Material.IRON_ORE) || item.equals(Material.GOLD_ORE) || item.equals(Material.DIAMOND_ORE) || item.equals(Material.REDSTONE_ORE) || item.equals(Material.GLOWING_REDSTONE_ORE)){
                                        if(item.equals(Material.GLOWING_REDSTONE_ORE)){
                                            Bukkit.getPluginManager().callEvent(new ResourceGainEvent(player.getUniqueId(), Material.REDSTONE_ORE));
                                        }else if(item.equals(Material.COBBLESTONE)){
                                            Bukkit.getPluginManager().callEvent(new ResourceGainEvent(player.getUniqueId(), Material.STONE));
                                        }else{
                                            Bukkit.getPluginManager().callEvent(new ResourceGainEvent(player.getUniqueId(), item));
                                        }
                                    }else{

                                    }
                                }else{
                                    player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "The block must be in a mine harvesting zone to harvest.");
                                }
                            }
                        }else{
                            player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "You must be in a mine harvesting zone to harvest.");
                        }
                    }else{

                    }
                }else{

                }
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event){
        Player player = event.getPlayer();
        Block block = event.getBlock();
        KPlayerInventory inventory = InventoryManager.getInventory(player.getUniqueId());
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            if(PlayerLocationManager.canPlayerHarvest(player.getUniqueId())){

            }else{
                event.setCancelled(true);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    private boolean canBreakBlock(Block block){
        return PlayerLocationManager.getBreakBlock(block);
    }

    private boolean canHarvest(UUID uuid){
        return PlayerLocationManager.canPlayerHarvest(uuid);
    }


    private boolean isPickaxe(ItemStack item){
        return item.getType().equals(Material.WOOD_PICKAXE) || item.getType().equals(Material.STONE_PICKAXE) || item.getType().equals(Material.IRON_PICKAXE) || item.getType().equals(Material.GOLD_PICKAXE) || item.getType().equals(Material.DIAMOND_PICKAXE);
    }

    private boolean isHatchet(ItemStack item){
        return item.getType().equals(Material.WOOD_AXE) || item.getType().equals(Material.STONE_AXE) || item.getType().equals(Material.IRON_AXE) || item.getType().equals(Material.GOLD_AXE) || item.getType().equals(Material.DIAMOND_AXE);
    }


}

