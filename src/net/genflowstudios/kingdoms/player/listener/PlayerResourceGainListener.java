package net.genflowstudios.kingdoms.player.listener;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.events.ResourceGainEvent;
import net.genflowstudios.kingdoms.items.harvesting.HarvestingItem;
import net.genflowstudios.kingdoms.player.KPlayerInventory;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.skills.KPlayerSkills;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.UUID;

public class PlayerResourceGainListener implements Listener{

    public static HashMap<UUID, HashMap<Material, Double>> playerTime = new HashMap<UUID, HashMap<Material, Double>>();

    int id;

    @EventHandler
    public void onResourceGain(ResourceGainEvent event) throws Exception{
        Material item = event.getMaterial();
        UUID uuid = event.getUUID();
        KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
        KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
        if(playerTime.containsKey(uuid) == false || playerTime.isEmpty()){
            playerTime.put(uuid, new HashMap<Material, Double>());
        }
        if(HarvestingItem.getMaterials().contains(item)){
            HarvestingItem harvestingItem = HarvestingItem.getHarvestingItem(item);
            if(skills.getSkillLevel(harvestingItem.getSkill()) >= harvestingItem.getHarvestLevel()){
                addItem(item, uuid, harvestingItem, skills);
            }else{
                Bukkit.getPlayer(uuid).sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You need a " + harvestingItem.getSkill().getName() + " level of " + harvestingItem.getHarvestLevel() + " to " + harvestingItem.getSkill().getActionName() + " that.");
            }
        }

    }


    public void addItem(Material item, UUID uuid, HarvestingItem harvestingItem, KPlayerSkills skills) throws Exception{
        KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
        KPlayerInventory inventory = kingdomPlayer.getInventory();
        HashMap<Material, Double> times = playerTime.get(uuid);
        if(playerTime.get(uuid).containsKey(item)){
            Bukkit.getPlayer(uuid).sendMessage("===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "You must wait " + playerTime.get(uuid).get(item) + " seconds in order to harvest that again.");
        }else{
            if(inventory.isStone(item)){
                inventory.getStoneBag().addMaterial(HarvestingItem.getAmountHarvested(skills.getSkillLevel(harvestingItem.getSkill())));
                Bukkit.getPlayer(uuid).sendMessage("===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have " + harvestingItem.getSkill().getActionName() + "d " + HarvestingItem.getAmountHarvested(skills.getSkillLevel(harvestingItem.getSkill())) + " " + harvestingItem.getName() + ".");
                times.put(item, HarvestingItem.getTimeTillHarvest(skills.getSkillLevel(harvestingItem.getSkill()), harvestingItem.getAmountTimes()));
                playerTime.remove(uuid);
                playerTime.put(uuid, times);
                skills.addSkillExp(harvestingItem.getSkill(), harvestingItem.getAwardedExp());
                skills.levelUp(uuid);
                new Timer(uuid, item);
            }else{
                inventory.getLumberBag().addMaterial(HarvestingItem.getAmountHarvested(skills.getSkillLevel(harvestingItem.getSkill())));
                Bukkit.getPlayer(uuid).sendMessage("===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have " + harvestingItem.getSkill().getActionName() + "ped " + HarvestingItem.getAmountHarvested(skills.getSkillLevel(harvestingItem.getSkill())) + " " + harvestingItem.getName() + ".");
                times.put(item, HarvestingItem.getTimeTillHarvest(skills.getSkillLevel(harvestingItem.getSkill()), harvestingItem.getAmountTimes()));
                playerTime.remove(uuid);
                playerTime.put(uuid, times);
                skills.addSkillExp(harvestingItem.getSkill(), harvestingItem.getAwardedExp());
                skills.levelUp(uuid);
                new Timer(uuid, item);
            }
        }
    }


    class Timer{

        int id;
        double time;
        UUID uuid;
        Material item;

        public Timer(UUID uuid, Material item){
            this.time = playerTime.get(uuid).get(item);
            this.uuid = uuid;
            this.item = item;
            start();
        }

        private void start(){
            id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Engine.getInstance(), new Runnable(){
                @Override
                public void run(){
                    HashMap<Material, Double> times = playerTime.get(uuid);
                    if(time > 0){
                        time -= .05;
                        time = Double.parseDouble(new DecimalFormat("##.##").format(time));
                        playerTime.remove(uuid);
                        times.remove(item);
                        times.put(item, time);
                        playerTime.put(uuid, times);
                    }else{
                        playerTime.remove(uuid);
                        times.remove(item);
                        playerTime.put(uuid, times);
                        Bukkit.getScheduler().cancelTask(id);
                    }
                }
            }, 0, 1);
        }


    }


}
