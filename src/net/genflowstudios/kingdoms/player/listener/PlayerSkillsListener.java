package net.genflowstudios.kingdoms.player.listener;

import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.events.PlayerAddExpEvent;
import net.genflowstudios.kingdoms.player.events.PlayerLevelUpEvent;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

/**
 * Created by Samuel on 6/22/2015.
 */
public class PlayerSkillsListener implements Listener{

    @EventHandler
    public void onPlayerExpAddition(PlayerAddExpEvent event){
        UUID uuid = event.getUuid();
        double exp = event.getAmount();
        SkillType type = event.getType();
        try{
            KingdomPlayer.getPlayer(uuid).getStats().getSkills().addSkillExp(type, exp);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerLevelAddition(PlayerLevelUpEvent event){
        UUID uuid = event.getUuid();
        int amount = event.getAmount();
        SkillType type = event.getType();
        try{
            KingdomPlayer.getPlayer(uuid).getStats().getSkills().addSkillLevel(type, 1);
            KingdomPlayer.getPlayer(uuid).getStats().getSkills().removeSkillExp(type, KingdomPlayer.getPlayer(uuid).getStats().getSkills().getSkillExp(type));
            Bukkit.broadcastMessage("===[" + ChatColor.AQUA + "Announcement" + ChatColor.WHITE + "]===\n" + ChatColor.GOLD + Bukkit.getPlayer(uuid).getName() + " has leveled up in " + type.getName() + "!");
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
