package net.genflowstudios.kingdoms.player.listener;


import net.genflowstudios.kingdoms.kingdoms.areas.KingdomRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomCityManager;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomPlayerAreaManager;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomRegionManager;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener{


    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerMove(PlayerMoveEvent event){
        Player player = event.getPlayer();
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            for(KingdomRegion region : KingdomRegionManager.kingdomRegions){
                for(KingdomSubRegion subRegion : region.getOwnedRegions()){
                    if(subRegion.getRegion().contains(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())){
                        for(KingdomCity city : KingdomCityManager.kingdomCities){
                            if(city.getRegion().contains(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())){
                                kingdomPlayer.getLocation().setCity(city.getName());
                                kingdomPlayer.getLocation().setArea("**");
                                kingdomPlayer.getLocation().setSubRegion(subRegion.getName());
                                kingdomPlayer.getLocation().setRegion(region.getName());
                            }
                        }
                        for(KingdomPlayerArea area : KingdomPlayerAreaManager.kingdomPlayerAreas){
                            if(area.getRegion().contains(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())){
                                kingdomPlayer.getLocation().setCity("**");
                                kingdomPlayer.getLocation().setArea(area.getAreaName());
                                kingdomPlayer.getLocation().setSubRegion(subRegion.getName());
                                kingdomPlayer.getLocation().setRegion(region.getName());
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
