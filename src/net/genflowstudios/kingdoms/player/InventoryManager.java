package net.genflowstudios.kingdoms.player;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 6/9/2015.
 */
public class InventoryManager{


    public static ArrayList<KPlayerInventory> inventories = new ArrayList<KPlayerInventory>();


    public InventoryManager(KPlayerInventory inventory) throws Exception{
        if(exists(inventory)){
            throw new Exception("This inventory already exists!");
        }else{
            addInventory(inventory);
        }
    }

    public static KPlayerInventory getInventory(UUID owner){
        if(inventories.isEmpty()){
            return null;
        }else{
            for(KPlayerInventory inventory : inventories){
                if(inventory.getPlayerOwner().equals(owner)){
                    return inventory;
                }
            }
        }
        return null;
    }

    private void addInventory(KPlayerInventory inventory){
        inventories.add(inventory);
    }

    public void removeInventory(KPlayerInventory inventory) throws Exception{
        if(exists(inventory)){
            inventories.remove(inventory);
        }else{
            throw new Exception("That inventory does not exist!");
        }
    }

    public boolean exists(KPlayerInventory inventory){
        if(inventories.isEmpty()){
            return false;
        }else{
            for(KPlayerInventory i : inventories){
                if(i.getPlayerOwner().equals(inventory.getPlayerOwner())){
                    return true;
                }
            }
        }
        return false;
    }

    public boolean exists(UUID owner){

        return false;
    }


}
