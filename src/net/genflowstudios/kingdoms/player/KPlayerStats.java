package net.genflowstudios.kingdoms.player;

import net.genflowstudios.kingdoms.economy.EconData;
import net.genflowstudios.kingdoms.player.skills.KPlayerSkills;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.Bukkit;

import java.util.ArrayList;

public class KPlayerStats{


    public static ArrayList<KPlayerStats> playerstats = new ArrayList<KPlayerStats>();

    KingdomPlayer player;
    int kills, deaths;
    float kdRatio;
    KPlayerStats stats;
    String areaName;
    KPlayerAttributes attributes;
    KPlayerSkills skills;
    EconData econData;


    public KPlayerStats(KingdomPlayer player){
        if(exists(player)){
            this.econData = getEconData();
            this.attributes = getAttributes();
            this.kills = getKills();
            this.deaths = getDeaths();
            this.player = getPlayer();
            this.skills = getSkills();
            player.getPlayer().setMaxHealth(getSkills().getSkillLevel(SkillType.HITPOINTS) * 100.0);
            player.getPlayer().setHealth(getAttributes().getHealth());
            player.getPlayer().setHealthScale(20);
            getAttributes().setHealth(getPlayer().getPlayer().getHealth());
        }else{
            this.player = player;
            this.stats = this;
            this.attributes = new KPlayerAttributes(stats);
            this.skills = new KPlayerSkills();
            this.econData = new EconData();
            econData.addAccount(player.getPlayer(), 0.0);
            player.getPlayer().setMaxHealth(getSkills().getSkillLevel(SkillType.HITPOINTS) * 100.0);
            player.getPlayer().setHealth(getAttributes().getHealth());
            player.getPlayer().setHealthScale(20);
            getAttributes().setHealth(getPlayer().getPlayer().getHealth());
            this.stats = this;
            playerstats.add(stats);
        }
    }


    private boolean exists(KingdomPlayer p){
        for(KPlayerStats stats : playerstats){
            if(stats.getPlayer().getUuid().equals(p.getUuid())){
                return true;
            }
        }
        return false;
    }


    /**
     * @return the player
     */
    public KingdomPlayer getPlayer(){
        return player;
    }


    /**
     * @param player the player to set
     */
    public void setPlayer(KingdomPlayer player){
        this.player = player;
    }


    /**
     * @return the tokens
     */
    public int getTokens(){
        return econData.getTokens(player.getUuid());
    }


    /**
     * @param tokens the tokens to set
     */
    public void setTokens(int tokens){
        econData.setTokens(player.getUuid(), tokens);
    }


    /**
     * @return the bonds
     */
    public double getBonds(){
        return econData.getMoney(player.getUuid());
    }


    /**
     * @param bonds the bonds to set
     */
    public void setBonds(double bonds){
        econData.setAccountMoney(Bukkit.getOfflinePlayer(player.getUuid()), bonds);
    }


    /**
     * @return the attributes
     */
    public KPlayerAttributes getAttributes(){
        return attributes;
    }


    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(KPlayerAttributes attributes){
        this.attributes = attributes;
    }


    /**
     * @return the kills
     */
    public int getKills(){
        return kills;
    }


    /**
     * @param kills the kills to set
     */
    public void setKills(int kills){
        this.kills = kills;
    }


    /**
     * @return the deaths
     */
    public int getDeaths(){
        return deaths;
    }


    /**
     * @param deaths the deaths to set
     */
    public void setDeaths(int deaths){
        this.deaths = deaths;
    }

    /**
     * @return the econData
     */
    public EconData getEconData(){
        return econData;
    }

    /**
     * @param econData the econData to set
     */
    public void setEconData(EconData econData){
        this.econData = econData;
    }

    /**
     * @return the kdRatio
     */
    public float getKdRatio(){
        return kdRatio;
    }

    /**
     * @param kdRatio the kdRatio to set
     */
    public void setKdRatio(float kdRatio){
        this.kdRatio = kdRatio;
    }

    public String getAreaName(){
        return areaName;
    }

    public void setAreaName(String areaName){
        this.areaName = areaName;
    }


    public KPlayerSkills getSkills(){
        return skills;
    }

    public void setSkills(KPlayerSkills skills){
        this.skills = skills;
    }


}
