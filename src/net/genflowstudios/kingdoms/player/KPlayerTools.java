package net.genflowstudios.kingdoms.player;

public class KPlayerTools{


    boolean harvestTool;

    public KPlayerTools(boolean harvestTool){
        this.harvestTool = harvestTool;
    }

    /**
     * @return the harvestTool
     */
    public boolean isHarvestTool(){
        return harvestTool;
    }

    /**
     * @param harvestTool the harvestTool to set
     */
    public void setHarvestTool(boolean harvestTool){
        this.harvestTool = harvestTool;
    }


}
