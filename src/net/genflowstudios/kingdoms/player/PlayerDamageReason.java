package net.genflowstudios.kingdoms.player;

/**
 * Created by Samuel on 6/22/2015.
 */
public enum PlayerDamageReason{

    PLAYER_MAGIC_SOLO,
    PLAYER_MAGIC_AREA,
    PLAYER_PUNCH,
    PLAYER_WEAPON,
    PLAYER_ABILITY,
    PLAYER_SPELL,
    ENTITY,
    ENTITY_BOSS,
    ENVIRONMENT,
    FALLING

}
