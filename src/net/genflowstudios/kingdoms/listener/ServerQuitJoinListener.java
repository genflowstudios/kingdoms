package net.genflowstudios.kingdoms.listener;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.adapters.VaultAdapter;
import net.genflowstudios.kingdoms.data.*;
import net.genflowstudios.kingdoms.display.DisplayHandler;
import net.genflowstudios.kingdoms.player.KPlayerInventory;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

public class ServerQuitJoinListener implements Listener{

    int id;

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();
        ArrayList<ItemStack> items = new ArrayList<ItemStack>();
        for(ItemStack item : player.getInventory().getContents()){
            items.add(item);
        }
        PlayerSavingHandler.items.put(player.getUniqueId(), items);
        KingdomPlayer kPlayer;
        SettingsHandler handler = new SettingsHandler();
        if(handler.isFlatfileMode()){
            ConfigChangeHandler changeHandler = new ConfigChangeHandler();
            changeHandler.savePlayerConfigs(player.getUniqueId());
            DisplayHandler displayHandler = new DisplayHandler();
            for(Player p : Bukkit.getOnlinePlayers()){
                if(Bukkit.getOnlinePlayers().size() == 0){

                }else{
                    displayHandler.addPlayerStatsScoreboard(p);
                }
            }
        }else{
            ConfigChangeHandler changeHandler = new ConfigChangeHandler();
            changeHandler.savePlayerConfigs(player.getUniqueId());
            DisplayHandler displayHandler = new DisplayHandler();
            for(Player p : Bukkit.getOnlinePlayers()){
                if(Bukkit.getOnlinePlayers().size() == 0){

                }else{
                    displayHandler.addPlayerStatsScoreboard(p);
                }
            }
        }
        PlayerSavingHandler.players.add(player.getUniqueId());
    }


    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        player.setHealthScale(20);
        checkData(player);
        checkInventory(player);
    }


    private void checkInventory(Player player){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerInventory inventory = kingdomPlayer.getInventory();
            if(inventory.getStoneBag() == null || inventory.getStoneBag() == null){

            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void checkFiles(Player player, ConfigChangeHandler changeHandler){
        String fileName = player.getUniqueId() + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Players");
        File configFile = new File(dataFolder, fileName);
        if(configFile.exists()){

        }else{
            FileConfiguration cfgPlayer = ConfigManager.getPlayerConfig(player.getUniqueId());
            changeHandler.generatePlayerConfig(player.getUniqueId(), cfgPlayer);
        }
    }


    private void startCheck(Player player){
        final UUID uuid = player.getUniqueId();
        id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Engine.getInstance(), new Runnable(){
            @Override
            public void run(){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    try{
                        KingdomPlayer kPlayer = KingdomPlayer.getPlayer(uuid);
                        if(kPlayer.getStats().getBonds() != VaultAdapter.getEconomy().getBalance(Bukkit.getOfflinePlayer(uuid))){
                            kPlayer.getStats().setBonds(VaultAdapter.getEconomy().getBalance(Bukkit.getOfflinePlayer(uuid)));
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }else{
                    Bukkit.getScheduler().cancelTask(id);
                }

            }
        }, 60L, 20L);
    }


    private void checkData(final Player player){
        SettingsHandler settingsHandler = new SettingsHandler();
        KingdomPlayer kingdomPlayer = new KingdomPlayer(player.getUniqueId());
        kingdomPlayer.getLocation().setArea("Wilderness");
        final PlayerLoadHandler loadHandler = new PlayerLoadHandler();
        checkFiles(player, new ConfigChangeHandler());
        Bukkit.getScheduler().scheduleSyncDelayedTask(Engine.getInstance(), new Runnable(){
            @Override
            public void run(){
                if(MySQLAdapter.playerDataContainsPlayer(player.getUniqueId())){
                    loadHandler.loadPlayer(player.getUniqueId());
                }else{
                    MySQLAdapter.createData(player.getUniqueId());
                }
            }
        });
        if(settingsHandler.isFlatfileMode()){
            if(KingdomPlayer.exists(player.getUniqueId())){
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(Bukkit.getOnlinePlayers().size() == 0){

                    }else{
                        DisplayHandler displayHandler = new DisplayHandler();
                        displayHandler.addPlayerStatsScoreboard(p);

                    }

                }
            }else{
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(Bukkit.getOnlinePlayers().size() == 0){

                    }else{
                        DisplayHandler displayHandler = new DisplayHandler();
                        displayHandler.addPlayerStatsScoreboard(p);

                    }

                }

            }

        }else{
            if(KingdomPlayer.exists(player.getUniqueId())){
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(Bukkit.getOnlinePlayers().size() == 0){

                    }else{
                        DisplayHandler displayHandler = new DisplayHandler();
                        displayHandler.addPlayerStatsScoreboard(p);

                    }

                }
            }else{
                for(Player p : Bukkit.getOnlinePlayers()){
                    if(Bukkit.getOnlinePlayers().size() == 0){

                    }else{
                        DisplayHandler displayHandler = new DisplayHandler();
                        displayHandler.addPlayerStatsScoreboard(p);

                    }

                }
            }
        }
        startCheck(player);

    }
}
