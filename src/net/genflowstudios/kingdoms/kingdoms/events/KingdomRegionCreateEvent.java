package net.genflowstudios.kingdoms.kingdoms.events;

import net.genflowstudios.kingdoms.events.KingdomEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class KingdomRegionCreateEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    String name;
    boolean cancel = false;

    public KingdomRegionCreateEvent(Player player, String name){
        this.name = name;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public String getName(){
        return name;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

}
