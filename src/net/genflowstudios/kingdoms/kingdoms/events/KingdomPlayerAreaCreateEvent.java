package net.genflowstudios.kingdoms.kingdoms.events;

import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import net.genflowstudios.kingdoms.adapters.WorldEditAdapter;
import net.genflowstudios.kingdoms.events.KingdomEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class KingdomPlayerAreaCreateEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    Player player;
    String name;
    Polygonal2DSelection selection;
    boolean cancel = false;

    public KingdomPlayerAreaCreateEvent(Player player, String name){
        this.player = player;
        this.name = name;
        WorldEditAdapter adapter = new WorldEditAdapter();
        if(adapter.getWorldEdit().getSelection(player) instanceof Polygonal2DSelection){
            this.selection = (Polygonal2DSelection) adapter.getWorldEdit().getSelection(player);
        }
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public Player getPlayer(){
        return player;
    }

    public String getName(){
        return name;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

    public Polygonal2DSelection getSelection(){
        return selection;
    }
}
