package net.genflowstudios.kingdoms.kingdoms.events;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.kingdoms.listeners.CityCaptureListener;
import net.genflowstudios.kingdoms.kingdoms.listeners.KingdomRegionCreateListener;
import net.genflowstudios.kingdoms.kingdoms.listeners.KingdomSubRegionCreateListener;
import org.bukkit.Bukkit;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomEventManager{


    public void registerEvents(){
        Bukkit.getPluginManager().registerEvents(new CityCaptureListener(), Engine.getInstance());
        Bukkit.getPluginManager().registerEvents(new KingdomRegionCreateListener(), Engine.getInstance());
        Bukkit.getPluginManager().registerEvents(new KingdomSubRegionCreateListener(), Engine.getInstance());


    }


}
