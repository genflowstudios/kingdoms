package net.genflowstudios.kingdoms.kingdoms.events;

import net.genflowstudios.kingdoms.events.KingdomEvent;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class CityCaptureEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    KingdomCity city;
    Guild capturer, owner;
    boolean cancel = false;

    public CityCaptureEvent(KingdomCity city, Guild guild){
        this.city = city;
        if(city.isOwned()){
            this.owner = city.getController();
            this.capturer = guild;
        }else{
            this.owner = Guild.getNPC();
            this.capturer = guild;
        }
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }


    public KingdomCity getCity(){
        return city;
    }

    public Guild getCapturer(){
        return capturer;
    }

    public Guild getOwner(){
        return owner;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

}
