package net.genflowstudios.kingdoms.kingdoms.events;

import net.genflowstudios.kingdoms.events.KingdomEvent;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class PlayerAreaCaptureEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    Guild capturer, owner;
    KingdomPlayerArea region;
    boolean cancel = false;

    public PlayerAreaCaptureEvent(KingdomPlayerArea region, Guild guild){
        this.region = region;
        this.owner = region.getOwner();
        this.capturer = guild;

    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public KingdomPlayerArea getRegion(){
        return region;
    }

    public Guild getCapturer(){
        return capturer;
    }

    public Guild getOwner(){
        return owner;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

}
