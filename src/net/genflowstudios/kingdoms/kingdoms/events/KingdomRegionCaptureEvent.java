package net.genflowstudios.kingdoms.kingdoms.events;

import net.genflowstudios.kingdoms.events.KingdomEvent;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomRegion;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class KingdomRegionCaptureEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    Guild capturer, owner;
    KingdomRegion region;
    boolean cancel = false;

    public KingdomRegionCaptureEvent(KingdomRegion region, Guild guild){
        this.region = region;
        if(region.isControlled()){
            this.owner = region.getController();
            this.capturer = guild;
        }else{
            this.owner = Guild.getNPC();
            this.capturer = guild;
        }
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public KingdomRegion getRegion(){
        return region;
    }

    public Guild getCapturer(){
        return capturer;
    }

    public Guild getOwner(){
        return owner;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

}
