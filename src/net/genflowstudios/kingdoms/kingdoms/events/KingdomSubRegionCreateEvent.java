package net.genflowstudios.kingdoms.kingdoms.events;

import com.sk89q.worldedit.bukkit.selections.Selection;
import net.genflowstudios.kingdoms.adapters.WorldEditAdapter;
import net.genflowstudios.kingdoms.events.KingdomEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

public class KingdomSubRegionCreateEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();

    Player player;
    Selection selection;
    String name, region;
    boolean cancel = false;

    public KingdomSubRegionCreateEvent(Player player, String name, String region){
        this.player = player;
        this.name = name;
        this.region = region;
        WorldEditAdapter worldEditAdapter = new WorldEditAdapter();
        this.selection = worldEditAdapter.getWorldEdit().getSelection(player);
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    public Player getPlayer(){
        return player;
    }

    public Selection getSelection(){
        return selection;
    }

    public String getName(){
        return name;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }


    @Override
    public void callEvent(){
        // TODO Auto-generated method stub

    }

    public String getRegion(){
        return region;
    }
}
