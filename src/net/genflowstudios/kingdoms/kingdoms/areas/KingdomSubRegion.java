package net.genflowstudios.kingdoms.kingdoms.areas;

import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomSubRegionManager;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import org.bukkit.Bukkit;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomSubRegion{

    String name;
    Guild controller;
    boolean controlled;

    KingdomRegion kingdomRegion;
    ProtectedPolygonalRegion region;

    ArrayList<KingdomPlayerArea> ownedAreas;
    ArrayList<KingdomCity> ownedCities;


    public KingdomSubRegion(KingdomRegion kingdomRegion, ArrayList<KingdomPlayerArea> ownedAreas, ArrayList<KingdomCity> ownedCities, String name, Guild controller, boolean controlled, ProtectedPolygonalRegion region){
        this.kingdomRegion = kingdomRegion;
        this.region = region;
        this.ownedAreas = ownedAreas;
        this.ownedCities = ownedCities;
        this.name = name;
        this.controller = controller;
        this.controlled = controlled;
        try{
            KingdomSubRegionManager.addKingdomRegion(this);
            KingdomSubRegionManager.generateConfigFile(this);
        }catch(Exception e){
            e.printStackTrace();
        }
        Bukkit.broadcastMessage("A new kingdom sub-region has been created!");
    }


    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public Guild getController(){
        return controller;
    }

    public void setController(Guild controller){
        this.controller = controller;
    }

    public boolean isControlled(){
        return controlled;
    }

    public void setControlled(boolean controlled){
        this.controlled = controlled;
    }

    public KingdomRegion getKingdomRegion(){
        return kingdomRegion;
    }

    public void setKingdomRegion(KingdomRegion kingdomRegion){
        this.kingdomRegion = kingdomRegion;
    }

    public ProtectedPolygonalRegion getRegion(){
        return region;
    }

    public void setRegion(ProtectedPolygonalRegion region){
        this.region = region;
    }

    public ArrayList<KingdomPlayerArea> getOwnedAreas(){
        return ownedAreas;
    }

    public void setOwnedAreas(ArrayList<KingdomPlayerArea> ownedAreas){
        this.ownedAreas = ownedAreas;
    }

    public ArrayList<KingdomCity> getOwnedCities(){
        return ownedCities;
    }

    public void setOwnedCities(ArrayList<KingdomCity> ownedCities){
        this.ownedCities = ownedCities;
    }
}
