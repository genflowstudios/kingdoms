package net.genflowstudios.kingdoms.kingdoms.areas.cities.buildings;

/**
 * Created by Samuel on 7/19/2015.
 */
public class CityBuilding{


    int buildingTier;
    CityBuildingType type;
    double buildingHealth;
    boolean underConstruction, finished;

    public CityBuilding(CityBuildingType type, int buildingTier, double buildingHealth, boolean underConstruction, boolean finished){
        this.type = type;
        this.buildingTier = buildingTier;
        this.buildingHealth = buildingHealth;
        this.underConstruction = underConstruction;
        this.finished = finished;
    }

    public CityBuildingType getType(){
        return type;
    }

    public void setType(CityBuildingType type){
        this.type = type;
    }

    public int getBuildingTier(){
        return buildingTier;
    }

    public void setBuildingTier(int buildingTier){
        this.buildingTier = buildingTier;
    }

    public double getBuildingHealth(){
        return buildingHealth;
    }

    public void setBuildingHealth(double buildingHealth){
        this.buildingHealth = buildingHealth;
    }

    public boolean isUnderConstruction(){
        return underConstruction;
    }

    public void setUnderConstruction(boolean underConstruction){
        this.underConstruction = underConstruction;
    }

    public boolean isFinished(){
        return finished;
    }

    public void setFinished(boolean finished){
        this.finished = finished;
    }
}
