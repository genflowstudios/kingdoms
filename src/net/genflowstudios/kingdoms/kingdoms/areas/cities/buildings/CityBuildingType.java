package net.genflowstudios.kingdoms.kingdoms.areas.cities.buildings;

/**
 * Created by Samuel on 7/19/2015.
 */
public enum CityBuildingType{

    CITY_HALL,
    BARRACKS,
    TOWER,
    FORGE,
    MARKET,
    MINE,
    LUMBERMILL


}
