package net.genflowstudios.kingdoms.kingdoms.areas.cities;

import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.buildings.CityBuilding;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomCity{


    String name;
    ProtectedPolygonalRegion region;
    boolean isOwned;
    Guild controller;
    KCityTier tier;
    ArrayList<UUID> citizens;
    ArrayList<CityBuilding> cityBuildings;


    public KingdomCity(String name, boolean isOwned, Guild controller, KCityTier tier, ArrayList<UUID> citizens, ArrayList<CityBuilding> cityBuildings, ProtectedPolygonalRegion region){
        this.name = name;
        this.region = region;
        this.isOwned = isOwned;
        this.controller = controller;
        this.tier = tier;
        this.citizens = citizens;
        this.cityBuildings = cityBuildings;
    }


    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public ProtectedPolygonalRegion getRegion(){
        return region;
    }

    public void setRegion(ProtectedPolygonalRegion region){
        this.region = region;
    }

    public boolean isOwned(){
        return isOwned;
    }

    public void setIsOwned(boolean isOwned){
        this.isOwned = isOwned;
    }

    public Guild getController(){
        return controller;
    }

    public void setController(Guild controller){
        this.controller = controller;
    }

    public KCityTier getTier(){
        return tier;
    }

    public void setTier(KCityTier tier){
        this.tier = tier;
    }

    public ArrayList<UUID> getCitizens(){
        return citizens;
    }

    public void setCitizens(ArrayList<UUID> citizens){
        this.citizens = citizens;
    }

    public ArrayList<CityBuilding> getCityBuildings(){
        return cityBuildings;
    }

    public void setCityBuildings(ArrayList<CityBuilding> cityBuildings){
        this.cityBuildings = cityBuildings;
    }
}
