package net.genflowstudios.kingdoms.kingdoms.areas.managers;

import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.genflowstudios.kingdoms.adapters.WorldGuardAdapter;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class PlayerLocationManager{


    public static boolean canPlayerHarvest(UUID uuid){
        Player player = Bukkit.getPlayer(uuid);
        WorldGuardAdapter adapter = new WorldGuardAdapter();
        RegionContainer container = adapter.getWorldGuard().getRegionContainer();
        RegionManager regions = container.get(player.getWorld());
        for(ProtectedRegion region : regions.getApplicableRegions(player.getLocation())){
            for(KingdomPlayerArea playerArea : KingdomPlayerAreaManager.kingdomPlayerAreas){
                if(playerArea.getRegion().equals(region)){
                    return false;
                }
            }
            for(KingdomCity city : KingdomCityManager.kingdomCities){
                if(city.getRegion().equals(region)){
                    return false;
                }
            }
        }
        return true;
    }


    public static boolean getBreakBlock(Block block){
        WorldGuardAdapter adapter = new WorldGuardAdapter();
        RegionContainer container = adapter.getWorldGuard().getRegionContainer();
        RegionManager regions = container.get(block.getWorld());
        for(ProtectedRegion region : regions.getApplicableRegions(block.getLocation())){
            for(KingdomPlayerArea playerArea : KingdomPlayerAreaManager.kingdomPlayerAreas){
                if(playerArea.getRegion().equals(region)){
                    return false;
                }
            }
            for(KingdomCity city : KingdomCityManager.kingdomCities){
                if(city.getRegion().equals(region)){
                    return false;
                }
            }
        }
        return true;

    }


}
