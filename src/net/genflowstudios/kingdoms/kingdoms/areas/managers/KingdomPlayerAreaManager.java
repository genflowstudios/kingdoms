package net.genflowstudios.kingdoms.kingdoms.areas.managers;

import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomPlayerAreaManager{


    public static ArrayList<KingdomPlayerArea> kingdomPlayerAreas = new ArrayList<KingdomPlayerArea>();


    public static KingdomPlayerArea getRegion(String id){
        if(kingdomPlayerAreas.isEmpty()){
            return null;
        }else{
            for(KingdomPlayerArea kingdomRegion : kingdomPlayerAreas){
                if(kingdomRegion.getAreaName().equals(id)){
                    return kingdomRegion;
                }
            }
            return null;
        }

    }


    public static void addKingdomPlayerArea(KingdomPlayerArea region) throws Exception{
        if(contains(region)){
            throw new Exception("That player area already exists!");
        }else{
            kingdomPlayerAreas.add(region);
        }
    }

    public static void removeKingdomPlayerArea(KingdomPlayerArea region) throws Exception{
        if(contains(region)){
            kingdomPlayerAreas.remove(region);
        }else{
            throw new Exception("That player area does not exist!");
        }
    }

    public static boolean contains(KingdomPlayerArea region){
        if(kingdomPlayerAreas.isEmpty()){
            return false;
        }else{
            return kingdomPlayerAreas.contains(region);
        }
    }


}
