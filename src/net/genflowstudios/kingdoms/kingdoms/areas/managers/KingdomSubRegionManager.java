package net.genflowstudios.kingdoms.kingdoms.areas.managers;

import net.genflowstudios.kingdoms.data.ConfigManager;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomSubRegionManager{


    public static ArrayList<KingdomSubRegion> kingdomSubRegions = new ArrayList<KingdomSubRegion>();


    public static KingdomSubRegion getRegion(String id){
        if(kingdomSubRegions.isEmpty()){
            return null;
        }else{
            for(KingdomSubRegion kingdomRegion : kingdomSubRegions){
                if(kingdomRegion.getName().equals(id)){
                    return kingdomRegion;
                }
            }
            return null;
        }

    }


    public static void addKingdomRegion(KingdomSubRegion region) throws Exception{
        if(contains(region)){
            throw new Exception("That region already exists!");
        }else{
            kingdomSubRegions.add(region);
        }
    }

    public static void removeKingdomRegion(KingdomSubRegion region) throws Exception{
        if(contains(region)){
            kingdomSubRegions.remove(region);
        }else{
            throw new Exception("That region does not exist!");
        }
    }

    public static boolean contains(KingdomSubRegion region){
        if(kingdomSubRegions.isEmpty()){
            return false;
        }else{
            return kingdomSubRegions.contains(region);
        }
    }


    public static void generateConfigFile(KingdomSubRegion region){
        FileConfiguration cfgRegion = ConfigManager.getKingdomRegionConfig(region.getKingdomRegion().getName());
        FileConfiguration cfg = ConfigManager.getSubRegionConfig(region.getName());
        List<String> names = cfgRegion.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getStringList("Names");
        names.add(region.getName());
        cfgRegion.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").set("Names", names);
        ConfigManager.saveKingdomRegionConfig(region.getKingdomRegion().getName(), cfgRegion);
        cfg.createSection("Owned-Regions");
        cfg.createSection("Points");
        for(int i = 1; i < region.getRegion().getPoints().size(); i++){
            cfg.getConfigurationSection("Points").createSection("" + i);
            cfg.getConfigurationSection("Points").getConfigurationSection("" + i).set("X-Pos", region.getRegion().getPoints().get(i).getBlockX());
            cfg.getConfigurationSection("Points").getConfigurationSection("" + i).set("Z-Pos", region.getRegion().getPoints().get(i).getBlockZ());
        }
        cfg.getConfigurationSection("Points").set("Y-Max", region.getRegion().getMaximumPoint().getBlockY());
        cfg.getConfigurationSection("Points").set("Y-Min", region.getRegion().getMinimumPoint().getBlockY());
        cfg.getConfigurationSection("Owned-Regions").createSection("Cities");
        cfg.getConfigurationSection("Owned-Regions").createSection("Player-Areas");
        cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Cities").set("Names", new ArrayList<String>());
        cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Player-Areas").set("Names", new ArrayList<String>());
        ConfigManager.saveSubRegionConfig(region.getName(), cfg);
    }


}
