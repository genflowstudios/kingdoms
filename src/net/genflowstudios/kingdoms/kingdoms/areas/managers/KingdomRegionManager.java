package net.genflowstudios.kingdoms.kingdoms.areas.managers;

import net.genflowstudios.kingdoms.data.ConfigManager;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomRegionManager{


    public static ArrayList<KingdomRegion> kingdomRegions = new ArrayList<KingdomRegion>();


    public static KingdomRegion getRegion(String id){
        if(kingdomRegions.isEmpty()){
            return null;
        }else{
            for(KingdomRegion kingdomRegion : kingdomRegions){
                if(kingdomRegion.getName().equals(id)){
                    return kingdomRegion;
                }
            }
            return null;
        }

    }


    public static void addKingdomRegion(KingdomRegion region) throws Exception{
        if(contains(region)){
            throw new Exception("That region already exists!");
        }else{
            kingdomRegions.add(region);
        }
    }

    public static void removeKingdomRegion(KingdomRegion region) throws Exception{
        if(contains(region)){
            kingdomRegions.remove(region);
        }else{
            throw new Exception("That region does not exist!");
        }
    }

    public static boolean contains(KingdomRegion region){
        if(kingdomRegions.isEmpty()){
            return false;
        }else{
            return kingdomRegions.contains(region);
        }
    }


    public static void generateConfigFile(KingdomRegion region){
        ArrayList<String> subRegions = new ArrayList<String>();
        ArrayList<String> cities = new ArrayList<String>();
        ArrayList<String> playerOwned = new ArrayList<String>();
        for(KingdomSubRegion subRegion : region.getOwnedRegions()){
            subRegions.add(subRegion.getName());
            for(KingdomCity city : subRegion.getOwnedCities()){
                cities.add(city.getName());
            }
            for(KingdomPlayerArea area : subRegion.getOwnedAreas()){
                playerOwned.add(area.getAreaName());
            }
        }
        FileConfiguration cfg = ConfigManager.getKingdomRegionConfig(region.getName());
        cfg.createSection("Owned-Regions");
        cfg.getConfigurationSection("Owned-Regions").createSection("Sub-Regions");
        cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").set("Names", subRegions);
        for(String s : subRegions){
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").createSection(s);
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(s).set("Cities", new ArrayList<String>());
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(s).set("Player-Areas", new ArrayList<String>());

        }
        ConfigManager.saveKingdomRegionConfig(region.getName(), cfg);
        for(KingdomSubRegion subRegion : region.getOwnedRegions()){
            List<String> names = cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(subRegion.getName()).getStringList("Cities");
            for(KingdomCity city : subRegion.getOwnedCities()){
                names.add(city.getName());
            }
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(subRegion.getName()).set("Cities", names);
            List<String> names2 = cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(subRegion.getName()).getStringList("Player-Areas");
            for(KingdomPlayerArea area : subRegion.getOwnedAreas()){
                names2.add(area.getAreaName());
            }
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(subRegion.getName()).set("Player-Areas", names2);
        }
        ConfigManager.saveKingdomRegionConfig(region.getName(), cfg);
    }


    public static void saveFiles(KingdomRegion region){
        FileConfiguration cfg = ConfigManager.getKingdomRegionConfig(region.getName());
        ArrayList<String> names = new ArrayList<String>();
        ArrayList<String> cities = new ArrayList<String>();
        ArrayList<String> playerOwned = new ArrayList<String>();
        for(KingdomSubRegion subRegion : region.getOwnedRegions()){
            names.add(subRegion.getName());
            for(KingdomCity city : subRegion.getOwnedCities()){
                cities.add(city.getName());
            }
            for(KingdomPlayerArea area : subRegion.getOwnedAreas()){
                playerOwned.add(area.getAreaName());
            }
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(subRegion.getName()).set("Cities", cities);
            cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").getConfigurationSection(subRegion.getName()).set("Player-Areas", playerOwned);
        }
        cfg.getConfigurationSection("Owned-Regions").getConfigurationSection("Sub-Regions").set("Names", names);
        ConfigManager.saveKingdomRegionConfig(region.getName(), cfg);
    }

    public static void saveAllRegions(){
        for(KingdomRegion region : kingdomRegions){
            saveFiles(region);
        }
    }


}
