package net.genflowstudios.kingdoms.kingdoms.areas;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomRegionManager;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomRegion{

    String name;
    boolean controlled;
    ArrayList<KingdomSubRegion> ownedRegions;
    Guild controller;


    public KingdomRegion(String name, boolean controlled, ArrayList<KingdomSubRegion> ownedRegions, Guild controller){
        this.name = name;
        this.controlled = controlled;
        this.ownedRegions = ownedRegions;
        this.controller = controller;
        try{
            KingdomRegionManager.addKingdomRegion(this);
            KingdomRegionManager.generateConfigFile(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public boolean isControlled(){
        return controlled;
    }

    public void setControlled(boolean controlled){
        this.controlled = controlled;
    }

    public ArrayList<KingdomSubRegion> getOwnedRegions(){
        return ownedRegions;
    }

    public void setOwnedRegions(ArrayList<KingdomSubRegion> ownedRegions){
        this.ownedRegions = ownedRegions;
    }

    public Guild getController(){
        return controller;
    }

    public void setController(Guild controller){
        this.controller = controller;
    }
}
