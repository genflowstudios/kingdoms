package net.genflowstudios.kingdoms.kingdoms.areas.playerareas.buildings;

/**
 * Created by Samuel on 7/19/2015.
 */
public enum PlayerAreaBuildingType{

    TOWN_HALL,
    BARRACKS,
    TOWER,
    FORGE,
    MARKET,
    MINE,
    LUMBERMILL


}
