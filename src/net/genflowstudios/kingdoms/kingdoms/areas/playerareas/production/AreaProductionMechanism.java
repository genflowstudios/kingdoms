package net.genflowstudios.kingdoms.kingdoms.areas.playerareas.production;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.landmarks.LandmarkType;
import org.bukkit.Bukkit;

/**
 * Created by Samuel on 7/20/2015.
 */
public class AreaProductionMechanism{

    boolean isProducing;
    AreaProductionType type;
    LandmarkType landmarkType;
    int produced;

    public AreaProductionMechanism(AreaProductionType type, LandmarkType landmarkType){
        this.type = type;
        this.landmarkType = landmarkType;
    }

    public int getProduction(){
        return this.produced;
    }

    public void startProduction(){
        this.isProducing = true;
    }

    public void stopProduction(){
        this.isProducing = false;
    }


    public void startMech(){
        if(isProducing){
            if(type.equals(AreaProductionType.MONEY)){

            }else if(type.equals(AreaProductionType.LUMBER)){
                Bukkit.getScheduler().scheduleSyncRepeatingTask(Engine.getInstance(), new Runnable(){
                    @Override
                    public void run(){
                        produced += 50;
                    }
                }, 20, 18000);
            }else if(type.equals(AreaProductionType.MINERALS)){
                Bukkit.getScheduler().scheduleSyncRepeatingTask(Engine.getInstance(), new Runnable(){
                    @Override
                    public void run(){
                        produced += 45;
                    }
                }, 20, 18000);
            }
        }else{

        }
    }


}
