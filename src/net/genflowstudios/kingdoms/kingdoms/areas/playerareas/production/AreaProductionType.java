package net.genflowstudios.kingdoms.kingdoms.areas.playerareas.production;

/**
 * Created by Samuel on 7/20/2015.
 */
public enum AreaProductionType{


    MONEY,
    LUMBER,
    MINERALS,
    POWER

}
