package net.genflowstudios.kingdoms.kingdoms.areas.playerareas;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomPlayerAreaManager;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.buildings.PlayerAreaBuilding;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class KingdomPlayerArea{


    KingdomSubRegion subRegion;

    String areaName;
    KPlayerAreaTier tier;
    Guild owner;
    ArrayList<UUID> citizens;
    ArrayList<PlayerAreaBuilding> playerAreaBuildings;
    ProtectedRegion region;


    public KingdomPlayerArea(String areaName, Guild owner, KPlayerAreaTier tier, ArrayList<UUID> citizens, ArrayList<PlayerAreaBuilding> playerAreaBuildings, ProtectedRegion region, KingdomSubRegion subRegion){
        this.areaName = areaName;
        this.tier = tier;
        this.owner = owner;
        this.citizens = citizens;
        this.playerAreaBuildings = playerAreaBuildings;
        this.region = region;
        this.subRegion = subRegion;
        try{
            KingdomPlayerAreaManager.addKingdomPlayerArea(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public Guild getOwner(){
        return owner;
    }

    public void setOwner(Guild owner){
        this.owner = owner;
    }

    public ArrayList<PlayerAreaBuilding> getPlayerAreaBuildings(){
        return playerAreaBuildings;
    }

    public void setPlayerAreaBuildings(ArrayList<PlayerAreaBuilding> playerAreaBuildings){
        this.playerAreaBuildings = playerAreaBuildings;
    }

    public KingdomSubRegion getSubRegion(){
        return subRegion;
    }

    public String getAreaName(){
        return areaName;
    }

    public void setAreaName(String areaName){
        this.areaName = areaName;
    }

    public ArrayList<UUID> getCitizens(){
        return citizens;
    }

    public void setCitizens(ArrayList<UUID> citizens){
        this.citizens = citizens;
    }

    public KPlayerAreaTier getTier(){
        return tier;
    }

    public void setTier(KPlayerAreaTier tier){
        this.tier = tier;
    }

    public ProtectedRegion getRegion(){
        return region;
    }

    public void setRegion(ProtectedRegion region){
        this.region = region;
    }
}
