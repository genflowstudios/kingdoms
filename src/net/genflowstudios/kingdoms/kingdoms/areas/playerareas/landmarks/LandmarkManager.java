package net.genflowstudios.kingdoms.kingdoms.areas.playerareas.landmarks;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/20/2015.
 */
public class LandmarkManager{


    public static ArrayList<AreaLandmark> landmarks = new ArrayList<AreaLandmark>();


    public static void addLandmark(AreaLandmark landmark) throws Exception{
        if(contains(landmark)){
            throw new Exception("That landmark already exists!");
        }else{
            landmarks.add(landmark);
        }
    }

    public static void removeLandmark(AreaLandmark landmark) throws Exception{
        if(contains(landmark)){
            landmarks.remove(landmark);
        }else{
            throw new Exception("That landmark does not exist!");
        }
    }

    public static boolean contains(AreaLandmark landmark){
        if(landmarks.isEmpty()){
            return false;
        }else{
            return landmarks.contains(landmark);
        }
    }


}
