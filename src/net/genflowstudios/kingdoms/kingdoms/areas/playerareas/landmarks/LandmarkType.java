package net.genflowstudios.kingdoms.kingdoms.areas.playerareas.landmarks;

/**
 * Created by Samuel on 7/20/2015.
 */
public enum LandmarkType{


    RUINS{
        @Override
        public int getBuildCost(int tier){
            if(tier == 1){
                return 5000;
            }else if(tier == 2){
                return 10000;
            }else if(tier == 3){
                return 20000;
            }else{
                return 0;
            }
        }
    },
    FOREST{
        @Override
        public int getBuildCost(int tier){
            if(tier == 1){
                return 100;
            }else if(tier == 2){
                return 500;
            }else if(tier == 3){
                return 1000;
            }else{
                return 0;
            }
        }
    },
    ABANDONED_LUMBER_MILL{
        @Override
        public int getBuildCost(int tier){
            if(tier == 1){
                return 2500;
            }else if(tier == 2){
                return 7500;
            }else if(tier == 3){
                return 20000;
            }else{
                return 0;
            }
        }
    },
    VEIN{
        @Override
        public int getBuildCost(int tier){
            if(tier == 1){
                return 100;
            }else if(tier == 2){
                return 500;
            }else if(tier == 3){
                return 1000;
            }else{
                return 0;
            }
        }
    },
    ABANDONED_MINE{
        @Override
        public int getBuildCost(int tier){
            if(tier == 1){
                return 300;
            }else if(tier == 2){
                return 1000;
            }else if(tier == 3){
                return 1750;
            }else{
                return 0;
            }
        }
    },
    FORGOTTEN_FORT{
        @Override
        public int getBuildCost(int tier){
            if(tier == 1){
                return 15000;
            }else if(tier == 2){
                return 25000;
            }else if(tier == 3){
                return 50000;
            }else{
                return 0;
            }
        }
    };

    public abstract int getBuildCost(int tier);


}
