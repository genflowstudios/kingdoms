package net.genflowstudios.kingdoms.kingdoms.areas.playerareas.landmarks;

import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;

/**
 * Created by Samuel on 7/20/2015.
 */
public class AreaLandmark{


    LandmarkType type;
    ProtectedPolygonalRegion region;


    public AreaLandmark(LandmarkType type, ProtectedPolygonalRegion region){
        this.type = type;
        this.region = region;
        try{
            LandmarkManager.addLandmark(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public ProtectedPolygonalRegion getRegion(){
        return region;
    }

    public void setRegion(ProtectedPolygonalRegion region){
        this.region = region;
    }

    public LandmarkType getType(){
        return type;
    }

    public void setType(LandmarkType type){
        this.type = type;
    }
}
