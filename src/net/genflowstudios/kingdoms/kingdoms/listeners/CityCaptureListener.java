package net.genflowstudios.kingdoms.kingdoms.listeners;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.events.CityCaptureEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class CityCaptureListener implements Listener{

    @EventHandler
    public void onCityCapture(CityCaptureEvent event){
        KingdomCity city = event.getCity();
        if(city.isOwned()){
            Guild owner = event.getOwner();
            Guild capturer = event.getCapturer();
            correctOwnerCity(owner, capturer, city);
            sendOwnerMessage(owner, city);
            sendCapturerMessage(capturer, city);
        }else{
            Guild capturer = event.getCapturer();
            correctCity(capturer, city);
            sendCapturerMessage(capturer, city);
        }
    }

    private void correctCity(Guild capturer, KingdomCity city){
        city.setController(capturer);
        city.setIsOwned(true);
        city.setCitizens(capturer.getMembers());
        try{
            capturer.getManager().addKingdomCity(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendCapturerMessage(Guild capturer, KingdomCity city){
        if(capturer.getMembers().isEmpty()){

        }else{
            for(UUID uuid : capturer.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The owned city " + city.getName() + " has been captured!");
                }
            }
        }
    }

    private void sendOwnerMessage(Guild owner, KingdomCity city){
        if(owner.getMembers().isEmpty()){

        }else{
            for(UUID uuid : owner.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The owned city " + city.getName() + " has been captured!");
                }
            }
        }

    }

    private void correctOwnerCity(Guild owner, Guild capturer, KingdomCity city){
        city.setController(capturer);
        city.setIsOwned(true);
        city.setCitizens(capturer.getMembers());
        try{
            capturer.getManager().addKingdomCity(city);
            owner.getManager().removeKingdomCity(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
