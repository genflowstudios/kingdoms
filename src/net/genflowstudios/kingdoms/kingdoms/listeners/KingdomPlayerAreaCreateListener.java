package net.genflowstudios.kingdoms.kingdoms.listeners;

import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import net.genflowstudios.kingdoms.adapters.VaultAdapter;
import net.genflowstudios.kingdoms.adapters.WorldGuardAdapter;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomSubRegionManager;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KPlayerAreaTier;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.buildings.PlayerAreaBuilding;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.landmarks.AreaLandmark;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.landmarks.LandmarkManager;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomPlayerAreaCreateEvent;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Samuel on 7/20/2015.
 */
public class KingdomPlayerAreaCreateListener implements Listener{


    @EventHandler
    public void onKingdomPlayerAreaCreate(KingdomPlayerAreaCreateEvent event){
        Player player = event.getPlayer();
        Polygonal2DSelection selection = event.getSelection();
        WorldGuardAdapter worldGuardAdapter = new WorldGuardAdapter();
        RegionContainer container = worldGuardAdapter.getWorldGuard().getRegionContainer();
        RegionManager manager = container.get(player.getWorld());
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            if(kingdomPlayer.isInGuild()){
                if(checkForRegion(selection.getNativePoints(), player)){
                    AreaLandmark landmark = getLandmark(selection.getNativePoints(), player);
                    VaultAdapter.getEconomy().depositPlayer(Bukkit.getOfflinePlayer(player.getUniqueId()), -landmark.getType().getBuildCost(1));
                    ProtectedPolygonalRegion protectedPolygonalRegion = new ProtectedPolygonalRegion(event.getName(), selection.getNativePoints(), 0, 256);
                    protectedPolygonalRegion.setPriority(102);
                    protectedPolygonalRegion.setFlag(DefaultFlag.GREET_MESSAGE, "===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "Now entering " + event.getName() + ".");
                    protectedPolygonalRegion.setFlag(DefaultFlag.FAREWELL_MESSAGE, "===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "Now leaving " + event.getName() + ".");
                    protectedPolygonalRegion.setFlag(DefaultFlag.PASSTHROUGH, StateFlag.State.DENY);
                    manager.addRegion(protectedPolygonalRegion);
                    ArrayList<UUID> uuids = new ArrayList<UUID>();
                    for(UUID uuid : kingdomPlayer.getGuild().getMembers()){
                        uuids.add(uuid);
                    }
                    new KingdomPlayerArea(event.getName(), kingdomPlayer.getGuild(), KPlayerAreaTier.CAMP, uuids, new ArrayList<PlayerAreaBuilding>(), protectedPolygonalRegion, KingdomSubRegionManager.getRegion(kingdomPlayer.getLocation().getSubRegion()));
                    player.sendMessage("===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You have successfully repaired this area!");
                    LandmarkManager.removeLandmark(landmark);
                }
            }else{
                player.sendMessage("===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You must be in a guild in order to repair this area!");
            }

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public boolean checkForRegion(List<BlockVector2D> blocks, Player player){
        WorldGuardAdapter worldGuardAdapter = new WorldGuardAdapter();
        RegionContainer container = worldGuardAdapter.getWorldGuard().getRegionContainer();
        RegionManager regionManager = container.get(player.getWorld());
        for(AreaLandmark landmark : LandmarkManager.landmarks){
            if(landmark.getRegion().contains(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())){
                return true;
            }
        }
        return false;
    }

    public AreaLandmark getLandmark(List<BlockVector2D> blocks, Player player){
        WorldGuardAdapter worldGuardAdapter = new WorldGuardAdapter();
        RegionContainer container = worldGuardAdapter.getWorldGuard().getRegionContainer();
        RegionManager regionManager = container.get(player.getWorld());
        for(AreaLandmark landmark : LandmarkManager.landmarks){
            if(landmark.getRegion().contains(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ())){
                return landmark;
            }
        }
        return null;
    }
}
