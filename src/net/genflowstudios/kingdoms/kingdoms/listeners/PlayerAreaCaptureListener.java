package net.genflowstudios.kingdoms.kingdoms.listeners;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.playerareas.KingdomPlayerArea;
import net.genflowstudios.kingdoms.kingdoms.events.PlayerAreaCaptureEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class PlayerAreaCaptureListener implements Listener{

    @EventHandler
    public void onCityCapture(PlayerAreaCaptureEvent event){
        KingdomPlayerArea area = event.getRegion();
        Guild owner = event.getOwner();
        Guild capturer = event.getCapturer();
        correctOwnerCity(owner, capturer, area);
        sendOwnerMessage(owner, area);
        sendCapturerMessage(capturer, area);
    }

    private void correctCity(Guild capturer, KingdomPlayerArea city){
        city.setOwner(capturer);
        city.setCitizens(capturer.getMembers());
        try{
            capturer.getManager().addKingdomPlayerArea(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendCapturerMessage(Guild capturer, KingdomPlayerArea city){
        if(capturer.getMembers().isEmpty()){

        }else{
            for(UUID uuid : capturer.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The owned area " + city.getAreaName() + " has been captured!");
                }
            }
        }
    }

    private void sendOwnerMessage(Guild owner, KingdomPlayerArea city){
        if(owner.getMembers().isEmpty()){

        }else{
            for(UUID uuid : owner.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The owned area " + city.getAreaName() + " has been captured!");
                }
            }
        }

    }

    private void correctOwnerCity(Guild owner, Guild capturer, KingdomPlayerArea city){
        city.setOwner(capturer);
        city.setCitizens(capturer.getMembers());
        try{
            capturer.getManager().addKingdomPlayerArea(city);
            owner.getManager().removeKingdomPlayerArea(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
