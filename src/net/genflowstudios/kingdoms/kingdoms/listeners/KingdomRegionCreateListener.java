package net.genflowstudios.kingdoms.kingdoms.listeners;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomRegion;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomRegionCreateEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

/**
 * Created by Samuel on 7/20/2015.
 */
public class KingdomRegionCreateListener implements Listener{


    @EventHandler
    public void onKingdomRegionCreate(KingdomRegionCreateEvent event){
        String areaName = event.getName();
        new KingdomRegion(areaName, false, new ArrayList<KingdomSubRegion>(), Guild.getNPC());
        Bukkit.broadcastMessage("A new Kingdom Region has been created.");
    }
}
