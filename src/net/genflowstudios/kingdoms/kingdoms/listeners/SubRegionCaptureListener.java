package net.genflowstudios.kingdoms.kingdoms.listeners;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomSubRegion;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomSubRegionCaptureEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class SubRegionCaptureListener implements Listener{

    @EventHandler
    public void onSubRegionCapture(KingdomSubRegionCaptureEvent event){
        KingdomSubRegion city = event.getRegion();
        if(city.isControlled()){
            Guild owner = event.getOwner();
            Guild capturer = event.getCapturer();
            correctOwnerCity(owner, capturer, city);
            sendOwnerMessage(owner, city);
            sendCapturerMessage(capturer, city);
        }else{
            Guild capturer = event.getCapturer();
            correctCity(capturer, city);
            sendCapturerMessage(capturer, city);
        }
    }

    private void correctCity(Guild capturer, KingdomSubRegion city){
        city.setController(capturer);
        city.setControlled(true);
        try{
            capturer.getManager().addKingdomSubRegion(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendCapturerMessage(Guild capturer, KingdomSubRegion city){
        if(capturer.getMembers().isEmpty()){

        }else{
            for(UUID uuid : capturer.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The sub-region " + city.getName() + " has been captured by your guild!");
                }
            }
        }
    }

    private void sendOwnerMessage(Guild owner, KingdomSubRegion city){
        if(owner.getMembers().isEmpty()){

        }else{
            for(UUID uuid : owner.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The owned sub-region " + city.getName() + " has been captured!");
                }
            }
        }

    }

    private void correctOwnerCity(Guild owner, Guild capturer, KingdomSubRegion city){
        city.setController(capturer);
        city.setControlled(true);
        try{
            capturer.getManager().addKingdomSubRegion(city);
            owner.getManager().removeKingdomSubRegion(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
