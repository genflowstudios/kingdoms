package net.genflowstudios.kingdoms.kingdoms.listeners;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.KingdomRegion;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomRegionCaptureEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.UUID;

/**
 * Created by Samuel on 7/19/2015.
 */
public class RegionCaptureListener implements Listener{

    @EventHandler
    public void onRegionCapture(KingdomRegionCaptureEvent event){
        KingdomRegion city = event.getRegion();
        if(city.isControlled()){
            Guild owner = event.getOwner();
            Guild capturer = event.getCapturer();
            correctOwnerCity(owner, capturer, city);
            sendOwnerMessage(owner, city);
            sendCapturerMessage(capturer, city);
        }else{
            Guild capturer = event.getCapturer();
            correctCity(capturer, city);
            sendCapturerMessage(capturer, city);
        }
    }

    private void correctCity(Guild capturer, KingdomRegion city){
        city.setController(capturer);
        city.setControlled(true);
        try{
            capturer.getManager().addKingdomRegion(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void sendCapturerMessage(Guild capturer, KingdomRegion city){
        if(capturer.getMembers().isEmpty()){

        }else{
            for(UUID uuid : capturer.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The region " + city.getName() + " has been captured by your guild!");
                }
            }
        }
    }

    private void sendOwnerMessage(Guild owner, KingdomRegion city){
        if(owner.getMembers().isEmpty()){

        }else{
            for(UUID uuid : owner.getMembers()){
                if(Bukkit.getOfflinePlayer(uuid).isOnline()){
                    Bukkit.getPlayer(uuid).sendMessage("The owned region " + city.getName() + " has been captured!");
                }
            }
        }

    }

    private void correctOwnerCity(Guild owner, Guild capturer, KingdomRegion city){
        city.setController(capturer);
        city.setControlled(true);
        try{
            capturer.getManager().addKingdomRegion(city);
            owner.getManager().removeKingdomRegion(city);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


}
