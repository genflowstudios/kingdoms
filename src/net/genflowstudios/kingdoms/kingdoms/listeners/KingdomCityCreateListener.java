package net.genflowstudios.kingdoms.kingdoms.listeners;

import com.sk89q.worldedit.BlockVector2D;
import com.sk89q.worldedit.bukkit.selections.Polygonal2DSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.genflowstudios.kingdoms.adapters.WorldGuardAdapter;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KCityTier;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.KingdomCity;
import net.genflowstudios.kingdoms.kingdoms.areas.cities.buildings.CityBuilding;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomCityCreateEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Samuel on 7/20/2015.
 */
public class KingdomCityCreateListener implements Listener{


    @EventHandler
    public void onKingdomCityCreate(KingdomCityCreateEvent event){
        Player player = event.getPlayer();
        String areaName = event.getName();
        String regionName = event.getRegion();
        Selection selection = event.getSelection();
        if(selection instanceof Polygonal2DSelection){
            Polygonal2DSelection polygonalSelection = (Polygonal2DSelection) selection;
            List<BlockVector2D> points = polygonalSelection.getNativePoints();
            WorldGuardAdapter worldGuardAdapter = new WorldGuardAdapter();
            if(checkForRegion(points, player)){
                ProtectedPolygonalRegion protectedPolygonalRegion = new ProtectedPolygonalRegion(areaName, points, 0, 256);
                new KingdomCity(areaName, false, Guild.getNPC(), KCityTier.ONE, new ArrayList<UUID>(), new ArrayList<CityBuilding>(), protectedPolygonalRegion);
                protectedPolygonalRegion.setPriority(102);
                protectedPolygonalRegion.setFlag(DefaultFlag.GREET_MESSAGE, "===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "Now entering " + areaName + ".");
                protectedPolygonalRegion.setFlag(DefaultFlag.FAREWELL_MESSAGE, "===[" + ChatColor.AQUA + "Area" + ChatColor.WHITE + "]===\n" + ChatColor.YELLOW + "Now leaving " + areaName + ".");
                protectedPolygonalRegion.setFlag(DefaultFlag.PASSTHROUGH, StateFlag.State.DENY);
                worldGuardAdapter.getWorldGuard().getRegionContainer().get(player.getWorld()).addRegion(protectedPolygonalRegion);
            }else{
                player.sendMessage("You can not create this city here!");
                event.setCancelled(true);
            }
        }else{
            player.sendMessage("In order to create a city please make a polygonal selection!");
            event.setCancelled(true);
        }

    }

    public boolean checkForRegion(List<BlockVector2D> blocks, Player player){
        WorldGuardAdapter worldGuardAdapter = new WorldGuardAdapter();
        RegionContainer container = worldGuardAdapter.getWorldGuard().getRegionContainer();
        RegionManager regionManager = container.get(player.getWorld());
        for(BlockVector2D blockVector2D : blocks){
            for(ProtectedRegion region : regionManager.getApplicableRegions(blockVector2D.toVector())){
                if(region.getPriority() > 101){
                    return false;
                }
            }
        }
        return true;

    }
}
