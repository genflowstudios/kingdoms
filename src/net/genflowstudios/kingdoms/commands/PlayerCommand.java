package net.genflowstudios.kingdoms.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerCommand{

    public PlayerCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("player")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("level")){
                        levelCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("skills")){
                        skillsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("admin")){
                        adminInfoCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("stats")){
                        statsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("scoreboard")){
                        scoreboardCommand(player, cmd, Label, args);
                    }else{

                    }
                }else if(args.length >= 2){
                    if(args[0].equalsIgnoreCase("level")){
                        if(args[1].equalsIgnoreCase("exp")){
                            expLevelCommand(player, cmd, Label, args);
                        }
                    }else if(args[0].equalsIgnoreCase("admin")){
                        if(args[1].equalsIgnoreCase("level")){
                            if(args[2].equalsIgnoreCase("add")){
                                addLevelCommand(player, cmd, Label, args);
                            }
                        }
                    }else{

                    }
                }else{

                }
            }
        }
    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding player commands.");
    }

    private void scoreboardCommand(Player player, Command cmd, String label, String[] args){

    }

    private void addLevelCommand(Player player, Command cmd, String label, String[] args){
        //check perms
    }

    private void adminInfoCommand(Player player, Command cmd, String label, String[] args){
        //check perms
    }

    private void expLevelCommand(Player player, Command cmd, String label, String[] args){

    }

    private void statsCommand(Player player, Command cmd, String label, String[] args){

    }

    private void skillsCommand(Player player, Command cmd, String label, String[] args){

    }

    private void levelCommand(Player player, Command cmd, String label, String[] args){

    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

}
