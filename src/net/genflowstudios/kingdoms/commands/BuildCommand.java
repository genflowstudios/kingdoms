package net.genflowstudios.kingdoms.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCommand{

    public BuildCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Build")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){

                }
            }
        }
    }

    private static void sendErrorMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Error" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You must own an area that is a settlement prior to creating a building!");
    }

    private static void sendError2Message(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Error" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "Your area must be at least a settlement before creating buildings!");
    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Build" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding build commands.");
    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

}
