package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.kingdoms.events.KingdomRegionCreateEvent;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomSubRegionCreateEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class KingdomsCommand{

    public KingdomsCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Kingdoms")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("stats")){
                        statsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("list")){
                        listCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("commands")){
                        commandsCommand(player, cmd, Label, args);
                    }else{

                    }
                }else if(args.length == 2){

                }else{
                    if(args[0].equalsIgnoreCase("region")){
                        if(args[1].equalsIgnoreCase("create")){
                            regionCreateCommand(player, cmd, Label, args);
                        }else if(args[1].equalsIgnoreCase("remove")){
                            regionRemoveCommand(player, cmd, Label, args);
                        }

                    }else if(args[0].equalsIgnoreCase("subregion")){
                        if(args[1].equalsIgnoreCase("create")){
                            subRegionCreateCommand(player, cmd, Label, args);
                        }else if(args[1].equalsIgnoreCase("remove")){
                            subRegionRemoveCommand(player, cmd, Label, args);
                        }

                    }
                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Kingdoms" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding kingdoms.");
    }

    private void subRegionRemoveCommand(Player player, Command cmd, String label, String[] args){

    }

    private void subRegionCreateCommand(Player player, Command cmd, String label, String[] args){
        Bukkit.getPluginManager().callEvent(new KingdomSubRegionCreateEvent(player, args[2], args[3]));
    }

    private void regionRemoveCommand(Player player, Command cmd, String label, String[] args){

    }

    private void regionCreateCommand(Player player, Command cmd, String label, String[] args){
        Bukkit.getPluginManager().callEvent(new KingdomRegionCreateEvent(player, args[2]));
    }

    private void commandsCommand(Player player, Command cmd, String Label, String[] args){

    }

    private void listCommand(Player player, Command cmd, String Label, String[] args){

    }

    private void statsCommand(Player player, Command cmd, String Label, String[] args){

    }

    private void infoCommand(Player player, Command cmd, String Label, String[] args){
        sendInfoMessage(player);
    }

}
