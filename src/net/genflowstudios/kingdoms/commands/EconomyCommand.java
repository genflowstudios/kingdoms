package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.display.DisplayHandler;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EconomyCommand{

    public EconomyCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Economy")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("stats")){
                        statsCommand(player, cmd, Label, args);
                    }else{

                    }
                }else if(args.length == 2){
                    if(args[0].equalsIgnoreCase("stats")){
                        statsCommand(player, cmd, Label, args);
                    }else{

                    }
                }else if(args.length == 3){
                    if(args[0].equalsIgnoreCase("addBonds")){
                        bondAddCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("addTokens")){
                        tokenAddCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("removeBonds")){
                        bondRemoveCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("removeTokens")){
                        tokenRemoveCommand(player, cmd, Label, args);
                    }else{

                    }
                }else{

                }
            }
        }else{
            if(Label.equalsIgnoreCase("Economy")){
                if(args.length == 0){

                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("stats")){
                    }else{

                    }
                }else if(args.length == 3){
                    if(args[0].equalsIgnoreCase("stats")){

                    }else if(args[0].equalsIgnoreCase("addBonds")){
                        bondAddCommand(Bukkit.getServer().getPlayer(args[1]), cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("addTokens")){
                        tokenAddCommand(Bukkit.getPlayer(args[1]), cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("removeBonds")){
                        bondRemoveCommand(Bukkit.getPlayer(args[1]), cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("removeTokens")){
                        tokenRemoveCommand(Bukkit.getPlayer(args[1]), cmd, Label, args);
                    }else{

                    }
                }else{

                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Cities" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding cities.");
    }

    private void tokenRemoveCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            kingdomPlayer.getStats().setTokens(kingdomPlayer.getStats().getTokens() - Integer.parseInt(args[2]));
            if(Integer.parseInt(args[2]) > 1){
                player.sendMessage(args[2] + " tokens have been removed from your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }else{
                player.sendMessage(args[2] + " token has been removed from your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void tokenAddCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            kingdomPlayer.getStats().setTokens(kingdomPlayer.getStats().getTokens() + Integer.parseInt(args[2]));
            if(Integer.parseInt(args[2]) > 1){
                player.sendMessage(args[2] + " tokens have been added to your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }else{
                player.sendMessage(args[2] + " token has been added to your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void bondRemoveCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            kingdomPlayer.getStats().setBonds(kingdomPlayer.getStats().getBonds() - Double.parseDouble(args[2]));
            if(Double.parseDouble(args[2]) > 1){
                player.sendMessage(args[2] + " bonds have been removed from your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }else{
                player.sendMessage(args[2] + " bond has been removed from your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void bondAddCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            kingdomPlayer.getStats().setBonds(kingdomPlayer.getStats().getBonds() + Double.parseDouble(args[2]));
            if(Double.parseDouble(args[2]) > 1){
                player.sendMessage(args[2] + " bonds have been added to your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }else{
                player.sendMessage(args[2] + " bond has been added to your account.");
                DisplayHandler displayHandler = new DisplayHandler();
                displayHandler.addPlayerStatsScoreboard(player);
            }

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void statsOtherCommand(Player player, Command cmd, String label, String[] args){

    }

    private void statsCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            player.sendMessage("Bonds: " + kingdomPlayer.getStats().getBonds() + "\nTokens: " + kingdomPlayer.getStats().getTokens());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

}
