package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.items.KingdomItem;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ItemCommand{

    public ItemCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("KingdomItem")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){

                }else{
                    if(args[0].equalsIgnoreCase("give")){
                        giveCommand(player, cmd, Label, args);
                    }else{

                    }
                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Cities" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding cities.");
    }

    private void giveCommand(Player player, Command cmd, String label, String[] args){
        KingdomItem.giveItem(player, args[1]);
    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

}
