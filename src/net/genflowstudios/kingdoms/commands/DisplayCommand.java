package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.display.DisplayHandler;
import net.genflowstudios.kingdoms.display.ScoreboardTemplate;
import net.genflowstudios.kingdoms.display.ScoreboardTypes;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DisplayCommand{

    public DisplayCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Display")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){

                }else if(args.length == 2){
                    if(args[0].equalsIgnoreCase("add")){
                        addCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("template")){
                        templateCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("remove")){
                        removeCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("list")){
                        listCommand(player, cmd, Label, args);
                    }
                }else{

                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Cities" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding cities.");
    }

    private void listCommand(Player player, Command cmd, String label, String[] args){
        DisplayHandler displayHandler = new DisplayHandler();
        if(args[1].equalsIgnoreCase("types")){
            player.sendMessage("Displaying Types:");
            for(ScoreboardTypes types : displayHandler.getTypes()){
                player.sendMessage(types.getName());
            }
        }else if(args[1].equalsIgnoreCase("templates")){
            player.sendMessage("Displaying Templates:");
            for(ScoreboardTemplate types : displayHandler.getTemplates()){
                player.sendMessage(types.getTemplateName());
            }
        }
    }

    private void removeCommand(Player player, Command cmd, String label, String[] args){
        DisplayHandler displayHandler = new DisplayHandler();
        KingdomPlayer kingdomPlayer;
        try{
            kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            for(ScoreboardTypes types : displayHandler.getTypes()){
                if(args[1].equalsIgnoreCase(types.getName())){
                    try{
                        kingdomPlayer.removeDisplay(types);
                        displayHandler.addPlayerStatsScoreboard(player);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void templateCommand(Player player, Command cmd, String label, String[] args){
        DisplayHandler displayHandler = new DisplayHandler();
        KingdomPlayer kingdomPlayer;
        try{
            kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            for(ScoreboardTemplate types : displayHandler.getTemplates()){
                if(args[1].equalsIgnoreCase(types.getTemplateName())){
                    for(ScoreboardTypes type : types.getTemplateTypes()){
                        kingdomPlayer.addDisplay(type);
                    }
                    displayHandler.addPlayerStatsScoreboard(player);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void addCommand(Player player, Command cmd, String label, String[] args){
        DisplayHandler displayHandler = new DisplayHandler();
        KingdomPlayer kingdomPlayer;
        try{
            kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            for(ScoreboardTypes types : displayHandler.getTypes()){
                if(args[1].equalsIgnoreCase(types.getName())){
                    kingdomPlayer.addDisplay(types);
                    displayHandler.addPlayerStatsScoreboard(player);
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

}
