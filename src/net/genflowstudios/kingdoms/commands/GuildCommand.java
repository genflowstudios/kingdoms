package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.guilds.GuildManager;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GuildCommand{

    public GuildCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Guilds")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("stats")){
                        statsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("list")){
                        listCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("commands")){
                        commandsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("disband")){
                        disbandGuildCommand(player, cmd, Label, args);
                    }
                }else if(args.length == 2){
                    if(args[0].equalsIgnoreCase("create")){
                        createGuildCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("invite")){
                        if(args[1].equalsIgnoreCase("accept")){
                            inviteAcceptGuildCommand(player, cmd, Label, args);
                        }else if(args[1].equalsIgnoreCase("decline")){
                            inviteDeclineGuildCommand(player, cmd, Label, args);
                        }else{
                            inviteGuildCommand(player, cmd, Label, args);
                        }
                    }
                }else{

                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Guilds" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding guilds.");
    }

    private void inviteDeclineGuildCommand(Player player, Command cmd, String label, String[] args){

    }

    private void inviteAcceptGuildCommand(Player player, Command cmd, String label, String[] args){

    }

    private void inviteGuildCommand(Player player, Command cmd, String label, String[] args){

    }

    private void disbandGuildCommand(Player player, Command cmd, String label, String[] args){

        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            if(kingdomPlayer.isInGuild()){
                Guild guild = kingdomPlayer.getGuild();
                if(guild.getLeader().equals(player.getUniqueId())){
                    GuildManager.removeGuild(guild);
                    kingdomPlayer.setGuild(null);
                    kingdomPlayer.setInGuild(false);
                    player.sendMessage("===[" + ChatColor.AQUA + "Guilds" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have successfully disbanded your guild.");
                }else{
                    player.sendMessage("===[" + ChatColor.AQUA + "Guilds" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You can't disband this guild! You don't own it, in order to leave type /guilds leave.");
                }
            }else{
                player.sendMessage("===[" + ChatColor.AQUA + "Guilds" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You can't disband this guild! You don't own it, in order to leave type /guilds leave.");
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void createGuildCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            if(kingdomPlayer.isInGuild()){
                player.sendMessage("===[" + ChatColor.AQUA + "Guilds" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "You must leave the guild your in, in order to create your own!");
            }else{
                Guild guild = new Guild(args[1], player.getUniqueId());
                kingdomPlayer.setGuild(guild);
                kingdomPlayer.setInGuild(true);
                player.sendMessage("===[" + ChatColor.AQUA + "Guilds" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "You have successfully created a guild.");
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void commandsCommand(Player player, Command cmd, String Label, String[] args){

    }

    private void listCommand(Player player, Command cmd, String Label, String[] args){

    }

    private void statsCommand(Player player, Command cmd, String Label, String[] args){

    }

    private void infoCommand(Player player, Command cmd, String Label, String[] args){
        sendInfoMessage(player);
    }

}
