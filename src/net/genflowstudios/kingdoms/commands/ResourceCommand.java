package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ResourceCommand{

    public ResourceCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(Label.equalsIgnoreCase("resource")){
            if(args.length == 0){
                infoCommand(sender, cmd, Label, args);
            }else if(args.length == 1){
                if(args[0].equalsIgnoreCase("toggle")){
                    toggleCommand(sender, cmd, Label, args);
                }else{

                }
            }else{

            }
        }


    }


    private void toggleCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            try{
                KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
                if(kingdomPlayer.getInventory().getPlayerTools().isHarvestTool()){
                    kingdomPlayer.getInventory().getPlayerTools().setHarvestTool(false);
                    player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "Disabled Harvesting");
                }else{
                    kingdomPlayer.getInventory().getPlayerTools().setHarvestTool(true);
                    player.sendMessage("===[" + ChatColor.AQUA + "Player" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "Enabled Harvesting");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }


    private void infoCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
        }
    }

}
