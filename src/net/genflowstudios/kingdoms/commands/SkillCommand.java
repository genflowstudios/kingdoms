package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.skills.KPlayerSkills;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SkillCommand{

    public SkillCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Skill")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("Woodcutting")){
                        woodcuttingCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("Mining")){
                        miningCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("Attack")){
                        attackCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("Defence")){
                        defenceCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("Strength")){
                        strengthCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("HitPoints")){
                        hitpointsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("reset")){
                        resetCommand(player, cmd, Label, args);
                    }
                }else if(args.length == 3){
                    if(args[0].equalsIgnoreCase("levelset")){
                        levelSetCommand(player, cmd, Label, args);
                    }
                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Skills" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding cities.");
    }

    private void resetCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            for(SkillType type : SkillType.values()){
                skills.setSkillLevel(type, 1);
                skills.setSkillExp(type, 0.0);
            }
            player.sendMessage("You have reset your skills.");
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void hitpointsCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            player.sendMessage("Hit Points level: " + skills.getSkillLevel(SkillType.HITPOINTS));
            player.sendMessage("Hit Points exp: " + skills.getSkillExp(SkillType.HITPOINTS));
            player.sendMessage("Hit Points Exp to next level: " + skills.amountTillNextLevel(SkillType.HITPOINTS));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void strengthCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            player.sendMessage("Strength level: " + skills.getSkillLevel(SkillType.STRENGTH));
            player.sendMessage("Strength exp: " + skills.getSkillExp(SkillType.STRENGTH));
            player.sendMessage("Strength Exp to next level: " + skills.amountTillNextLevel(SkillType.STRENGTH));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void defenceCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            player.sendMessage("Defence level: " + skills.getSkillLevel(SkillType.DEFENCE));
            player.sendMessage("Defence exp: " + skills.getSkillExp(SkillType.DEFENCE));
            player.sendMessage("Defence Exp to next level: " + skills.amountTillNextLevel(SkillType.DEFENCE));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void attackCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            player.sendMessage("Attack level: " + skills.getSkillLevel(SkillType.ATTACK));
            player.sendMessage("Attack exp: " + skills.getSkillExp(SkillType.ATTACK));
            player.sendMessage("Attack Exp to next level: " + skills.amountTillNextLevel(SkillType.ATTACK));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void levelSetCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            if(args[1].equalsIgnoreCase("Woodcutting")){
                skills.setSkillLevel(SkillType.WOODCUTTING, Integer.parseInt(args[2]));
            }else if(args[1].equalsIgnoreCase("Mining")){
                skills.setSkillLevel(SkillType.MINING, Integer.parseInt(args[2]));
            }else if(args[1].equalsIgnoreCase("Defence")){
                skills.setSkillLevel(SkillType.DEFENCE, Integer.parseInt(args[2]));
            }else if(args[1].equalsIgnoreCase("Attack")){
                skills.setSkillLevel(SkillType.ATTACK, Integer.parseInt(args[2]));
            }else if(args[1].equalsIgnoreCase("Strength")){
                skills.setSkillLevel(SkillType.STRENGTH, Integer.parseInt(args[2]));
            }else if(args[1].equalsIgnoreCase("HitPoints")){
                skills.setSkillLevel(SkillType.HITPOINTS, Integer.parseInt(args[2]));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void miningCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            player.sendMessage("Mining level: " + skills.getSkillLevel(SkillType.MINING));
            player.sendMessage("Mining exp: " + skills.getSkillExp(SkillType.MINING));
            player.sendMessage("Mining Exp to next level: " + skills.amountTillNextLevel(SkillType.MINING));
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    private void woodcuttingCommand(Player player, Command cmd, String label, String[] args){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            player.sendMessage("Woodcutting level: " + skills.getSkillLevel(SkillType.WOODCUTTING));
            player.sendMessage("Woodcutting exp: " + skills.getSkillExp(SkillType.WOODCUTTING));
            player.sendMessage("Woodcutting exp to next level: " + skills.amountTillNextLevel(SkillType.WOODCUTTING));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

}
