package net.genflowstudios.kingdoms.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PartyCommands{

    public PartyCommands(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Party")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){//Commands that have one parameter.
                    if(args[0].equalsIgnoreCase("stats")){
                        statsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("list")){
                        listCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("commands")){
                        commandsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("disband")){
                        disbandCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("create")){
                        createCommand(player, cmd, Label, args);
                    }
                    {

                    }
                }else if(args.length == 2){//Commands that have two parameters.
                    if(args[0].equalsIgnoreCase("invite")){
                        inviteCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("kick")){
                        kickCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("promote")){
                        promoteCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("demote")){
                        demoteCommand(player, cmd, Label, args);
                    }
                }else{

                }
            }
        }

    }

    private void demoteCommand(Player player, Command cmd, String label, String[] args){

    }

    private void promoteCommand(Player player, Command cmd, String label, String[] args){

    }

    private void kickCommand(Player player, Command cmd, String label, String[] args){

    }

    private void inviteCommand(Player player, Command cmd, String label, String[] args){

    }

    private void createCommand(Player player, Command cmd, String label, String[] args){

    }

    private void disbandCommand(Player player, Command cmd, String label, String[] args){

    }

    private void commandsCommand(Player player, Command cmd, String label, String[] args){

    }

    private void listCommand(Player player, Command cmd, String label, String[] args){

    }

    private void statsCommand(Player player, Command cmd, String label, String[] args){

    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){

    }

}
