package net.genflowstudios.kingdoms.commands;

import net.genflowstudios.kingdoms.Engine;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandManager implements CommandExecutor{

    Engine kingdoms;
    String error = "===[" + ChatColor.AQUA + "Kingdoms" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "[ERROR] - That command does not exist!";

    //Constructor
    public CommandManager(Engine kingdoms){
        this.kingdoms = kingdoms;
    }

    //On player command.
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(Label.equalsIgnoreCase("Kingdoms")){
            new KingdomsCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Party")){
            new PartyCommands(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Area")){

        }else if(Label.equalsIgnoreCase("Guilds")){
            new GuildCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Build")){
            new BuildCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Player")){
            new PlayerCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Resource")){
            new ResourceCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Display")){
            new DisplayCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Skill")){
            new SkillCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("Economy")){
            new EconomyCommand(sender, cmd, Label, args);
        }else if(Label.equalsIgnoreCase("KingdomItem")){
            new ItemCommand(sender, cmd, Label, args);
        }else{
            sender.sendMessage(error);
        }
        return false;
    }


}
