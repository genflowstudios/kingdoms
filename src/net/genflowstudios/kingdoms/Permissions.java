package net.genflowstudios.kingdoms;

import org.bukkit.permissions.Permission;

public class Permissions{

    /**
     * Kingdom admin permission.
     */
    public static Permission kingdomAdmin = new Permission("kingdoms.*");

    /**
     * All commands permission.
     */
    public static Permission allCommands = new Permission("kingdoms.command.*");

    /**
     * All kingdom commands permission.
     */
    public static Permission allKingdomCommands = new Permission("kingdoms.command.kingdoms.*");

    /**
     * Kingdom info command permission.
     */
    public static Permission kingdomInfoCommand = new Permission("kingdoms.command.kingdoms.info");

    /**
     * Kingdom create command permission.
     */
    public static Permission kingdomCreateCommand = new Permission("kingdom.command.kingdoms.create");

    /**
     * Kingdom invite command permission.
     */
    public static Permission kingdomInviteCommand = new Permission("kingdom.command.kingdoms.invite");

    /**
     * Kingdom join command permission.
     */
    public static Permission kingdomJoinCommand = new Permission("kingdom.command.kingdoms.join");

    /**
     * All city commands permission.
     */
    public static Permission allCityCommands = new Permission("kingdoms.command.city.*");

    /**
     * City info command permission.
     */
    public static Permission cityInfoCommand = new Permission("kingdoms.command.city.info");

    /**
     * City stockpile command permission.
     */
    public static Permission cityStockpileCommand = new Permission("kingdoms.command.city.stockpile");

    /**
     * City stats command permission.
     */
    public static Permission cityStatsCommand = new Permission("kingdoms.command.city.stats");

    /**
     * City create command permission.
     */
    public static Permission cityCreateCommand = new Permission("kingdoms.command.city.create");

    /**
     * All player commands permission.
     */
    public static Permission allPlayerCommands = new Permission("kingdoms.command.player.*");

}
