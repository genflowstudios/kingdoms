package net.genflowstudios.kingdoms.events;

import org.bukkit.event.Event;

public abstract class KingdomEvent extends Event{

    public abstract void callEvent();

}
