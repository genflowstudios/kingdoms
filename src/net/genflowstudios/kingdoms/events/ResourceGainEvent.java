package net.genflowstudios.kingdoms.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class ResourceGainEvent extends KingdomEvent implements Cancellable{

    private static final HandlerList handlers = new HandlerList();
    boolean cancel = false;
    UUID uuid;
    Location location;
    Material item;

    public ResourceGainEvent(UUID uuid, Material item){
        this.uuid = uuid;
        this.location = Bukkit.getPlayer(uuid).getLocation();
        this.item = item;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

    /**
     * @return the cancel
     */
    public boolean isCancel(){
        return cancel;
    }

    /**
     * @param cancel the cancel to set
     */
    public void setCancel(boolean cancel){
        this.cancel = cancel;
    }

    /**
     * @return the Material
     */
    public Material getMaterial(){
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setMaterial(Material item){
        this.item = item;
    }

    /**
     * @return the player
     */
    public UUID getUUID(){
        return uuid;
    }

    /**
     * @return the location
     */
    public Location getLocation(){
        return location;
    }

    public HandlerList getHandlers(){
        return handlers;
    }



    @Override
    public boolean isCancelled(){
        return cancel;
    }

    @Override
    public void setCancelled(boolean value){
        this.cancel = value;
    }

    @Override
    public void callEvent(){

    }

}
