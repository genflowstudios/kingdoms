package net.genflowstudios.kingdoms.events;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.items.KingdomItem;
import net.genflowstudios.kingdoms.listener.BuildingEventListener;
import net.genflowstudios.kingdoms.listener.ServerQuitJoinListener;
import net.genflowstudios.kingdoms.player.actions.*;
import net.genflowstudios.kingdoms.player.listener.PlayerMoveListener;
import net.genflowstudios.kingdoms.player.listener.PlayerResourceGainListener;
import net.genflowstudios.kingdoms.player.listener.PlayerResourceHarvestListener;
import net.genflowstudios.kingdoms.player.listener.PlayerSkillsListener;
import org.bukkit.Bukkit;

public class EventManager{

    Engine kingdoms;

    public EventManager(Engine kingdoms){
        this.kingdoms = kingdoms;
    }

    /**
     * Registers all events.
     */
    public void registerEvents(){
        try{
            Bukkit.getPluginManager().registerEvents(new ServerQuitJoinListener(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new PlayerResourceHarvestListener(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new PlayerMoveListener(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new PlayerResourceGainListener(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new KingdomItem(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new BuildingEventListener(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new CheckBagAction(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new ResourceHarvestToggleAction(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new PartyInviteAction(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new BlockAction(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new AttackAction(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new DamageAction(), kingdoms);
            Bukkit.getPluginManager().registerEvents(new PlayerSkillsListener(), kingdoms);
        }catch(Exception e){
            System.out.println("Error registering events.");
            e.printStackTrace();
        }
    }


}
