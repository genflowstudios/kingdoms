package net.genflowstudios.kingdoms.parties;

public enum PartyType{


    DUNGEON{
        @Override
        public int getMaxPartySize(){
            return 10;
        }
    },
    COMMON{
        @Override
        public int getMaxPartySize(){
            return 6;
        }
    },
    FRIEND{
        @Override
        public int getMaxPartySize(){
            return 9;
        }
    },
    RAID{
        @Override
        public int getMaxPartySize(){
            return 25;
        }
    },
    KINGDOM{
        @Override
        public int getMaxPartySize(){
            return -1;
        }
    };


    public abstract int getMaxPartySize();

}
