package net.genflowstudios.kingdoms.adapters;

import net.genflowstudios.kingdoms.Engine;
import org.bukkit.Bukkit;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class WriterAdapter{


    public WriterAdapter(String message){

    }

    public WriterAdapter(final String playerSave, int time){
        write(playerSave, time);
    }


    public void writeToDatabase(String message){
        try{
            Socket client = new Socket("72.231.199.200", 15004);
            PrintWriter writer = new PrintWriter(client.getOutputStream());
            writer.println(message);
            writer.close();
            client.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }


    public synchronized void write(final String playerSave, int time){
        Bukkit.getScheduler().scheduleSyncDelayedTask(Engine.getInstance(), new Runnable(){

            @Override
            public void run(){
                try{
                    Socket client = new Socket("72.231.199.200", 15004);
                    PrintWriter writer = new PrintWriter(client.getOutputStream());
                    writer.println("LISTEN");
                    writer.println(playerSave);
                    writer.close();
                    client.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }, time);
        Bukkit.getScheduler().scheduleSyncDelayedTask(Engine.getInstance(), new Runnable(){

            @Override
            public void run(){
                try{
                    Socket client = new Socket("72.231.199.200", 15004);
                    PrintWriter writer = new PrintWriter(client.getOutputStream());
                    writer.println("STOP");
                    writer.close();
                    client.close();
                }catch(IOException e){
                    e.printStackTrace();
                }
            }
        }, time * 2);
    }
}
