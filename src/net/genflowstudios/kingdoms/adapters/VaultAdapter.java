package net.genflowstudios.kingdoms.adapters;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultAdapter{
    private static final String VAULT = "Vault";

    private static Economy ecoVault = null;
    private static boolean vaultLoaded = false;

    public static Economy getEconomy(){
        if(!vaultLoaded){
            vaultLoaded = true;
            Server theServer = Bukkit.getServer();
            if(theServer.getPluginManager().getPlugin(VAULT) != null){
                RegisteredServiceProvider<Economy> rsp = theServer.getServicesManager().getRegistration(Economy.class);
                if(rsp != null){
                    ecoVault = rsp.getProvider();
                }
            }
        }

        return ecoVault;
    }
}
