package net.genflowstudios.kingdoms.adapters;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import net.genflowstudios.kingdoms.Engine;
import org.bukkit.plugin.Plugin;

public class WorldEditAdapter{


    public WorldEditPlugin getWorldEdit(){
        Plugin plugin = Engine.getInstance().getServer().getPluginManager().getPlugin("WorldEdit");

        // WorldGuard may not be loaded
        if(plugin == null || !(plugin instanceof WorldEditPlugin)){
            return null; // Maybe you want throw an exception instead
        }

        return (WorldEditPlugin) plugin;
    }

}
