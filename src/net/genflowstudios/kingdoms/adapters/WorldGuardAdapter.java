package net.genflowstudios.kingdoms.adapters;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import net.genflowstudios.kingdoms.Engine;
import org.bukkit.plugin.Plugin;

public class WorldGuardAdapter{


    public WorldGuardPlugin getWorldGuard(){
        Plugin plugin = Engine.getInstance().getServer().getPluginManager().getPlugin("WorldGuard");

        // WorldGuard may not be loaded
        if(plugin == null || !(plugin instanceof WorldGuardPlugin)){
            return null; // Maybe you want throw an exception instead
        }

        return (WorldGuardPlugin) plugin;
    }

}
