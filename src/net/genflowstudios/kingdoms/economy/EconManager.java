package net.genflowstudios.kingdoms.economy;

import org.bukkit.OfflinePlayer;

public class EconManager{

    //Constructor.
    public EconManager(OfflinePlayer player) throws Exception{
        if(load(player)){

        }else{
            throw new Exception("New Player Exception!");
        }
    }

    /**
     * Loads new players.
     *
     * @param player
     * @return Whether if the player exists and if the accounts are created.
     */
    private boolean load(OfflinePlayer player){
        EconLoader loader = new EconLoader();
        return loader.loadPlayer(player);
    }


}
