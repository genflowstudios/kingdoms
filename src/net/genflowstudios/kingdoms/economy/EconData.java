package net.genflowstudios.kingdoms.economy;

import net.genflowstudios.kingdoms.adapters.VaultAdapter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.HashMap;
import java.util.UUID;

public class EconData{

    public static HashMap<UUID, Double> money = new HashMap<UUID, Double>();
    public static HashMap<UUID, Integer> tokens = new HashMap<UUID, Integer>();

    /**
     * Adds a vault account for player money.
     *
     * @param player
     * @param amount
     * @return Whether the account was created.
     */
    public void addAccount(OfflinePlayer player, double amount){
        if(VaultAdapter.getEconomy().hasAccount(player)){
            if(money.containsKey(player.getUniqueId())){
                if(tokens.containsKey(player.getUniqueId())){

                }else{
                    tokens.put(player.getUniqueId(), 0);
                }
            }else{
                if(tokens.containsKey(player.getUniqueId())){

                }else{
                    tokens.put(player.getUniqueId(), 0);
                }
                money.put(player.getUniqueId(), VaultAdapter.getEconomy().getBalance(player));
            }
        }else{
            if(money.containsKey(player.getUniqueId())){
                if(tokens.containsKey(player.getUniqueId())){

                }else{
                    tokens.put(player.getUniqueId(), 0);
                }
            }else{
                if(tokens.containsKey(player.getUniqueId())){

                }else{
                    tokens.put(player.getUniqueId(), 0);
                }
                money.put(player.getUniqueId(), VaultAdapter.getEconomy().getBalance(player));
            }
        }
    }

    /**
     * Removes vault accounts for player money.
     *
     * @param player
     * @return Whether the account was removed.
     */
    public boolean removeAccount(OfflinePlayer player){
        if(VaultAdapter.getEconomy().hasAccount(player)){
            if(money.containsKey(player)){
                VaultAdapter.getEconomy().withdrawPlayer(player, VaultAdapter.getEconomy().getBalance(player));
                money.remove(player);
                return true;
            }else{
                VaultAdapter.getEconomy().withdrawPlayer(player, VaultAdapter.getEconomy().getBalance(player));
                return true;
            }
        }else{
            if(money.containsKey(player)){
                money.remove(player);
                return true;
            }else{
                return false;
            }
        }
    }

    /**
     * @param
     * @return Vault account money.
     */
    private double getAccountMoney(UUID uuid){
        if(VaultAdapter.getEconomy().hasAccount(Bukkit.getOfflinePlayer(uuid))){
            return VaultAdapter.getEconomy().getBalance(Bukkit.getOfflinePlayer(uuid));
        }else{
            VaultAdapter.getEconomy().createPlayerAccount(Bukkit.getOfflinePlayer(uuid));
            return VaultAdapter.getEconomy().getBalance(Bukkit.getOfflinePlayer(uuid));
        }
    }

    /**
     * @param uuid
     * @return Saved in instance money.
     */
    private double getSavedMoney(UUID uuid){
        if(money.containsKey(uuid)){
            return money.get(uuid);
        }else{
            addAccount(Bukkit.getOfflinePlayer(uuid), 0.0);
            return 0.0;
        }
    }

    /**
     * Sets instance money.
     *
     * @param player
     * @param amount
     */
    private void setSavedMoney(UUID player, double amount){
        if(money.containsKey(player)){
            money.put(player, amount);
        }else{
            money.put(player, amount);
        }
    }


    /**
     * Sets vault account money.
     *
     * @param player
     * @param amount
     */
    public void setAccountMoney(OfflinePlayer player, double amount){
        if(VaultAdapter.getEconomy().hasAccount(player)){
            VaultAdapter.getEconomy().withdrawPlayer(player, VaultAdapter.getEconomy().getBalance(player));
            VaultAdapter.getEconomy().depositPlayer(player, amount);
        }else{
            VaultAdapter.getEconomy().createPlayerAccount(player);
            VaultAdapter.getEconomy().depositPlayer(player, amount);
        }
    }

    /**
     * @param player
     * @return Money of a player.
     */
    public double getMoney(UUID player){
        if(getAccountMoney(player) == getSavedMoney(player)){
            return getSavedMoney(player);
        }else{
            setSavedMoney(player, getAccountMoney(player));
            return getMoney(player);
        }
    }


    public void setTokens(UUID player, int value){
        if(tokens.containsKey(player)){
            tokens.put(player, value);
        }else{
            tokens.put(player, value);
        }
    }


    /**
     * @param uuid
     * @return Money of a player.
     */
    public int getTokens(UUID uuid){
        if(tokens.containsKey(uuid)){
            return tokens.get(uuid);
        }else{
            return 0;
        }
    }


}
