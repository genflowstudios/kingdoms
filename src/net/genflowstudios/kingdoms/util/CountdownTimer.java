package net.genflowstudios.kingdoms.util;

import net.genflowstudios.kingdoms.Engine;
import org.bukkit.Bukkit;

import java.util.ArrayList;

public class CountdownTimer{

    public static ArrayList<CountdownTimer> timers = new ArrayList<CountdownTimer>();
    final int originalTime;
    int id;
    int time;
    boolean stopped;
    CountdownTimer timer;


    public CountdownTimer(int time){
        this.originalTime = time;
        this.time = time;
        this.stopped = false;
        this.timer = this;
        timers.add(timer);
    }

    //Creates a countdown timer for events.
    public void startTimer(){
        if(stopped){//If stopped, do not continue count and fire an event.
            timers.remove(timer);
        }else{
            if(time > 0){//Checks if time is greater than zero.
                this.id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Engine.getInstance(), new Runnable(){
                    @Override
                    public void run(){
                        time -= 1;
                    }

                }, 20L, 0L);
            }else{
                this.stopped = true;
                startTimer();
            }
        }
    }


    //Getters

    public boolean isDone(){
        return !timers.contains(timer);
    }

    public int getTimeLeft(){
        return time;
    }

    public void setTimeLeft(int timeValue){
        this.time = timeValue;
    }

    public int getOriginalTime(){
        return originalTime;
    }

}
