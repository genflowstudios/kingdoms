package net.genflowstudios.kingdoms;

import net.genflowstudios.kingdoms.commands.CommandManager;
import net.genflowstudios.kingdoms.data.*;
import net.genflowstudios.kingdoms.events.EventManager;
import net.genflowstudios.kingdoms.guilds.Guild;
import net.genflowstudios.kingdoms.items.mechanics.ItemChecker;
import net.genflowstudios.kingdoms.kingdoms.areas.managers.KingdomRegionManager;
import net.genflowstudios.kingdoms.kingdoms.events.KingdomEventManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Engine extends JavaPlugin{

    private static Engine instance;
    private int id;

    // Allows access to the main class
    public static Engine getInstance(){
        return instance;
    }

    @Override
    public void onEnable(){
        instance = this;
        CommandManager commandManager = new CommandManager(this);
        EventManager eventManager = new EventManager(this);
        eventManager.registerEvents();
        getCommand("Kingdoms").setExecutor(commandManager);
        getCommand("Party").setExecutor(commandManager);
        getCommand("Area").setExecutor(commandManager);
        getCommand("Build").setExecutor(commandManager);
        getCommand("Display").setExecutor(commandManager);
        getCommand("Guilds").setExecutor(commandManager);
        getCommand("KingdomItem").setExecutor(commandManager);
        getCommand("Economy").setExecutor(commandManager);
        getCommand("Resource").setExecutor(commandManager);
        getCommand("Skill").setExecutor(commandManager);
        checkFiles();
        KingdomEventManager kingdomEventManager = new KingdomEventManager();
        kingdomEventManager.registerEvents();
        new Guild("NPC");
        new ItemChecker();
        getLogger().info("has been enabled!");
    }


    @Override
    public void onDisable(){
        PlayerSavingHandler playerSavingHandler = new PlayerSavingHandler();
        playerSavingHandler.savePlayers();
        KingdomRegionManager.saveAllRegions();
        getLogger().info("has been disabled!");
    }

    // Checks default files.
    private void checkFiles(){
        File kingdomFiles = new File(getInstance().getDataFolder(), "Settings");
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        SettingsHandler settingsHandler = new SettingsHandler();
        //If default config files exist.
        if(kingdomFiles.exists()){
            if(settingsHandler.isFlatfileMode()){

            }else{
                MySQLAdapter.createTables();
            }
            //If Free-Roam mode is enabled
            if(cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getString("Plugin-Mode").equalsIgnoreCase("Free-Roam")){
                cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Quests-Enabled", false);
                cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Vault-Enabled", true);
                cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Global-Economy-Enabled", true);
                ConfigManager.saveDefaultConfig(cfg);
                getLogger().info("Free-Roam mode enabled!");
            }else{
                cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Quests-Enabled", true);
                cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Vault-Enabled", true);
                cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Global-Economy-Enabled", true);
                ConfigManager.saveDefaultConfig(cfg);
                getLogger().info("Main-Quest mode enabled!");
            }

            //If Flatfile mode is enabled.
            if(cfg.getConfigurationSection("Settings").getBoolean("Flatfile")){
                getLogger().info("Flatfile mode enabled!");
            }else{
                getLogger().info("MySQL mode enabled!");
            }
        }else{
            createDefaultSettings(cfg);
        }
    }


    //Creates default settings.
    private void createDefaultSettings(FileConfiguration cfg){
        ConfigChangeHandler configChangeHandler = new ConfigChangeHandler();
        configChangeHandler.generateSettingsFile(cfg);
        getLogger().info("Created default config files!");
    }


}
