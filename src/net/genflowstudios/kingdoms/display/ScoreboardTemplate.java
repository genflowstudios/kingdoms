package net.genflowstudios.kingdoms.display;

import java.util.ArrayList;


public enum ScoreboardTemplate{


    PLAYER_ATTRIBUTES{
        @Override
        public ArrayList<ScoreboardTypes> getTemplateTypes(){
            ArrayList<ScoreboardTypes> types = new ArrayList<ScoreboardTypes>();
            types.add(ScoreboardTypes.ARMOR);
            types.add(ScoreboardTypes.INTELLECT);
            types.add(ScoreboardTypes.STRENGTH);
            types.add(ScoreboardTypes.RAGE);
            types.add(ScoreboardTypes.MANA);
            types.add(ScoreboardTypes.HEALTH);
            return types;
        }

        @Override
        public String getTemplateName(){
            return "PlayerAttributes";
        }
    },
    PLAYER_STATS{
        @Override
        public ArrayList<ScoreboardTypes> getTemplateTypes(){
            ArrayList<ScoreboardTypes> types = new ArrayList<ScoreboardTypes>();
            types.add(ScoreboardTypes.DEATHS);
            types.add(ScoreboardTypes.KILLS);
            types.add(ScoreboardTypes.TOKENS);
            types.add(ScoreboardTypes.BONDS);
            return types;
        }

        @Override
        public String getTemplateName(){
            return "PlayerStats";
        }
    },
    SERVER_STATS{
        @Override
        public ArrayList<ScoreboardTypes> getTemplateTypes(){
            ArrayList<ScoreboardTypes> types = new ArrayList<ScoreboardTypes>();
            types.add(ScoreboardTypes.PLAYERS);
            return types;
        }

        @Override
        public String getTemplateName(){
            return "ServerStats";
        }
    };


    public abstract ArrayList<ScoreboardTypes> getTemplateTypes();

    public abstract String getTemplateName();

}
