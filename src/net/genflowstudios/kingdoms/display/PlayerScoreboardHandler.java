package net.genflowstudios.kingdoms.display;

import net.genflowstudios.kingdoms.Engine;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PlayerScoreboardHandler{

    Engine kingdoms;
    HashMap<Player, ScoreboardHandler> playerScoreboards = new HashMap<Player, ScoreboardHandler>();

    //Constructor
    public PlayerScoreboardHandler(Engine kingdoms){
        this.kingdoms = kingdoms;
    }

    //Returns player scoreboard.
    public ScoreboardHandler getPlayerScoreboard(Player player){
        if(isPlayerInList(player)){
            return playerScoreboards.get(player);
        }else{
            return null;
        }
    }

    //Checks whether player is in list.
    public boolean isPlayerInList(Player player){
        for(Player players : playerScoreboards.keySet()){
            if(players.equals(player)){
                return true;
            }
        }
        return false;
    }

    //Sets player scoreboard.
    public void setPlayerScoreboard(Player player, ScoreboardHandler scoreboard){
        if(isPlayerInList(player)){
            playerScoreboards.remove(player);
            playerScoreboards.put(player, scoreboard);
        }else{
            playerScoreboards.put(player, scoreboard);
        }
    }


}
