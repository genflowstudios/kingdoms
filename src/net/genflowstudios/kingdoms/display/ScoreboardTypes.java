package net.genflowstudios.kingdoms.display;

import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;

public enum ScoreboardTypes{


    //Economy
    BONDS{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�f" + player.getStats().getBonds());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�e�lBonds:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Bonds";
        }
    },
    TOKENS{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�d" + player.getStats().getTokens());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�b�lTokens:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Tokens";
        }
    },
    WEBSITE{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�fwww.genflow.net");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�e�lWebsite:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Website";
        }
    },
    PLAYERS{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�2" + Bukkit.getOnlinePlayers().size());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�a�lPlayers Online:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Players";
        }
    },

    //Attributes
    HEALTH{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�c" + player.getStats().getAttributes().getHealth());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�4�lHealth:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Health";
        }
    },
    MANA{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�d" + player.getStats().getAttributes().getMana());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�5�lMana:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Mana";
        }
    },
    RAGE{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), color + player.getStats().getAttributes().getRage());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�c�lRage:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Rage";
        }
    },
    STRENGTH{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�8" + player.getStats().getAttributes().getStrength());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�7�lStrength:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Strength";
        }
    },
    INTELLECT{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�5" + player.getStats().getAttributes().getIntellect());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�9�lIntellect:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Intellect";
        }
    },
    KILLS{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�d" + player.getStats().getKills());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�a�lKills:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Kills";
        }
    },
    DEATHS{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�c" + player.getStats().getDeaths());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�4�lDeaths:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Deaths";
        }
    },
    ARMOR{
        @Override
        public void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color){
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            scoreboard.setLine(player.getDisplayLine(), "�7" + player.getStats().getAttributes().getArmor());
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setLine(player.getDisplayLine(), "�8�lArmor:");
            player.setDisplayLine(player.getDisplayLine() + 1);
            scoreboard.setBlank(player.getDisplayLine());
            player.setDisplayLine(player.getDisplayLine() + 1);
        }

        @Override
        public String getName(){
            return "Armor";
        }
    };


    public abstract void addToScoreboard(KingdomPlayer player, ScoreboardHandler scoreboard, String color);

    public abstract String getName();
}
