package net.genflowstudios.kingdoms.display;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class DisplayHandler{

    Engine kingdoms;


    //Adds the scoreboard.
    public void addPlayerStatsScoreboard(Player player){
        KingdomPlayer kPlayer;
        try{
            kPlayer = KingdomPlayer.getPlayer(player.getUniqueId());
            if(kPlayer.getDisplays().isEmpty()){
            }else{
                ScoreboardHandler scoreboard = new ScoreboardHandler(player, "�6�lGenFlow Kingdoms:");
                kPlayer.setDisplayLine(0);
                for(ScoreboardTypes type : kPlayer.getDisplays()){
                    type.addToScoreboard(kPlayer, scoreboard, "�a");
                }
                scoreboard.build();
                scoreboard.send();
            }
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public ArrayList<ScoreboardTypes> getTypes(){
        ArrayList<ScoreboardTypes> types = new ArrayList<ScoreboardTypes>();
        types.add(ScoreboardTypes.ARMOR);
        types.add(ScoreboardTypes.BONDS);
        types.add(ScoreboardTypes.HEALTH);
        types.add(ScoreboardTypes.INTELLECT);
        types.add(ScoreboardTypes.MANA);
        types.add(ScoreboardTypes.RAGE);
        types.add(ScoreboardTypes.STRENGTH);
        types.add(ScoreboardTypes.TOKENS);
        types.add(ScoreboardTypes.KILLS);
        types.add(ScoreboardTypes.DEATHS);
        types.add(ScoreboardTypes.WEBSITE);
        types.add(ScoreboardTypes.PLAYERS);
        return types;
    }

    public ArrayList<ScoreboardTemplate> getTemplates(){
        ArrayList<ScoreboardTemplate> types = new ArrayList<ScoreboardTemplate>();
        types.add(ScoreboardTemplate.SERVER_STATS);
        types.add(ScoreboardTemplate.PLAYER_STATS);
        types.add(ScoreboardTemplate.PLAYER_ATTRIBUTES);
        return types;
    }


}
