package net.genflowstudios.kingdoms.data;

import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

public class MySQLAdapter{

    public static Connection connection;


    // Opens the MySQL Connection
    public synchronized static void openConnection(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        String host = cfg.getConfigurationSection("MySQL").getString("Host-IP");
        String port = cfg.getConfigurationSection("MySQL").getString("Port");
        String database = cfg.getConfigurationSection("MySQL").getString("Database");
        String username = cfg.getConfigurationSection("MySQL").getString("Username");
        String password = cfg.getConfigurationSection("MySQL").getString("Password");
        try{
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Creates default mysql tables.
     */
    public synchronized static void createTables(){
        try{
            if(connection == null){
                openConnection();
                PreparedStatement sql = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerstats(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Bonds double NOT NULL, Tokens int NOT NULL, Level int NOT NULL, Exp double NOT NULL, HarvestTool varchar(6) NOT NULL, Area varchar(255) NOT NULL, SocialRank varchar(255), MilitaryRank varchar(255), PRIMARY KEY(id))");
                sql.executeUpdate();
                PreparedStatement sql2 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerinventory(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Lumber int NOT NULL, Stone int NOT NULL, Coal int NOT NULL, Iron int NOT NULL, Gold int NOT NULL, Redstone int NOT NULL, Lapis int NOT NULL, Diamond int NOT NULL, PRIMARY KEY(id))");
                sql2.executeUpdate();
                PreparedStatement sql3 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerattributes(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Health double NOT NULL, Mana double NOT NULL, Rage double NOT NULL, Focus double NOT NULL, Strength int NOT NULL, Intellect int NOT NULL, Armor double NOT NULL, MagicArmor double NOT NULL, PRIMARY KEY(id))");
                sql3.executeUpdate();
                PreparedStatement sql4 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS kigndomitems(ID int NOT NULL AUTO_INCREMENT, Name varchar(60) NOT NULL, Damage int NOT NULL, LevelRequirement int NOT NULL, HasAbilities boolean NOT NULL, WeaponType varchar(20) NOT NULL, Material varchar(30), PRIMARY KEY(id))");
                sql4.executeUpdate();
                PreparedStatement sql5 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerskills(ID int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Username varchar(16) NOT NULL, AttackLevel int NOT NULL, AttackExp double NOT NULL, StrengthLevel int NOT NULL, StrengthExp double NOT NULL, DefenceLevel int NOT NULL, DefenceExp double NOT NULL, HitpointsLevel int NOT NULL, HitpointsExp double NOT NULL, RangedLevel int NOT NULL, RangedExp double NOT NULL, MagicLevel int NOT NULL, MagicExp double NOT NULL, WoodcuttingLevel int NOT NULL, WoodcuttingExp double NOT NULL, MiningLevel int NOT NULL, MiningExp double NOT NULL, CraftingLevel int NOT NULL, CraftingExp double NOT NULL, SmithingLevel int NOT NULL, SmithingExp double NOT NULL, PRIMARY KEY(id))");
                sql5.executeUpdate();
                PreparedStatement sql6 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS kingdomeconomy(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Name varchar(30) NOT NULL, Lore varchar(90) NOT NULL, Item varchar(30) NOT NULL, Amount int NOT NULL, Cost double NOT NULL, TimeLeft int NOT NULL, PRIMARY KEY(id))");
                sql6.executeUpdate();
            }else{
                openConnection();
                PreparedStatement sql = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerstats(id int NOT NULL AUTO_INCREMENT, Player varchar(60) NOT NULL, Bonds double NOT NULL, Tokens int NOT NULL, Level int NOT NULL, Exp double NOT NULL, HarvestTool varchar(6) NOT NULL, Area varchar(255) NOT NULL, SocialRank varchar(255), MilitaryRank varchar(255), PRIMARY KEY(id))");
                sql.executeUpdate();
                PreparedStatement sql2 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerinventory(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Lumber int NOT NULL, Stone int NOT NULL, Coal int NOT NULL, Iron int NOT NULL, Gold int NOT NULL, Redstone int NOT NULL, Lapis int NOT NULL, Diamond int NOT NULL, PRIMARY KEY(id))");
                sql2.executeUpdate();
                PreparedStatement sql3 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerattributes(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Health double NOT NULL, Mana double NOT NULL, Rage double NOT NULL, Focus double NOT NULL, Strength int NOT NULL, Intellect int NOT NULL, Armor double NOT NULL, MagicArmor double NOT NULL, PRIMARY KEY(id))");
                sql3.executeUpdate();
                PreparedStatement sql4 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS kigndomitems(ID int NOT NULL AUTO_INCREMENT, Name varchar(60) NOT NULL, Damage int NOT NULL, LevelRequirement int NOT NULL, HasAbilities boolean NOT NULL, WeaponType varchar(20) NOT NULL, Material varchar(30), PRIMARY KEY(id))");
                sql4.executeUpdate();
                PreparedStatement sql5 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS playerskills(ID int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Username varchar(16) NOT NULL, AttackLevel int NOT NULL, AttackExp double NOT NULL, StrengthLevel int NOT NULL, StrengthExp double NOT NULL, DefenceLevel int NOT NULL, DefenceExp double NOT NULL, HitpointsLevel int NOT NULL, HitpointsExp double NOT NULL, RangedLevel int NOT NULL, RangedExp double NOT NULL, MagicLevel int NOT NULL, MagicExp double NOT NULL, WoodcuttingLevel int NOT NULL, WoodcuttingExp double NOT NULL, MiningLevel int NOT NULL, MiningExp double NOT NULL, CraftingLevel int NOT NULL, CraftingExp double NOT NULL, SmithingLevel int NOT NULL, SmithingExp double NOT NULL, PRIMARY KEY(id))");
                sql5.executeUpdate();
                PreparedStatement sql6 = connection.prepareStatement("CREATE TABLE IF NOT EXISTS kingdomeconomy(id int NOT NULL AUTO_INCREMENT, UUID varchar(60) NOT NULL, Name varchar(30) NOT NULL, Lore varchar(90) NOT NULL, Item varchar(30) NOT NULL, Amount int NOT NULL, Cost double NOT NULL, TimeLeft int NOT NULL, PRIMARY KEY(id))");
                sql6.executeUpdate();
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            closeConnection();
        }
    }

    // Closes the MySQL Connection
    public synchronized static void closeConnection(){
        try{
            connection.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    //Checks if the player data is already created.
    public synchronized static boolean playerDataContainsPlayer(UUID uuid){
        try{
            openConnection();
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM playerstats WHERE UUID=?");
            sql.setString(1, uuid.toString());
            ResultSet resultSet = sql.executeQuery();
            boolean containsPlayer = resultSet.next();
            sql.close();
            resultSet.close();
            return containsPlayer;
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }finally{
            closeConnection();
        }
    }

    public synchronized static void updateData(UUID uuid){
        KingdomPlayer kingdomPlayer = new KingdomPlayer(uuid);
        try{
            openConnection();
            PreparedStatement sql = connection.prepareStatement("UPDATE playerstats SET Bonds=?, Tokens=?, Area=?, Level=?, Exp=? WHERE UUID=?");
            sql.setDouble(1, kingdomPlayer.getStats().getBonds());
            sql.setInt(2, kingdomPlayer.getStats().getTokens());
            sql.setString(3, kingdomPlayer.getLocation().getArea());
            sql.setInt(4, 1);
            sql.setDouble(5, 0.0);
            sql.setString(6, uuid.toString());
            sql.executeUpdate();
            sql.close();
            PreparedStatement sql2 = connection.prepareStatement("UPDATE playerinventory SET Lumber=?, Stone=?, Coal=?, Iron=?, Gold=?, Redstone=?, Lapis=?, Diamond=? WHERE UUID=?");
            sql2.setInt(1, kingdomPlayer.getInventory().getLumberBag().getLumberAmount());
            sql2.setInt(2, kingdomPlayer.getInventory().getStoneBag().getStoneAmount());
            sql2.setInt(3, 0);
            sql2.setInt(4, 0);
            sql2.setInt(5, 0);
            sql2.setInt(6, 0);
            sql2.setInt(7, 0);
            sql2.setInt(8, 0);
            sql2.setString(9, uuid.toString());
            sql2.executeUpdate();
            sql2.close();
            PreparedStatement sql3 = connection.prepareStatement("UPDATE playerattributes SET Health=?, Mana=?, Rage=?, Focus=?, Strength=?, Intellect=?, Armor=?, MagicArmor=? WHERE UUID=?");
            sql3.setDouble(1, kingdomPlayer.getStats().getAttributes().getHealth());
            sql3.setDouble(2, kingdomPlayer.getStats().getAttributes().getMana());
            sql3.setDouble(3, kingdomPlayer.getStats().getAttributes().getRage());
            sql3.setDouble(4, kingdomPlayer.getStats().getAttributes().getFocus());
            sql3.setInt(5, kingdomPlayer.getStats().getAttributes().getStrength());
            sql3.setInt(6, kingdomPlayer.getStats().getAttributes().getIntellect());
            sql3.setDouble(7, kingdomPlayer.getStats().getAttributes().getArmor());
            sql3.setDouble(8, kingdomPlayer.getStats().getAttributes().getMagicArmor());
            sql3.setString(9, uuid.toString());
            sql3.executeUpdate();
            sql3.close();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            closeConnection();
        }
    }

    public synchronized static void createData(UUID uuid){
        KingdomPlayer kingdomPlayer = new KingdomPlayer(uuid);
        try{
            openConnection();
            PreparedStatement sql = connection.prepareStatement("INSERT INTO playerstats(UUID, Bonds, Tokens, Level, Exp, HarvestTool, Area) values(?, ?, ?, ?, ?, ?, ?)");
            sql.setString(1, uuid.toString());
            sql.setDouble(2, kingdomPlayer.getStats().getBonds());
            sql.setInt(3, kingdomPlayer.getStats().getTokens());
            sql.setInt(4, 1);
            sql.setDouble(5, 0.0);
            sql.setString(6, "false");
            sql.setString(7, kingdomPlayer.getLocation().getArea());
            sql.executeUpdate();
            sql.close();
            PreparedStatement sql2 = connection.prepareStatement("INSERT INTO playerattributes(UUID, Health, Mana, Rage, Focus, Strength, Intellect, Armor, MagicArmor) values(?, 100, 100, 0.0, 0.0, 0, 0, 10, 0)");
            sql2.setString(1, uuid.toString());
            sql2.executeUpdate();
            sql2.close();
            PreparedStatement sql3 = connection.prepareStatement("INSERT INTO playerinventory(UUID, Lumber, Stone, Coal, Iron, Gold, Redstone, Lapis, Diamond) values(?, 0, 0, 0, 0, 0, 0, 0, 0)");
            sql3.setString(1, uuid.toString());
            sql3.executeUpdate();
            sql3.close();
            createSkillData(uuid);
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            closeConnection();
        }
    }

    public static void createSkillData(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            MySQLAdapter.openConnection();
            PreparedStatement sql = MySQLAdapter.connection.prepareStatement("INSERT INTO playerskills(UUID, Username, AttackLevel, AttackExp, StrengthLevel, StrengthExp, DefenceLevel, DefenceExp, HitpointsLevel, HitpointsExp, RangedLevel, RangedExp, MagicLevel, MagicExp, WoodcuttingLevel, WoodcuttingExp, MiningLevel, MiningExp, CraftingLevel, CraftingExp, SmithingLevel, SmithingExp) values(?, ?, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0, 1, 0.0)");
            sql.setString(1, uuid.toString());
            sql.setString(2, Bukkit.getPlayer(uuid).getName());
            sql.executeUpdate();
            sql.close();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            MySQLAdapter.closeConnection();
        }
    }

}
