package net.genflowstudios.kingdoms.data;

import net.genflowstudios.kingdoms.Engine;
import org.bukkit.configuration.file.FileConfiguration;

public class SettingsHandler{

    Engine kingdoms;


    public boolean isFlatfileMode(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getBoolean("Flatfile");
    }

    public String getWorld(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getString("World");
    }

    public String getPluginMode(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getString("Plugin-Mode");
    }

    public boolean isQuestsEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").getBoolean("Quests-Enabled");
    }

    public boolean isGlobalEconomyEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").getBoolean("Global-Economy-Enabled");
    }

    public boolean isVaultEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").getBoolean("Vault-Enabled");
    }

    public boolean isRacesEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Races-Enabled");
    }

    public boolean isClassesEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Classes-Enabled");
    }

    public boolean isSocialRanksEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Social-Ranks-Enabled");
    }

    public boolean isMilitaryRanksEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Military-Ranks-Enabled");
    }

    public boolean isKingdomsEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Kingdoms-Enabled");
    }

    public boolean isCitiesEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Cities-Enabled");
    }

    public boolean isGuildsEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").getBoolean("Guilds-Enabled");
    }


}
