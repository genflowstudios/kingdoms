package net.genflowstudios.kingdoms.data;

import net.genflowstudios.kingdoms.items.KingdomItem;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.skills.KPlayerSkills;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 6/17/2015.
 */
public class PlayerLoadHandler{


    public void loadInventory(UUID uuid){
        SettingsHandler settingsHandler = new SettingsHandler();
        if(settingsHandler.isFlatfileMode()){

        }else{
            loadBags(uuid);
        }
    }

    private void loadKingdomItems(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            for(KingdomItem item : getItems(uuid)){
                kingdomPlayer.getInventory().addKingdomItem(item);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList<KingdomItem> getItems(UUID uuid){
        FileConfiguration cfg = ConfigManager.getPlayerConfig(uuid);
        ArrayList<KingdomItem> items = new ArrayList<KingdomItem>();
        for(Integer id : cfg.getConfigurationSection("Inventory").getIntegerList("ID")){
            items.add(KingdomItem.getItem(id));
        }
        return items;
    }

    private void loadBags(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            MySQLAdapter.openConnection();
            PreparedStatement sql = MySQLAdapter.connection.prepareStatement("SELECT * FROM playerinventory WHERE UUID=?");
            sql.setString(1, uuid.toString());
            ResultSet resultSet = sql.executeQuery();
            resultSet.next();
            kingdomPlayer.getInventory().getLumberBag().setLumberAmount(resultSet.getInt("Lumber"));
            kingdomPlayer.getInventory().getStoneBag().setStoneAmount(resultSet.getInt("Stone"));
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            MySQLAdapter.closeConnection();
        }
    }

    public void loadAttributes(UUID uuid){
        SettingsHandler settingsHandler = new SettingsHandler();
        if(settingsHandler.isFlatfileMode()){

        }else{
            try{
                KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
                MySQLAdapter.openConnection();
                PreparedStatement sql = MySQLAdapter.connection.prepareStatement("SELECT * FROM playerattributes WHERE UUID=?");
                sql.setString(1, uuid.toString());
                ResultSet resultSet = sql.executeQuery();
                resultSet.next();
                kingdomPlayer.getStats().getAttributes().setHealth(resultSet.getDouble("Health"));
                kingdomPlayer.getStats().getAttributes().setMana(resultSet.getDouble("Mana"));
                kingdomPlayer.getStats().getAttributes().setRage(resultSet.getDouble("Rage"));
                kingdomPlayer.getStats().getAttributes().setFocus(resultSet.getDouble("Focus"));
                kingdomPlayer.getStats().getAttributes().setStrength(resultSet.getInt("Strength"));
                kingdomPlayer.getStats().getAttributes().setIntellect(resultSet.getInt("Intellect"));
                kingdomPlayer.getStats().getAttributes().setArmor(resultSet.getDouble("Armor"));
                kingdomPlayer.getStats().getAttributes().setMagicArmor(resultSet.getDouble("MagicArmor"));
                resultSet.close();
                sql.close();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                MySQLAdapter.closeConnection();
            }
        }
    }

    public void loadStats(UUID uuid){
        SettingsHandler settingsHandler = new SettingsHandler();
        if(settingsHandler.isFlatfileMode()){

        }else{
            try{
                KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
                MySQLAdapter.openConnection();
                PreparedStatement sql = MySQLAdapter.connection.prepareStatement("SELECT * FROM playerstats WHERE UUID=?");
                sql.setString(1, uuid.toString());
                ResultSet resultSet = sql.executeQuery();
                resultSet.next();
                kingdomPlayer.getStats().setBonds(resultSet.getDouble("Bonds"));
                kingdomPlayer.getStats().setTokens(resultSet.getInt("Tokens"));
                resultSet.close();
                sql.close();
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                MySQLAdapter.closeConnection();
            }
        }
    }

    public void loadSkills(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            KPlayerSkills skills = kingdomPlayer.getStats().getSkills();
            FileConfiguration cfg = ConfigManager.getPlayerConfig(uuid);
            for(SkillType type : SkillType.getSkills()){
                skills.setSkillLevel(type, cfg.getConfigurationSection("Stats").getConfigurationSection("Skills").getInt(type.getName()));
                skills.setSkillExp(type, cfg.getConfigurationSection("Stats").getConfigurationSection("Skills").getDouble(type.getName() + " Exp"));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void loadPlayer(UUID uuid){
        loadStats(uuid);
        loadSkills(uuid);
        loadAttributes(uuid);
        loadInventory(uuid);
    }


}
