package net.genflowstudios.kingdoms.data;

import net.genflowstudios.kingdoms.items.items.weapons.WeaponList;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Samuel on 6/17/2015.
 */
public class PlayerSavingHandler{

    public static ArrayList<UUID> players = new ArrayList<UUID>();
    public static HashMap<UUID, ArrayList<ItemStack>> items = new HashMap<UUID, ArrayList<ItemStack>>();


    public void saveInventory(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            MySQLAdapter.openConnection();
            PreparedStatement sql2 = MySQLAdapter.connection.prepareStatement("UPDATE playerinventory SET Lumber=?, Stone=?, Coal=?, Iron=?, Gold=?, Redstone=?, Lapis=?, Diamond=? WHERE UUID=?");
            sql2.setInt(1, kingdomPlayer.getInventory().getLumberBag().getLumberAmount());
            sql2.setInt(2, kingdomPlayer.getInventory().getStoneBag().getStoneAmount());
            sql2.setInt(3, 0);
            sql2.setInt(4, 0);
            sql2.setInt(5, 0);
            sql2.setInt(6, 0);
            sql2.setInt(7, 0);
            sql2.setInt(8, 0);
            sql2.setString(9, uuid.toString());
            sql2.executeUpdate();
            sql2.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        saveKingdomItems(uuid);
    }

    private void saveKingdomItems(UUID uuid){
        FileConfiguration cfg = ConfigManager.getPlayerConfig(uuid);
        cfg.getConfigurationSection("Inventory").set("ID", getItems(uuid));
        ConfigManager.savePlayerConfig(uuid, cfg);
    }

    private ArrayList<Integer> getItems(UUID uuid){
        ArrayList<Integer> listItems = new ArrayList<Integer>();
        for(ItemStack item : items.get(uuid)){
            if(item == null || item.getType().equals(Material.AIR)){

            }else{
                for(WeaponList weapon : WeaponList.getWeapons()){
                    if(weapon.getMaterial() == Material.AIR){

                    }else{
                        if(weapon.getMaterial().equals(item.getType())){
                            if(item.hasItemMeta()){
                                if(item.getItemMeta().hasDisplayName()){
                                    if(item.getItemMeta().getDisplayName().equals(weapon.getName())){
                                        listItems.add(weapon.getID());
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
        return listItems;
    }

    public void saveAttributes(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            PreparedStatement sql3 = MySQLAdapter.connection.prepareStatement("UPDATE playerattributes SET Health=?, Mana=?, Rage=?, Focus=?, Strength=?, Intellect=?, Armor=?, MagicArmor=? WHERE UUID=?");
            sql3.setDouble(1, kingdomPlayer.getStats().getAttributes().getHealth());
            sql3.setDouble(2, kingdomPlayer.getStats().getAttributes().getMana());
            sql3.setDouble(3, kingdomPlayer.getStats().getAttributes().getRage());
            sql3.setDouble(4, kingdomPlayer.getStats().getAttributes().getFocus());
            sql3.setInt(5, kingdomPlayer.getStats().getAttributes().getStrength());
            sql3.setInt(6, kingdomPlayer.getStats().getAttributes().getIntellect());
            sql3.setDouble(7, kingdomPlayer.getStats().getAttributes().getArmor());
            sql3.setDouble(8, kingdomPlayer.getStats().getAttributes().getMagicArmor());
            sql3.setString(9, uuid.toString());
            sql3.executeUpdate();
            sql3.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void saveStats(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            MySQLAdapter.openConnection();
            PreparedStatement sql = MySQLAdapter.connection.prepareStatement("UPDATE playerstats SET Bonds=?, Tokens=?, Area=?, Level=?, Exp=? WHERE UUID=?");
            sql.setDouble(1, kingdomPlayer.getStats().getBonds());
            sql.setInt(2, kingdomPlayer.getStats().getTokens());
            sql.setString(3, kingdomPlayer.getLocation().getArea());
            sql.setInt(4, 1);
            sql.setDouble(5, 0.0);
            sql.setString(6, uuid.toString());
            sql.executeUpdate();
            sql.close();
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            MySQLAdapter.closeConnection();
        }
    }

    public void saveSkills(UUID uuid){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            MySQLAdapter.openConnection();
            PreparedStatement sql = MySQLAdapter.connection.prepareStatement("UPDATE playerskills SET AttackLevel=?, AttackExp=?, StrengthLevel=?, StrengthExp=?, DefenceLevel=?, DefenceExp=?, HitpointsLevel=?, HitpointsExp=?, RangedLevel=?, RangedExp=?, MagicLevel=?, MagicExp=?, WoodcuttingLevel=?, WoodcuttingExp=?, MiningLevel=?, MiningExp=?, CraftingLevel=?, CraftingExp=?, SmithingLevel=?, SmithingExp=?, Username=? WHERE UUID=?");
            sql.setInt(1, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.ATTACK));
            sql.setDouble(2, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.ATTACK));
            sql.setInt(3, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.STRENGTH));
            sql.setDouble(4, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.STRENGTH));
            sql.setInt(5, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.DEFENCE));
            sql.setDouble(6, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.DEFENCE));
            sql.setInt(7, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.HITPOINTS));
            sql.setDouble(8, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.HITPOINTS));
            sql.setInt(9, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.RANGED));
            sql.setDouble(10, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.RANGED));
            sql.setInt(11, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.MAGIC));
            sql.setDouble(12, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.MAGIC));
            sql.setInt(13, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.WOODCUTTING));
            sql.setDouble(14, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.WOODCUTTING));
            sql.setInt(15, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.MINING));
            sql.setDouble(16, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.MINING));
            sql.setInt(17, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.CRAFTING));
            sql.setDouble(18, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.CRAFTING));
            sql.setInt(19, kingdomPlayer.getStats().getSkills().getSkillLevel(SkillType.SMITHING));
            sql.setDouble(20, kingdomPlayer.getStats().getSkills().getSkillExp(SkillType.SMITHING));
            sql.setString(21, kingdomPlayer.getUsername());
            sql.setString(22, uuid.toString());
            sql.executeUpdate();
            sql.close();
        }catch(Exception e){
            e.printStackTrace();
        }

    }


    public void savePlayer(UUID uuid){
        saveInventory(uuid);
        saveAttributes(uuid);
        saveSkills(uuid);
        saveStats(uuid);
    }


    public void savePlayers(){
        for(UUID uuid : players){
            savePlayer(uuid);
        }
    }
}
