package net.genflowstudios.kingdoms.data;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ConfigChangeHandler{

    static Engine kingdoms;


    public void generatePlayerConfig(UUID uuid, FileConfiguration cfgPlayer){
        Location playerLocation = Bukkit.getPlayer(uuid).getLocation();
        int x = playerLocation.getBlockX();
        int y = playerLocation.getBlockY();
        int z = playerLocation.getBlockZ();
        int xChunk = playerLocation.getChunk().getX();
        int zChunk = playerLocation.getChunk().getZ();
        List<String> settlements = new ArrayList<String>();
        List<String> cities = new ArrayList<String>();
        cfgPlayer.createSection("Stats");
        cfgPlayer.createSection("Settings");
        cfgPlayer.createSection("Location");
        cfgPlayer.createSection("Owned-Areas");
        cfgPlayer.createSection("Inventory");
        cfgPlayer.getConfigurationSection("Owned-Areas").set("Settlements", settlements);
        cfgPlayer.getConfigurationSection("Owned-Areas").set("Cities", cities);
        cfgPlayer.getConfigurationSection("Stats").createSection("Race/Class");
        cfgPlayer.getConfigurationSection("Stats").createSection("Kingdom-Stats");
        cfgPlayer.getConfigurationSection("Stats").createSection("Player-Stats");
        cfgPlayer.getConfigurationSection("Stats").createSection("Race/Class");
        cfgPlayer.getConfigurationSection("Stats").createSection("Social/Military");
        cfgPlayer.getConfigurationSection("Stats").createSection("Skills");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Bonds", 0.0);
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Tokens", 0);
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Level", 1);
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Kills", 0);
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Deaths", 0);
        for(SkillType type : SkillType.getSkills()){
            cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Skills").set(type.getName(), 1);
            cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Skills").set(type.getName() + " Exp", 0.0);
        }
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Kingdom-Stats").set("Kingdom", "**");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Kingdom-Stats").set("Guild", "**");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Race/Class").set("Race", "**");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Race/Class").set("Class", "**");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Race/Class").set("Specialty", "**");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Social-Rank", "Forigner");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Quest-Points", 0);
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Military-Rank", "**");
        cfgPlayer.getConfigurationSection("Stats").getConfigurationSection("Social/Military").set("Badges", 0);
        cfgPlayer.getConfigurationSection("Settings").set("Harvest-Tool", false);
        cfgPlayer.getConfigurationSection("Settings").set("Scoreboard-Mode", false);
        cfgPlayer.getConfigurationSection("Settings").set("Build-Tool", false);
        cfgPlayer.getConfigurationSection("Location").set("Area", "Wilderness");
        cfgPlayer.getConfigurationSection("Location").set("x", x);
        cfgPlayer.getConfigurationSection("Location").set("y", y);
        cfgPlayer.getConfigurationSection("Location").set("z", z);
        cfgPlayer.getConfigurationSection("Location").set("xChunk", xChunk);
        cfgPlayer.getConfigurationSection("Location").set("zChunk", zChunk);
        cfgPlayer.getConfigurationSection("Location").set("Chunks", xChunk + ", " + zChunk);
        cfgPlayer.getConfigurationSection("Inventory").set("ID", new ArrayList<Integer>());
        ConfigManager.savePlayerConfig(uuid, cfgPlayer);
    }


    public void savePlayerConfigs(UUID uuid){
        FileConfiguration cfg = ConfigManager.getPlayerConfig(uuid);
        savePlayerBonds(uuid, cfg);
        savePlayerTokens(uuid, cfg);
        savePlayerSkills(uuid, cfg);
        savePlayerLocation(uuid, cfg);
    }


    public void savePlayerLocation(UUID uuid, FileConfiguration cfg){
        Player player = Bukkit.getPlayer(uuid);
        Location location = player.getLocation();
        int x = location.getBlockX();
        int y = location.getBlockY();
        int z = location.getBlockZ();
        int xChunk = location.getChunk().getX();
        int zChunk = location.getChunk().getZ();
        cfg.getConfigurationSection("Location").set("x", x);
        cfg.getConfigurationSection("Location").set("y", y);
        cfg.getConfigurationSection("Location").set("z", z);
        cfg.getConfigurationSection("Location").set("xChunk", xChunk);
        cfg.getConfigurationSection("Location").set("zChunk", zChunk);
        cfg.getConfigurationSection("Location").set("Chunks", xChunk + ", " + zChunk);
        ConfigManager.savePlayerConfig(player.getUniqueId(), cfg);
    }



    public void savePlayerSkills(UUID uuid, FileConfiguration cfg){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            for(SkillType type : SkillType.getSkills()){
                cfg.getConfigurationSection("Stats").getConfigurationSection("Skills").set(type.getName(), kingdomPlayer.getStats().getSkills().getSkillLevel(type));
                cfg.getConfigurationSection("Stats").getConfigurationSection("Skills").set(type.getName() + " Exp", kingdomPlayer.getStats().getSkills().getSkillExp(type));
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void savePlayerTokens(UUID uuid, FileConfiguration cfg){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            cfg.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Tokens", kingdomPlayer.getStats().getTokens());
            ConfigManager.savePlayerConfig(uuid, cfg);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void savePlayerBonds(UUID uuid, FileConfiguration cfg){
        try{
            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(uuid);
            cfg.getConfigurationSection("Stats").getConfigurationSection("Player-Stats").set("Tokens", kingdomPlayer.getStats().getBonds());
            ConfigManager.savePlayerConfig(uuid, cfg);
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    public void generateSettingsFile(FileConfiguration cfg){
        FileConfiguration cfgArea = ConfigManager.getDefaultAreaConfig();
        cfgArea.createSection("Areas");
        cfgArea.getConfigurationSection("Areas").set("Area-Names", new ArrayList<String>());
        cfgArea.createSection("Server-Areas");
        cfgArea.getConfigurationSection("Server-Areas").set("Area-Names", new ArrayList<String>());
        cfgArea.createSection("Settings");
        cfgArea.getConfigurationSection("Settings").set("Default-Y-Amount-Down", 3);
        cfgArea.getConfigurationSection("Settings").set("Default-Y-Amount-Up", 20);
        cfgArea.getConfigurationSection("Settings").set("Default-Y-Amount-Down", 3);
        cfgArea.getConfigurationSection("Settings").set("Default-X-Amount-Both", 7);
        cfgArea.getConfigurationSection("Settings").set("Default-Z-Amount-Both", 7);
        cfg.createSection("Settings");
        cfg.getConfigurationSection("Settings").set("Flatfile", true);
        cfg.getConfigurationSection("Settings").createSection("Gameplay-Settings");
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").set("Plugin-Mode", "Free-Roam");
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").set("World", "Dravensgar");
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").createSection("Free-Roam-Settings");
        cfg.getConfigurationSection("Settings").createSection("Plugin-Hook-Settings");
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Kingdoms-Enabled", false);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Cities-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Guilds-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Races-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Classes-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Specialies-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Social-Ranks-Enabled", false);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Gameplay-Settings").getConfigurationSection("Free-Roam-Settings").set("Military-Ranks-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Quests-Enabled", false);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Vault-Enabled", true);
        cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").set("Global-Economy-Enabled", true);
        cfg.createSection("MySQL");
        cfg.getConfigurationSection("MySQL").set("Host-IP", "localhost");
        cfg.getConfigurationSection("MySQL").set("Database", "kingdoms");
        cfg.getConfigurationSection("MySQL").set("Port", "3306");
        cfg.getConfigurationSection("MySQL").set("Username", "root");
        cfg.getConfigurationSection("MySQL").set("Password", "pass");
        ConfigManager.saveDefaultConfig(cfg);
        ConfigManager.saveDefaultAreaConfig(cfgArea);
    }



}
