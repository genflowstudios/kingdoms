package net.genflowstudios.kingdoms.data;

import net.genflowstudios.kingdoms.Engine;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

public class ConfigManager{

    static Engine kingdom;

    //Returns default config.
    public static FileConfiguration getDefaultConfig(){
        String fileName = "config" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves default config.
    public static void saveDefaultConfig(FileConfiguration cfg){
        String fileName = "config" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //Returns sign config.
    public static FileConfiguration getSignConfig(){
        String fileName = "sign" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves sign config.
    public static void saveSignConfig(FileConfiguration cfg){
        String fileName = "sign" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //Returns player config.
    public static FileConfiguration getPlayerConfig(UUID name){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Players");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves player config.
    public static void savePlayerConfig(UUID uuid, FileConfiguration cfg){
        String fileName = uuid + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Players");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //Returns kingdom config.
    public static FileConfiguration getKingdomRegionConfig(String name){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Regions");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves kingdom config.
    public static void saveKingdomRegionConfig(String name, FileConfiguration cfg){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Regions");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //Returns city config.
    public static FileConfiguration getCityConfig(String name, String subRegion){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Regions//" + subRegion + "//Cities");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves city config.
    public static void saveCityConfig(String name, String subRegion, FileConfiguration cfg){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Regions//" + subRegion + "//Cities");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //Returns city config.
    public static FileConfiguration getSubRegionConfig(String name){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Regions//" + name);
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves city config.
    public static void saveSubRegionConfig(String name, FileConfiguration cfg){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Regions//" + name);
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

    //Returns default city config.
    public static FileConfiguration getDefaultAreaConfig(){
        String fileName = "areaconfig" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves default city config.
    public static void saveDefaultAreaConfig(FileConfiguration cfg){
        String fileName = "areaconfig" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

}
