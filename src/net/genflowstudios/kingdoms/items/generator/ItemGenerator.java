package net.genflowstudios.kingdoms.items.generator;

import net.genflowstudios.kingdoms.items.ItemRarityType;
import net.genflowstudios.kingdoms.items.ItemType;
import net.genflowstudios.kingdoms.items.KingdomItem;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ItemGenerator{


    public ItemGenerator(int number){
        generate(number);
    }

    private void generate(int number){
        for(int i = 0; i <= number; i++){
            Random r = new Random();
            new KingdomItem(getNames().get(r.nextInt(getNames().size())), getDescriptions().get(r.nextInt(getDescriptions().size())), getItemTypes().get(r.nextInt(getItemTypes().size())), getDamageValues().get(r.nextInt(getDamageValues().size())), getItemRarityTypes().get(r.nextInt(getItemRarityTypes().size())), getMaterials().get(r.nextInt(getMaterials().size())));
        }
    }


    private ArrayList<String> getNames(){
        ArrayList<String> names = new ArrayList<String>();
        //Add names here!
        return names;
    }

    private ArrayList<List<String>> getDescriptions(){
        ArrayList<List<String>> descs = new ArrayList<List<String>>();
        //Add descriptions here!
        return descs;
    }

    private ArrayList<ItemType> getItemTypes(){
        ArrayList<ItemType> itemTypes = new ArrayList<ItemType>();
        //Add types here!
        return itemTypes;
    }

    private ArrayList<Integer> getDamageValues(){
        ArrayList<Integer> damageValues = new ArrayList<Integer>();
        //Add damage values here!
        return damageValues;
    }

    private ArrayList<ItemRarityType> getItemRarityTypes(){
        ArrayList<ItemRarityType> itemRarity = new ArrayList<ItemRarityType>();
        //Add item rarities here!
        return itemRarity;
    }

    private ArrayList<Material> getMaterials(){
        ArrayList<Material> materials = new ArrayList<Material>();
        //Add item rarities here!
        return materials;
    }

}
