package net.genflowstudios.kingdoms.items;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class KingdomItem implements ItemProperties, Listener{

    public static ArrayList<KingdomItem> items = new ArrayList<KingdomItem>();
    int id;
    String name;
    List<String> description;
    ItemType type;
    int damageValue;
    ItemRarityType rarityType;
    KingdomItem item;
    ItemStack itemstack;
    ItemMeta meta;
    Material material;

    public KingdomItem(){

    }

    public KingdomItem(String name, List<String> description, ItemType type, int damageValue, ItemRarityType rarityType, Material material){
        if(items.isEmpty()){
            this.id = 1;
        }else{
            this.id = items.size() + 1;
        }
        this.name = name;
        this.description = description;
        this.type = type;
        this.damageValue = damageValue;
        this.rarityType = rarityType;
        this.material = material;
        this.itemstack = new ItemStack(material, 1);
        this.meta = itemstack.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(description);
        itemstack.setItemMeta(meta);
        this.item = this;
        items.add(item);
    }

    public static KingdomItem getItem(int id){
        for(KingdomItem item : items){
            if(item.getId() == id){
                return item;
            }
        }
        return items.get(0);
    }


    public static void giveItem(Player player, String name){
        for(KingdomItem item : items){
            if(item.getName().equals(name)){
                player.getInventory().addItem(item.getItemstack());
            }
        }
    }

    @EventHandler
    public void onDamageEvent(EntityDamageByEntityEvent event){
        //TODO - TO BE EXTENDED
    }

    @EventHandler
    public void onRightClick(PlayerInteractEvent event){

    }

    @Override
    public String getName(){
        return name;
    }

    @Override
    public void setName(String name){
        this.name = name;
    }

    @Override
    public List<String> getDescription(){
        return description;
    }

    @Override
    public void setDescription(List<String> desc){
        this.description = desc;
    }

    @Override
    public ItemType getItemType(){
        return type;
    }

    @Override
    public void setItemType(ItemType type){
        this.type = type;
    }

    @Override
    public int getDamageValue(){
        return damageValue;
    }

    @Override
    public void setDamageValue(int damageValue){
        this.damageValue = damageValue;
    }

    @Override
    public ItemRarityType getItemRarity(){
        return rarityType;
    }

    @Override
    public void setItemRarity(ItemRarityType type){
        this.rarityType = type;
    }


    /**
     * @return the material
     */
    public Material getMaterial(){
        return material;
    }


    /**
     * @param material the material to set
     */
    public void setMaterial(Material material){
        this.material = material;
    }

    /**
     * @return the itemstack
     */
    public ItemStack getItemstack(){
        return itemstack;
    }

    /**
     * @param itemstack the itemstack to set
     */
    public void setItemstack(ItemStack itemstack){
        this.itemstack = itemstack;
    }


    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(o == null || getClass() != o.getClass()) return false;

        KingdomItem that = (KingdomItem) o;

        if(damageValue != that.damageValue) return false;
        if(!name.equals(that.name)) return false;
        if(!description.equals(that.description)) return false;
        if(type != that.type) return false;
        if(rarityType != that.rarityType) return false;
        if(item != null ? !item.equals(that.item) : that.item != null) return false;
        if(itemstack != null ? !itemstack.equals(that.itemstack) : that.itemstack != null) return false;
        if(meta != null ? !meta.equals(that.meta) : that.meta != null) return false;
        return material == that.material;

    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }
}
