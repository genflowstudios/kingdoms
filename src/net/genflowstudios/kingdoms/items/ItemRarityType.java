package net.genflowstudios.kingdoms.items;

public enum ItemRarityType{

    COMMON,
    UN_COMMON,
    RARE,
    EPIC,
    LEDGENDARY,
    ARTIFACT

}
