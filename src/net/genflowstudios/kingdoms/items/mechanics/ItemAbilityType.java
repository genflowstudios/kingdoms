package net.genflowstudios.kingdoms.items.mechanics;

public enum ItemAbilityType{


    PHYSICAL,
    MAGICAL,
    BUFF,
    PASSIVE


}
