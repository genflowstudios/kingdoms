package net.genflowstudios.kingdoms.items.mechanics;

public interface ItemAbilityProperties{

    boolean usesMana();

    int getManaAmountUse();

    void setManaAmountUse(int amount);

    int getDamageInflict();

    void setDamageInflict(int amount);

    void setUsesMana(boolean value);

}
