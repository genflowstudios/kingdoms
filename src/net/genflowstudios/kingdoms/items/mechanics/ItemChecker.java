package net.genflowstudios.kingdoms.items.mechanics;

import net.genflowstudios.kingdoms.Engine;
import net.genflowstudios.kingdoms.items.items.weapons.WeaponList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Samuel on 6/27/2015.
 */
public class ItemChecker{


    int id;

    public ItemChecker(){
        this.id = Bukkit.getScheduler().scheduleSyncRepeatingTask(Engine.getInstance(), new Runnable(){
            @Override
            public void run(){
                if(Bukkit.getOnlinePlayers().isEmpty()){

                }else{
                    for(Player player : Bukkit.getOnlinePlayers()){
                        for(ItemStack item : player.getInventory().getContents()){
                            if(item == null || item.getType() == Material.AIR){

                            }else{
                                if(item.getItemMeta().hasDisplayName()){

                                }else{
                                    for(WeaponList weapon : WeaponList.getWeapons()){
                                        if(item.getType().equals(weapon.getMaterial())){
                                            player.getInventory().remove(item);
                                            ItemMeta meta = item.getItemMeta();
                                            meta.setDisplayName(weapon.getName());
                                            item.setItemMeta(meta);
                                            player.getInventory().addItem(item);
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }, 0, 20);
    }

}
