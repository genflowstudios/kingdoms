package net.genflowstudios.kingdoms.items;

import java.util.List;

public interface ItemProperties{


    String getName();

    void setName(String name);

    List<String> getDescription();

    void setDescription(List<String> desc);

    ItemType getItemType();

    void setItemType(ItemType type);

    int getDamageValue();

    void setDamageValue(int damageValue);

    ItemRarityType getItemRarity();

    void setItemRarity(ItemRarityType type);


}
