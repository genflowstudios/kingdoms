package net.genflowstudios.kingdoms.items.harvesting;

import net.genflowstudios.kingdoms.player.skills.SkillType;
import org.bukkit.Material;

import java.util.ArrayList;

/**
 * Created by Samuel on 6/17/2015.
 */
public enum HarvestingItem{

    LOG{
        @Override
        public int getHarvestLevel(){
            return 0;
        }

        @Override
        public Material getMaterial(){
            return Material.LOG;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.WOODCUTTING;
        }

        @Override
        public String getName(){
            return "Lumber";
        }

        @Override
        public double getAwardedExp(){
            return 3.7;
        }

        @Override
        public double getAmountTimes(){
            return 1;
        }
    },
    LOG_2{
        @Override
        public int getHarvestLevel(){
            return 0;
        }

        @Override
        public Material getMaterial(){
            return Material.LOG_2;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.WOODCUTTING;
        }

        @Override
        public String getName(){
            return "Lumber";
        }

        @Override
        public double getAwardedExp(){
            return 3.7;
        }

        @Override
        public double getAmountTimes(){
            return 1;
        }
    },
    STONE{
        @Override
        public int getHarvestLevel(){
            return 0;
        }

        @Override
        public Material getMaterial(){
            return Material.STONE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Stone";
        }

        @Override
        public double getAwardedExp(){
            return 3.2;
        }

        @Override
        public double getAmountTimes(){
            return 1;
        }
    },
    COBBLESTONE{
        @Override
        public int getHarvestLevel(){
            return 0;
        }

        @Override
        public Material getMaterial(){
            return Material.COBBLESTONE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Stone";
        }

        @Override
        public double getAwardedExp(){
            return 3.2;
        }

        @Override
        public double getAmountTimes(){
            return 1;
        }
    },
    COAL_ORE{
        @Override
        public int getHarvestLevel(){
            return 10;
        }

        @Override
        public Material getMaterial(){
            return Material.COAL_ORE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Coal";
        }

        @Override
        public double getAwardedExp(){
            return 5;
        }

        @Override
        public double getAmountTimes(){
            return 1.2;
        }
    },
    IRON_ORE{
        @Override
        public int getHarvestLevel(){
            return 20;
        }

        @Override
        public Material getMaterial(){
            return Material.IRON_ORE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Iron ore";
        }

        @Override
        public double getAwardedExp(){
            return 8;
        }

        @Override
        public double getAmountTimes(){
            return 1.6;
        }
    },
    GOLD_ORE{
        @Override
        public int getHarvestLevel(){
            return 30;
        }

        @Override
        public Material getMaterial(){
            return Material.GOLD_ORE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Gold ore";
        }

        @Override
        public double getAwardedExp(){
            return 8.2;
        }

        @Override
        public double getAmountTimes(){
            return 1.7;
        }
    },
    REDSTONE_ORE{
        @Override
        public int getHarvestLevel(){
            return 30;
        }

        @Override
        public Material getMaterial(){
            return Material.REDSTONE_ORE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Redstone";
        }

        @Override
        public double getAwardedExp(){
            return 9;
        }

        @Override
        public double getAmountTimes(){
            return 1.7;
        }
    },
    LAPIS_ORE{
        @Override
        public int getHarvestLevel(){
            return 30;
        }

        @Override
        public Material getMaterial(){
            return Material.LAPIS_ORE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Lapis";
        }

        @Override
        public double getAwardedExp(){
            return 9;
        }

        @Override
        public double getAmountTimes(){
            return 1.7;
        }
    },
    DIAMOND_ORE{
        @Override
        public int getHarvestLevel(){
            return 40;
        }

        @Override
        public Material getMaterial(){
            return Material.DIAMOND_ORE;
        }

        @Override
        public SkillType getSkill(){
            return SkillType.MINING;
        }

        @Override
        public String getName(){
            return "Diamond";
        }

        @Override
        public double getAwardedExp(){
            return 12.19;
        }

        @Override
        public double getAmountTimes(){
            return 3.0;
        }
    };


    public static HarvestingItem getHarvestingItem(Material material){
        for(HarvestingItem harvestingItem : getItems()){
            if(harvestingItem.getMaterial().equals(material)){
                return harvestingItem;
            }
        }
        return null;
    }

    public static double getTimeTillHarvest(int level, double amountTimes){
        if(level < 5){
            return 4 * amountTimes;
        }else if(level >= 5 && level < 10){
            return 3.75 * amountTimes;
        }else if(level >= 10 && level < 20){
            return 3.25 * amountTimes;
        }else if(level >= 20 && level < 30){
            return 2.90 * amountTimes;
        }else if(level >= 30 && level < 40){
            return 2.50 * amountTimes;
        }else if(level >= 40 && level < 50){
            return 2.09 * amountTimes;
        }else{
            return 2.09 * amountTimes;
        }
    }

    public static int getAmountHarvested(int level){
        if(level < 20){
            return 2;
        }else if(level > 20 && level < 40){
            return 3;
        }else if(level > 40 && level < 60){
            return 4;
        }else{
            return 4;
        }
    }

    public static ArrayList<HarvestingItem> getItems(){
        ArrayList<HarvestingItem> items = new ArrayList<HarvestingItem>();
        items.add(LOG);
        items.add(LOG_2);
        items.add(STONE);
        items.add(COBBLESTONE);
        items.add(COAL_ORE);
        items.add(IRON_ORE);
        items.add(GOLD_ORE);
        items.add(REDSTONE_ORE);
        items.add(LAPIS_ORE);
        items.add(DIAMOND_ORE);
        return items;
    }

    public static ArrayList<Material> getMaterials(){
        ArrayList<Material> items = new ArrayList<Material>();
        items.add(LOG.getMaterial());
        items.add(LOG_2.getMaterial());
        items.add(STONE.getMaterial());
        items.add(COBBLESTONE.getMaterial());
        items.add(COAL_ORE.getMaterial());
        items.add(IRON_ORE.getMaterial());
        items.add(GOLD_ORE.getMaterial());
        items.add(REDSTONE_ORE.getMaterial());
        items.add(LAPIS_ORE.getMaterial());
        items.add(DIAMOND_ORE.getMaterial());
        return items;
    }

    public abstract int getHarvestLevel();

    public abstract Material getMaterial();

    public abstract SkillType getSkill();

    public abstract String getName();

    public abstract double getAwardedExp();

    public abstract double getAmountTimes();

}
