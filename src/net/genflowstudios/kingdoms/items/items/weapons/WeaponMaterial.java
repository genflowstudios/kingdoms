package net.genflowstudios.kingdoms.items.items.weapons;

import org.bukkit.Material;

import java.util.ArrayList;

/**
 * Created by Samuel on 6/25/2015.
 */
public enum WeaponMaterial{

    //Swords and Spears

    STONE{
        @Override
        public int getLevelRequirement(){
            return 1;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 4;
            }else if(type.equals(WeaponType.SPEAR)){
                return 5;
            }else{
                return 1;
            }

        }

        @Override
        public Material getMaterialEquil(){
            return Material.STONE;
        }
    },
    RUSTY{
        @Override
        public int getLevelRequirement(){
            return 5;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 8;
            }else if(type.equals(WeaponType.SPEAR)){
                return 10;
            }else{
                return 5;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.WOOD;
        }
    },
    BRONZE{
        @Override
        public int getLevelRequirement(){
            return 10;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 12;
            }else if(type.equals(WeaponType.SPEAR)){
                return 14;
            }else{
                return 8;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.WOOD;
        }
    },
    IRON{
        @Override
        public int getLevelRequirement(){
            return 20;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 19;
            }else if(type.equals(WeaponType.SPEAR)){
                return 23;
            }else{
                return 15;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.IRON_INGOT;
        }
    },
    STEEL{
        @Override
        public int getLevelRequirement(){
            return 25;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 24;
            }else if(type.equals(WeaponType.SPEAR)){
                return 27;
            }else{
                return 20;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.IRON_INGOT;
        }
    },
    DAGONION_STEEL{
        @Override
        public int getLevelRequirement(){
            return 35;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 34;
            }else if(type.equals(WeaponType.SPEAR)){
                return 37;
            }else{
                return 30;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.GOLD_INGOT;
        }
    },
    DRAVEN{
        @Override
        public int getLevelRequirement(){
            return 55;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.SWORD)){
                return 45;
            }else if(type.equals(WeaponType.SPEAR)){
                return 50;
            }else{
                return 39;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.DIAMOND;
        }
    },

    //Bows

    OAK{
        @Override
        public int getLevelRequirement(){
            return 1;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.BOW)){
                return 7;
            }else{
                return 1;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.BOW;
        }
    },
    BIRCH{
        @Override
        public int getLevelRequirement(){
            return 10;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            if(type.equals(WeaponType.BOW)){
                return 14;
            }else{
                return 1;
            }
        }

        @Override
        public Material getMaterialEquil(){
            return Material.BOW;
        }
    },
    JUNGLE{
        @Override
        public int getLevelRequirement(){
            return 20;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 25;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.BOW;
        }
    },
    ACACIA{
        @Override
        public int getLevelRequirement(){
            return 30;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 37;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.BOW;
        }
    },
    SPRUCE{
        @Override
        public int getLevelRequirement(){
            return 45;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 45;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.BOW;
        }
    },
    DARK_OAK{
        @Override
        public int getLevelRequirement(){
            return 60;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 53;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.BOW;
        }
    },


    //SPELLBOOKS

    NOVICE{
        @Override
        public int getLevelRequirement(){
            return 1;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 1;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.ENCHANTED_BOOK;
        }
    },
    JOURNEYMAN{
        @Override
        public int getLevelRequirement(){
            return 20;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 2;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.ENCHANTED_BOOK;
        }
    },
    APPRENTICE{
        @Override
        public int getLevelRequirement(){
            return 40;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 3;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.ENCHANTED_BOOK;
        }
    },
    MASTER{
        @Override
        public int getLevelRequirement(){
            return 60;
        }

        @Override
        public int getBaseDamage(WeaponType type){
            return 5;
        }

        @Override
        public Material getMaterialEquil(){
            return Material.ENCHANTED_BOOK;
        }
    };

    public static ArrayList<WeaponMaterial> getWeaponMaterials(){
        ArrayList<WeaponMaterial> materials = new ArrayList<WeaponMaterial>();
        materials.add(STONE);
        materials.add(RUSTY);
        materials.add(BRONZE);
        materials.add(IRON);
        materials.add(STEEL);
        materials.add(DAGONION_STEEL);
        materials.add(DRAVEN);
        materials.add(OAK);
        materials.add(BIRCH);
        materials.add(JUNGLE);
        materials.add(ACACIA);
        materials.add(SPRUCE);
        materials.add(DARK_OAK);
        materials.add(NOVICE);
        materials.add(JOURNEYMAN);
        materials.add(APPRENTICE);
        materials.add(MASTER);
        return materials;
    }

    public static ArrayList<Material> getMaterials(){
        ArrayList<Material> materials = new ArrayList<Material>();
        for(WeaponMaterial material : getWeaponMaterials()){
            materials.add(material.getMaterialEquil());
        }
        return materials;
    }


    public abstract int getLevelRequirement();

    public abstract int getBaseDamage(WeaponType type);

    public abstract Material getMaterialEquil();


}
