package net.genflowstudios.kingdoms.items.items.weapons;

/**
 * Created by Samuel on 6/25/2015.
 */
public enum WeaponType{

    SWORD,
    SPEAR,
    STAFF,
    WAND,
    BOW,
    SPELL_BOOK

}
