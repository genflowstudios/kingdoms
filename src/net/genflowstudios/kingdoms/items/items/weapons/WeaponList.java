package net.genflowstudios.kingdoms.items.items.weapons;

import org.bukkit.Material;

import java.util.ArrayList;

/**
 * Created by Samuel on 6/21/2015.
 */
public enum WeaponList{

    STONE_SWORD{
        @Override
        public String getName(){
            return "Stone Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.STONE_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }


        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.STONE;
        }

        @Override
        public int getID(){
            return 1;
        }

    },
    STONE_SPEAR{
        @Override
        public String getName(){
            return "Stone Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.STONE_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.STONE;
        }

        @Override
        public int getID(){
            return 2;
        }

    },
    RUSTY_SWORD{
        @Override
        public String getName(){
            return "Rusty Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.WOOD_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.RUSTY;
        }

        @Override
        public int getID(){
            return 3;
        }

    },
    RUSTY_SPEAR{
        @Override
        public String getName(){
            return "Rusty Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.WOOD_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.RUSTY;
        }

        @Override
        public int getID(){
            return 4;
        }

    },
    BRONZE_SWORD{
        @Override
        public String getName(){
            return "Bronze Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.WOOD_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.BRONZE;
        }

        @Override
        public int getID(){
            return 5;
        }

    },
    BRONZE_SPEAR{
        @Override
        public String getName(){
            return "Bronze Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.WOOD_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.BRONZE;
        }

        @Override
        public int getID(){
            return 6;
        }

    },
    IRON_SWORD{
        @Override
        public String getName(){
            return "Iron Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.IRON_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.IRON;
        }

        @Override
        public int getID(){
            return 7;
        }

    },
    IRON_SPEAR{
        @Override
        public String getName(){
            return "Iron Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.IRON_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.IRON;
        }

        @Override
        public int getID(){
            return 8;
        }

    },
    STEEL_SWORD{
        @Override
        public String getName(){
            return "Steel Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.IRON_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.STEEL;
        }

        @Override
        public int getID(){
            return 9;
        }

    },
    STEEL_SPEAR{
        @Override
        public String getName(){
            return "Steel Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.IRON_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.STEEL;
        }

        @Override
        public int getID(){
            return 10;
        }

    },
    DAGONION_STEEL_SWORD{
        @Override
        public String getName(){
            return "Dagonion Steel Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.GOLD_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.DAGONION_STEEL;
        }

        @Override
        public int getID(){
            return 11;
        }

    },
    DAGONION_STEEL_SPEAR{
        @Override
        public String getName(){
            return "Dagonion Steel Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.GOLD_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.DAGONION_STEEL;
        }

        @Override
        public int getID(){
            return 12;
        }

    },
    DRAVEN_SWORD{
        @Override
        public String getName(){
            return "Draven Sword";
        }

        @Override
        public Material getMaterial(){
            return Material.DIAMOND_SWORD;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SWORD;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.DRAVEN;
        }

        @Override
        public int getID(){
            return 13;
        }

    },
    DRAVEN_SPEAR{
        @Override
        public String getName(){
            return "Draven Spear";
        }

        @Override
        public Material getMaterial(){
            return Material.DIAMOND_SPADE;
        }

        @Override
        public WeaponType getWeaponType(){
            return WeaponType.SPEAR;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return WeaponMaterial.DRAVEN;
        }

        @Override
        public int getID(){
            return 14;
        }

    },

    OAK_WOOD_BOW{
        @Override
        public String getName(){
            return "Oak Bow";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 15;
        }

    },
    BIRCH_WOOD_BOW{
        @Override
        public String getName(){
            return "Birch Bow";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 16;
        }

    },
    ACACIA_WOOD_BOW{
        @Override
        public String getName(){
            return "Acacia Bow";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 17;
        }

    },
    SPRUCE_WOOD_BOW{
        @Override
        public String getName(){
            return "Spruce Bow";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 18;
        }

    },
    DARK_OAK_WOOD_BOW{
        @Override
        public String getName(){
            return "Dark Oak Bow";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 19;
        }

    },

    NOVICE_SPELLBOOK{
        @Override
        public String getName(){
            return "Novice Spellbook";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 20;
        }

    },
    JOURNEYMAN_SPELLBOOK{
        @Override
        public String getName(){
            return "Journeyman Spellbook";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 21;
        }

    },
    APPRENTICE_SPELLBOOK{
        @Override
        public String getName(){
            return "Apprentice Spellbook";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 22;
        }

    },
    MASTER_SPELLBOOK{
        @Override
        public String getName(){
            return "Master Spellbook";
        }

        @Override
        public Material getMaterial(){
            return Material.AIR;
        }

        @Override
        public WeaponType getWeaponType(){
            return null;
        }

        @Override
        public WeaponMaterial getWeaponMaterial(){
            return null;
        }

        @Override
        public int getID(){
            return 23;
        }


    };


    public static ArrayList<String> getNames(){
        ArrayList<String> names = new ArrayList<String>();
        for(WeaponList type : getWeapons()){
            names.add(type.getName());
        }
        return names;
    }


    public static ArrayList<WeaponList> getWeapons(){
        ArrayList<WeaponList> types = new ArrayList<WeaponList>();
        types.add(STONE_SPEAR);
        types.add(STONE_SWORD);
        types.add(BRONZE_SPEAR);
        types.add(BRONZE_SWORD);
        types.add(RUSTY_SPEAR);
        types.add(RUSTY_SWORD);
        types.add(IRON_SPEAR);
        types.add(IRON_SWORD);
        types.add(STEEL_SPEAR);
        types.add(STEEL_SWORD);
        types.add(DAGONION_STEEL_SPEAR);
        types.add(DAGONION_STEEL_SWORD);
        types.add(DRAVEN_SPEAR);
        types.add(DRAVEN_SWORD);
        types.add(OAK_WOOD_BOW);
        types.add(BIRCH_WOOD_BOW);
        types.add(ACACIA_WOOD_BOW);
        types.add(SPRUCE_WOOD_BOW);
        types.add(DARK_OAK_WOOD_BOW);
        types.add(NOVICE_SPELLBOOK);
        types.add(JOURNEYMAN_SPELLBOOK);
        types.add(APPRENTICE_SPELLBOOK);
        types.add(MASTER_SPELLBOOK);
        return types;
    }

    public static void registerWeapons(){
        for(WeaponList type : getWeapons()){

        }
    }

    public abstract String getName();

    public abstract Material getMaterial();

    public abstract WeaponType getWeaponType();

    public abstract WeaponMaterial getWeaponMaterial();

    public abstract int getID();

}
