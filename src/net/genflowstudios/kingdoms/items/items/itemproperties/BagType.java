package net.genflowstudios.kingdoms.items.items.itemproperties;

/**
 * Created by Samuel on 6/4/2015.
 */
public enum BagType{

    LUMBER,
    STONE,
    QUIVER

}
