package net.genflowstudios.kingdoms.items.items.itemproperties;

/**
 * Created by Samuel on 6/4/2015.
 */
public enum BagTier{


    TIER_ONE{
        @Override
        public int getSize(){
            return 60;
        }
    },
    TIER_TWO{
        @Override
        public int getSize(){
            return 90;
        }
    },
    TIER_THREE{
        @Override
        public int getSize(){
            return 150;
        }
    };


    public abstract int getSize();


}
