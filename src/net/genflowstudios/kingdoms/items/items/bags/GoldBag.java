package net.genflowstudios.kingdoms.items.items.bags;

import net.genflowstudios.kingdoms.items.items.itemproperties.BagTier;
import net.genflowstudios.kingdoms.items.items.itemproperties.BagType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Samuel on 6/9/2015.
 */
public class GoldBag extends Bag{


    int stoneAmount;
    boolean full;

    public GoldBag(int size, BagTier tier, BagType type, ItemStack bagMaterial){
        super(size, tier, type, bagMaterial);
        this.stoneAmount = 0;
        this.full = false;
    }

    public void addMaterial(int amount){
        if(stoneAmount < size){
            int temp = stoneAmount + amount;
            if(temp > size){
                int temp2 = temp - size;
                amount = temp2;
                stoneAmount += amount;
                setFull(true);
            }else{
                stoneAmount += amount;
            }
        }else{
            setFull(true);
        }
    }


    public void removeMaterial(int amount){
        this.stoneAmount -= amount;
    }

    public boolean isFull(){
        return full;
    }

    public void setFull(boolean full){
        this.full = full;
    }


    public int getStoneAmount(){
        return stoneAmount;
    }

    public void setStoneAmount(int stoneAmount){
        this.stoneAmount = stoneAmount;
    }
}
