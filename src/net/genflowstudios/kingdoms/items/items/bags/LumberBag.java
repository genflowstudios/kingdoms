package net.genflowstudios.kingdoms.items.items.bags;

import net.genflowstudios.kingdoms.items.items.itemproperties.BagTier;
import net.genflowstudios.kingdoms.items.items.itemproperties.BagType;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Samuel on 6/9/2015.
 */
public class LumberBag extends Bag{


    int lumberAmount;
    boolean full;

    public LumberBag(int size, BagTier tier, BagType type, ItemStack bagMaterial){
        super(size, tier, type, bagMaterial);
        this.lumberAmount = 0;
        this.full = false;
    }

    public void addMaterial(int amount){
        if(lumberAmount < size){
            int temp = lumberAmount + amount;
            if(temp > size){
                int temp2 = temp - size;
                amount = temp2;
                lumberAmount += amount;
                setFull(true);
            }else{
                lumberAmount += amount;
            }
        }else{
            setFull(true);
        }

    }

    public void removeMaterial(int amount){
        this.lumberAmount -= amount;
    }

    public boolean isFull(){
        return full;
    }

    public void setFull(boolean full){
        this.full = full;
    }

    public int getLumberAmount(){
        return lumberAmount;
    }

    public void setLumberAmount(int amount){
        this.lumberAmount = amount;
    }
}
