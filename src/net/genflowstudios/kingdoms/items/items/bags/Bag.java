package net.genflowstudios.kingdoms.items.items.bags;


import net.genflowstudios.kingdoms.items.items.itemproperties.BagTier;
import net.genflowstudios.kingdoms.items.items.itemproperties.BagType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Samuel on 6/4/2015.
 */
public class Bag{

    BagType type;
    BagTier tier;
    int size;
    ItemStack bagMaterial;

    public Bag(int size, BagTier tier, BagType type, ItemStack bagMaterial){
        this.type = type;
        this.size = size;
        this.tier = tier;
        this.bagMaterial = bagMaterial;
    }


    /**
     * Adds material to the bag.
     *
     * @param material
     * @param amount
     * @throws Exception
     */
    public void addMaterial(Material material, int amount) throws Exception{
        //TO BE EXTENDED.
    }


    public BagType getType(){
        return type;
    }

    public void setType(BagType type){
        this.type = type;
    }

    public BagTier getTier(){
        return tier;
    }

    public void setTier(BagTier tier){
        this.tier = tier;
    }

    public int getSize(){
        return size;
    }

    public void setSize(int size){
        this.size = size;
    }


    public ItemStack getBagMaterial(){
        return bagMaterial;
    }

    public void setBagMaterial(ItemStack bagMaterial){
        this.bagMaterial = bagMaterial;
    }
}
