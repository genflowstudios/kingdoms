package net.genflowstudios.kingdoms.items.items.armor;

import java.util.ArrayList;

/**
 * Created by Samuel on 6/21/2015.
 */
public enum ArmorTypes{


    RUSTY_HELMET{
        @Override
        public String getName(){
            return "Rusty Helmet";
        }

        @Override
        public double getArmorValue(){
            return 3;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    RUSTY_CHEST{
        @Override
        public String getName(){
            return "Rusty Chestplate";
        }

        @Override
        public double getArmorValue(){
            return 5;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    RUSTY_LEGS{
        @Override
        public String getName(){
            return "Rusty Platelegs";
        }

        @Override
        public double getArmorValue(){
            return 4;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    RUSTY_BOOTS{
        @Override
        public String getName(){
            return "Rusty Boots";
        }

        @Override
        public double getArmorValue(){
            return 2;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },

    BRONZE_HELMET{
        @Override
        public String getName(){
            return "Bronze Helmet";
        }

        @Override
        public double getArmorValue(){
            return 4;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    BRONZE_CHEST{
        @Override
        public String getName(){
            return "Bronze Chestplate";
        }

        @Override
        public double getArmorValue(){
            return 6;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    BRONZE_LEGS{
        @Override
        public String getName(){
            return "Bronze Platelegs";
        }

        @Override
        public double getArmorValue(){
            return 5;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    BRONZE_BOOTS{
        @Override
        public String getName(){
            return "Bronze Boots";
        }

        @Override
        public double getArmorValue(){
            return 3;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },

    STEEL_HELMET{
        @Override
        public String getName(){
            return "Steel Helmet";
        }

        @Override
        public double getArmorValue(){
            return 5;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    STEEL_CHEST{
        @Override
        public String getName(){
            return "Steel Chestplate";
        }

        @Override
        public double getArmorValue(){
            return 7;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    STEEL_LEGS{
        @Override
        public String getName(){
            return "Steel Platelegs";
        }

        @Override
        public double getArmorValue(){
            return 6;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    },
    STEEL_BOOTS{
        @Override
        public String getName(){
            return "Steel Boots";
        }

        @Override
        public double getArmorValue(){
            return 4;
        }

        @Override
        public double getMagicArmorValue(){
            return -1;
        }
    };


    public static ArrayList<String> getNames(){
        ArrayList<String> names = new ArrayList<String>();
        for(ArmorTypes type : getArmor()){
            names.add(type.getName());
        }
        return names;
    }


    public static ArrayList<ArmorTypes> getArmor(){
        ArrayList<ArmorTypes> armortypes = new ArrayList<ArmorTypes>();
        armortypes.add(RUSTY_HELMET);
        armortypes.add(RUSTY_CHEST);
        armortypes.add(RUSTY_LEGS);
        armortypes.add(RUSTY_BOOTS);
        armortypes.add(BRONZE_HELMET);
        armortypes.add(BRONZE_CHEST);
        armortypes.add(BRONZE_LEGS);
        armortypes.add(BRONZE_BOOTS);
        armortypes.add(STEEL_HELMET);
        armortypes.add(STEEL_CHEST);
        armortypes.add(STEEL_LEGS);
        armortypes.add(STEEL_BOOTS);
        return armortypes;
    }


    public abstract String getName();

    public abstract double getArmorValue();

    public abstract double getMagicArmorValue();

}
