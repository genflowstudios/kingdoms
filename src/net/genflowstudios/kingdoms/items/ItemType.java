package net.genflowstudios.kingdoms.items;

public enum ItemType{

    SWORD,
    AXE,
    WAND,
    STAFF,
    BOW,
    ARROW,
    THROWING_KNIFE


}
